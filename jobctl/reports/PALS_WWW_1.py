#!/usr/bin/python
#
#    Generate reports for PALS_WWW_1 agent
#
import sys, re
from os.path import basename
from socket import gethostname
from reportUtils import *



# G_configs is created by reportUtils.py
loadConfigs('REPORT_PALS_WWW_1')
if 'mailList' not in G_configs:
  print basename(sys.argv[0]) + ": mailList not configured, please edit jobs.conf file before continuing"
  sys.exit(1)



#
#	Generate one section of report
#	Return number of rows processed
#
def generateSection(status, jobList, outputType):
  section = emitText("Jobs with status: " + status, outputType)
  section += startTable(['Timestamp', 'Job ID', 'Retries', 'Options/Messages'], outputType)
  itemCount = 0
  deleteCount = 0
  for stats in jobList:
    if (stats['status'] == status):
      optDict = parseOptionString(stats['options'])
      optList = []
      #
      #  Only report targetname (i.e. ignore cached filename)
      #  If this is not a benign failure ("action: rm" and "File
      #  does not exist"), highlight the row
      #
      action = ''
      msg = ''
      for optname in optDict.keys():
        if (optname == 'targetname'):
          optList.insert(0, 'targetname: ' + optDict['targetname'])
        if (optname == 'action'):
          optList.insert(0, 'action: ' + optDict['action'])
          if (optDict['action'] == 'rm'):
            deleteCount = deleteCount + 1
            action = 'rm'
      if ((status != "OK") and (stats['msg'])):
        optList.append('msg: ' + stats['msg'])
        msg = stats['msg']
      highlight = False
      if (status == "FATAL"):
        highlight = True
        if ((action == "rm") and (re.findall(r'File does not exist', msg))):
          highlight = False
      section += emitRow([stats['tstamp'], stats['jobid'], stats['retries'], optList], outputType, highlightRow=highlight)
      itemCount = itemCount + 1
  section += endTable(outputType)
  return section, itemCount, deleteCount

#
#	Generate report
#
def generateReport(sectionList, jobList, outputType):
  totalJobs = 0
  summary = ''
  body = ''
  for sectionTitle in sectionList:
    if (sectionTitle == "OK"):
      section, totalOK, totalDel = generateSection("OK", jobList, outputType)
      totalJobs = totalJobs + totalOK
      section += emitText('%d jobs OK (%d deletes)' % (totalOK, totalDel), outputType)
      section += emitText('', outputType)
      body += section
      summary += emitText('%d jobs OK (%d deletes)' % (totalOK, totalDel), outputType)
    if (sectionTitle == "FAILED"):
      section, totalFailed, totalDel = generateSection("FAILED", jobList, outputType)
      totalJobs = totalJobs + totalFailed
      section += emitText(str(totalFailed) + ' jobs failed, will be retried', outputType)
      section += emitText('', outputType)
      body += section
      summary += emitText(str(totalFailed) + ' jobs failed, will be retried', outputType)
    if (sectionTitle == "FATAL"):
      section, totalFatal, totalDel = generateSection("FATAL", jobList, outputType)
      totalJobs = totalJobs + totalFatal
      section += emitText(str(totalFatal) + ' jobs failed with fatal errors, cannot retry', outputType)
      section += emitText('', outputType)
      body += section
      summary += emitText(str(totalFatal) + ' jobs failed with fatal errors, cannot retry', outputType)
  report = emitText('Report summary:', outputType)
  report += summary
  report += emitText("Total " + str(totalJobs) + " jobs tracked", outputType)
  report += emitText('', outputType)
  report += body
  return report

#
#	Main
#
#	Handle command-line options
#	Generate the report text
#	Mail out report
#
myArgv = sys.argv
params = processOpts(myArgv)

if params['exceptions-only']:
  sections = ['FAILED', 'FATAL']
else:
  sections = ['OK', 'FAILED', 'FATAL']

myHostName = gethostname()
agent = 'PALS_WWW_1'
jobList = getJobRecords('PALS_WWW_1', params['date'], params['filename'])

body = emitText('Report for %s' % (params['date']), params['outputType'])
now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
body += emitText('Generated %s' % now, params['outputType'])
body += emitText('Hostname: %s' % myHostName, params['outputType'])
body += emitText('', params['outputType'])
body += generateReport(sections, jobList, params['outputType'])

# send email if necessary
if params['mailOutput']:
  subject = myHostName + ': PALS_WWW_1 report for ' + params['date']
  fromAddr = G_configs['fromAddr']
  toAddrList = G_configs['mailList']
  mailReport(params['outputType'], fromAddr, toAddrList, subject, body)
else:
  print body


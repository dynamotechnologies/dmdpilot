#!/bin/bash

# Sample crontab line:
# 15 0 * * * /var/www/html/drmtest/jobctl/reports/reporter.sh

APPDIR="/var/www/html/dmdpilot"

if [ "$APPDIR" == '' ]; then
  echo `basename $0` ": APPDIR not set, please edit and continue"
else
  $APPDIR/jobctl/reports/PALS_WWW_1.py -d -1 -h -m $APPDIR/logs/job_summary.log
fi

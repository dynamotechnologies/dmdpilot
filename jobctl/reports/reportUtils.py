#!/usr/bin/python

import sys, os
import urllib
import getopt
import re
import smtplib
import time
import types
import datetime
from glob import glob
import cgi
import ConfigParser

#
#    Utility functions for job control/exception reports
#



# Globals
G_formatStr = '%19s  %20s  %7s  %-30s'	# for text output
G_configs = {}


#
#	Merge G_configs with config data from requested section
#
def loadConfigs(section):
  global G_configs
  pathname = os.path.dirname(sys.argv[0]) + '/../../includes'
  fullpath = os.path.abspath(pathname)
  myConf = ConfigParser.SafeConfigParser()
  myConf.optionxform = str
  myConf.read(fullpath + '/jobs.conf.php')
  G_configs.update(myConf.items(section))

#
#	FIXME: should mail take an argument instead of using
#	       an implied distribution list?
#
def usage():
  appName = sys.argv[0]
  print "Usage: " + appName + " [-d date] [-h] [-m] [-t] [-x] <logfile>"
  print "  -d, --date (YYYY-MM-DD or integer date offset)"
  print "  -h, --html (html output)"
  print "  -m, --mail (send report via email)"
  print "  -t, --text (text output, default, excludes --html)"
  print "  -x, --exceptions-only (failed and fatal jobs only)"

#
#	Comparison function to sort rows by tstamp
#
def mycmp(x,y):
  return cmp(x['tstamp'], y['tstamp'])

#
# Send an email containing the supplied body text
#
# bodyType is "text" or "html"
# fromAddr is a string
# toAddrStr is a string containing a comma-separated list of addresses
#
def mailReport(bodyType, fromAddr, toAddrStr, subject, bodyStr):
  header = 'Subject: ' + subject + '\n'
  header = header + 'From: ' + fromAddr + '\n'
  toAddrList = toAddrStr.replace(',', ' ').split()
  header = header + 'To: ' + toAddrStr.strip() + '\n'
  if (bodyType == 'html'):
    bodyStr = bodyStr.encode('ascii', 'xmlcharrefreplace')
    header = header + "Content-type: text/html\n"
  bodyStr = header + '\n' + bodyStr
  server = smtplib.SMTP('localhost')
  # server.set_debuglevel(1)
  server.sendmail(fromAddr, toAddrList, bodyStr)
  server.quit()

#
# Convert the options string (from summary log) into a dictionary
#
# To parse options string:
# split on commas
# strip lead/trailing spaces, remove quotes
# split on equals sign into key/value
# unquote HTML entity codes
#
def parseOptionString(optStr):
  opts = optStr.split(',')
  opts = [ o.strip() for o in opts if o.strip() != '' ]
  opts = [ o[ 1:-1 ] for o in opts ]	# remove quotes
  olist = [o.split('=') for o in opts]	# result is a list of pairwise lists
  olist = [ [ o[0].strip(), o[1].strip() ] for o in olist ]
  olist = [ [ urllib.unquote(o[0]), urllib.unquote(o[1]) ] for o in olist ]
  result = dict(olist)
  return result

#
# Start a display table
# Client should use emitRow() to output each contained table row
# Client should pair this with endTable()
#
def startTable(titles, format="text"):
  global G_formatStr
  text = ''
  if (format == "html"):
    text += "<TABLE border='1'>"
    text += "<TR>"
    for title in titles:
      text += "<TD>" + title + "</TD>"
    text += "</TR>"
  else:
    text += G_formatStr % tuple(titles) + "\n"
    text += G_formatStr % ('-'*19, '-'*20, '-'*7, '-'*30) + "\n"
  return text

#
# Output a table row
#
# (text) If a column is a list, somehow convert the whole shebang
# to a reasonably printable form
#
def emitRow(columns, format="text", highlightRow=False):
  global G_formatStr
  text = ''
  tdstyle = ""
  if (highlightRow):
    tdstyle = "style='border:solid 2px red'"
  if (format == "html"):
    text += "<TR>"
    for col in columns:
      text += "<TD " + tdstyle + ">"
      if (type(col) == types.ListType):
        for i, line in enumerate(col):
          text += cgi.escape(str(line))
          if i < len(col) - 1:
            text += "<BR/>"
      else:
        text += cgi.escape(str(col))
      text += "</TD>"
    text += "</TR>\n"
  else:
    outputRows = []
    while True:
      somethingToPrint = False
      newRow = []
      ncol = len(columns)
      for i in range(0, ncol):
        col = columns[i]
        if (col == None):
          newRow.append('')
        else:
          if (type(col) == types.ListType):
            if (len(col) > 0):
              somethingToPrint = True
              newRow.append(col.pop(0))
            else:
              columns[i] = None
          else:
            somethingToPrint = True
            newRow.append(col)
            columns[i] = None
      if (somethingToPrint):
        outputRows.append(newRow)
      else:
        break
    for row in outputRows:
      text += G_formatStr % tuple(row) + "\n"
  return text

def endTable(format="text"):
  text = ''
  if (format == "html"):
    text = "</TABLE>"
  return text

def emitText(text, format="text"):
  if (format == "html"):
    text = cgi.escape(text) + "<BR/>\n"
  else:
    text = text + "\n"
  return text

#
#	Process command-line options
#	Returns parameters in a dictionary
#
#	Dictionary will have the following values set:
#	date		(date in YYYY-MM-DD format)
#	outputType	(text or html)
#	mailOutput	(boolean, send final report by mail)
#	exceptions-only	(boolean, only report on failed/fatal exceptions)
#	filename	(name of log file)
#
def processOpts(argv):
  try:
    opts, args = getopt.getopt(sys.argv[1:], "d:hmtx", ["date", "html", "mail", "text", "exceptions-only"])
  except getopt.GetoptError:
    usage()
    sys.exit(2)
  
  optDict = dict(opts)
  if '-d' in optDict:
    optDict['--date'] = optDict['-d']
  if '-h' in optDict:
    optDict['--html'] = optDict['-h']
  if '-m' in optDict:
    optDict['--mail'] = optDict['-m']
  if '-t' in optDict:
    optDict['--text'] = optDict['-t']
  if '-x' in optDict:
    optDict['--exceptions-only'] = optDict['-x']
  
  today = datetime.date.today().isoformat()	# YYYY-MM-DD

  parmDict = { 'date': today,
               'outputType': 'text',
               'mailOutput': False,
               'exceptions-only': False,
               'filename': '' }
  
  if (len(args) != 1):
    usage()
    sys.exit(2)
  
  parmDict['filename'] = args[0]

  if '--agent' in optDict:
    parmDict['agent'] = optDict['--agent']
  
  if '--date' in optDict:
    dateOpt = optDict['--date'].strip()
    fmt = ''

    try:
      time.strptime(dateOpt, '%Y-%m-%d')
      fmt = 'date'
    except ValueError:
      pass

    try:
      i = int(dateOpt)
      fmt = 'int'
    except ValueError:
      pass

    if (fmt == 'date'):
      parmDict['date'] = dateOpt
    elif (fmt == 'int'):
      d = datetime.date.today().toordinal()
      d = d + int(dateOpt)
      parmDict['date'] = datetime.date.fromordinal(d).isoformat()
    else:
      print "Unable to parse date parameter " + optDict['date']
      usage()
      sys.exit(2)
  
  if '--html' in optDict:
    parmDict['outputType'] = 'html'
  
  if '--text' in optDict:
    parmDict['outputType'] = 'text'
  
  if '--mail' in optDict:
    parmDict['mailOutput'] = True
  
  if '--exceptions-only' in optDict:
    parmDict['exceptions-only'] = True

  return parmDict

#
#    Collect/sort log records
#
#    This returns a list of log records, sorted by time and jobid.
#    Records are collected from all job log files in the log directory
#    (i.e. job_summary.log, job_summary.log.1, job_summary.log.2, ...)
#    Only records matching the supplied agent are included.
#
#    The list contains hashes with the following keys:
#
#	tstamp	- time that job was attempted
#	jobid	- identifier for the job request, matches the extension of
#		  the job request file
#	options	- set of options submitted with the request, this includes
#		  all options sent in the original job request
#	status	- 'OK', 'FAILED', or 'FATAL'
#	msg	- any message attached to the summary log--errors are usually
#		  described here
#	retries	- number of times this job has been attempted
#
def getJobRecords(agent, date, filepattern):
  alljobs = {}
  filelist = glob(filepattern)	# Get list of log files
  for filename in filelist:
    fp = open(filename, 'rb')
    for line in fp:
      if (re.match('^' + date, line)):
        [ tstamp, jobid, options, status, msg ] = line.split('|')
        [ agent, suffix ] = jobid.split('.')
      
        if (re.match('^' + agent + '$', agent)):
          options = urllib.unquote(options)
          msg = urllib.unquote(msg)
          agentrec = dict([('tstamp', tstamp), ('jobid', jobid), ('options', options),
            ('status', status), ('msg', msg), ('retries', 0)])
          if (alljobs.has_key(jobid)):
            agentrec['retries'] = alljobs[jobid]['retries'] + 1
          alljobs[jobid] = agentrec
    fp.close()

  joblist = alljobs.values()
  joblist.sort(mycmp)
  return joblist

#!/usr/bin/python
#
#	PALS_APPEALS_1 <jobfile>
#
#	Publish a PALS appeals file to Akamai
#
#	Currently expected files are:
#
#		Unknown
#
#       Exit statuses are defined in agentutils.py
#
#       Exit status 0 (JOB_OK) if OK
#       Exit status 1 (JOB_FAILED) if error
#       Exit status 2 (JOB_FAILED_FATAL) if unrecoverable error
#
#	Recognized options:
#
#	cached_file	The name of the file to be transferred
#			This is a file in the job_queues/jobfiles directory
#			(Provided automatically)
#
#	filename	The name of the original file (Datamart needs this
#			to determine how to process the file)
#			(Required)
#
from ftplib import FTP
import os
from agentutils import *
import stat
import subprocess

#
#	Application configs
#
#	G_appdir is the application directory from agentutils.py
#
QUEUEDIR=G_appdir + '/job_queues'
INQ=QUEUEDIR + '/in_q'
JOBFILES=QUEUEDIR + '/jobfiles'

#
#	Agent configs
#
# agentutils.py creates the G_configs dictionary for us
try:
  loadConfigs('AGENT_PALS_APPEALS_1')
  TGTDIR_PREFIX=G_configs['TGTDIR_PREFIX']
  SERVER=G_configs['SERVER']
  USER=G_configs['USER']
  PASS=G_configs['PASS']
except:
  print "Problem loading configs from jobs.conf"
  print "Missing config parameter?"
  raise

if (len(argv) != 2):
  print "Usage: " + argv[0] + " <jobfile>"
  exit(JOB_FAILED)

fname=argv[1]

# Parse job parameters from the job order file
options={}
try:
  readJobFile(INQ + '/' + fname, options)
except:
  startLog(fname, options)
  endLog(JOB_FAILED_FATAL, "Unable to read job file " + fname)
  exit(JOB_FAILED_FATAL)

# Log job name and options
startLog(fname, options)

targetDir = ''
if ('targetdir' in options):
  targetDir = options['targetdir']

if ('targetname' not in options):
  endLog(JOB_FAILED_FATAL, "No file target name")
  exit(JOB_FAILED_FATAL)

SRCFILE=JOBFILES + '/' + options['cached_file']
TGTDIR=TGTDIR_PREFIX + '/' + targetDir
TGTFNAME=options['targetname']

#
#	Do file transfer/ingest here
#
# Check for "action=rm" option, meaning delete named file
ACTION=''
if ('action' in options):
  if (options['action'] == "rm"):
    ACTION='rm'
  else:
    endLog(JOB_FAILED, "Bad value for job option action=" + options['action'])
    sys.exit(JOB_FAILED_FATAL)

local_size = os.stat(SRCFILE).st_size
remote_size = 0

try:
  log("Logging into " + SERVER)
  ftp = FTP(SERVER)
  ftp.login(USER, PASS)
  log("CDing to " + TGTDIR)
  ftp.cwd(TGTDIR)
  if (ACTION == 'rm'):
    log("Removing file " + TGTFNAME)
    ftp.delete(TGTFNAME)
  else:
    ftp.delete(TGTFNAME)
  else:
    log("Opening " + SRCFILE)
    fp = open(SRCFILE, "rb")
    log("Storing file as " + TGTFNAME)
    ftp.storbinary("STOR " + TGTFNAME, fp)
    log("Closing " + SRCFILE)
    fp.close()
    remote_size = ftp.size(TGTFNAME)
  log("Leaving FTP server")
  ftp.quit()
except Exception, errObj:
  # FIXME Python 2.6 syntax: except Exception as errmsg:
  errmsg = str(errObj)
  if ((ACTION == 'rm') and (errmsg.find("File does not exist") >= 0)):
    log("Unable to delete file, reason: " + errmsg)
    # benign, treat as OK
  else:
    endLog(JOB_FAILED, errmsg)
    sys.exit(JOB_FAILED)

# If this was a normal transfer, try to validate the transfer size
if (ACTION != 'rm'):
  log("Local size: " + str(local_size))
  log("Remote size: " + str(remote_size))
  if (local_size == remote_size):
    log("Transferred " + str(remote_size) + " bytes of " + SRCFILE)
  else:
    endLog(JOB_FAILED, "Bad upload, file size %d, expected %d bytes" % (remote_size, local_size))
    sys.exit(JOB_FAILED)


#
#	Cleanup/shutdown
#
log("Removing cached file " + options['cached_file'])
try:
  os.remove(SRCFILE)	# remove data file from jobfiles dir
  log("Removed OK")
except Exception:
  log("Remove failed!")

endLog(JOB_OK, "Job complete")

if (ACTION != 'rm'):
  endLog(JOB_OK, "Transfer complete")
else:
  endLog(JOB_OK, "Deletion completed")


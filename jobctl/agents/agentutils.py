#!/usr/bin/python

#
#  Agent script utility functions
#
#  Agents are written in Python, and take a single argument, the name of
#  a job file that contains the parameters for the job to be executed.
#  The readJobFile() function will parse the XML in the job file and
#  store the contents into the dictionary variable provided as the second
#  parameter.
#
#  Agents can exit with 3 codes:
#  JOB_OK (everything went well)
#  JOB_FAILED (something went wrong, will retry)
#  JOB_FAILED_FATAL (something went wrong and job cannot be retried)
#
#  There are two log files, a detail log and a summary log.  Most of
#  the log messages will go into the detail log, with a single summary
#  line entering the summary log when the agent finishes processing.
#  These files (job_detail.log and job_summary.log) are currently located
#  in the logs/ directory.
#
#  Agents should use endLog() to record the final job status with
#  one of these codes.  To use logging, the agent should first execute
#  startLog() as soon as possible, and then use log() to create any
#  other log messages in the detail log.
#
#  To use logging: clients should do
#  startLog(jobid, optionsList)
#  then
#  log("detailed message")
#  and finally
#  endLog(JOB_OK, "final message")
#  or
#  endLog(JOB_FAILED, "final message")
#  or
#  endLog(JOB_FAILED_FATAL, "final message")
#

from sys import *
from xml.dom.minidom import parse
from datetime import datetime
import logging
import time
import os, sys
import urllib
import ConfigParser
import boto3
import shutil
import errno
import stat

JOB_OK = 0
JOB_FAILED = 1		# can be retried
JOB_FAILED_FATAL = 2	# cannot be retried

G_appdir = os.path.dirname(sys.argv[0]) + "/../.."

G_jobid = 'Unknown'
G_options = {}
G_configs = {}	#	Config values from jobs.conf are stored here

DETAILLOG = None
SUMMARYLOG = None

# set up detail log
def initDetailLog(logdir):
  global DETAILLOG
  detailHandler = logging.FileHandler(logdir + '/logs/job_detail.log')
  DETAILLOG = logging.getLogger('detail')
  DETAILLOG.setLevel(logging.DEBUG)
  DETAILLOG.addHandler(detailHandler)

# set up summary log
def initSummaryLog(logdir):
  global SUMMARYLOG
  summaryHandler = logging.FileHandler(logdir + '/logs/job_summary.log')
  SUMMARYLOG = logging.getLogger('summary')
  SUMMARYLOG.setLevel(logging.DEBUG)
  SUMMARYLOG.addHandler(summaryHandler)

#
#       Merge G_configs with config data from requested section
#
def loadConfigs(section):
  global G_configs
  pathname = os.path.dirname(sys.argv[0]) + '/../../includes'
  fullpath = os.path.abspath(pathname)
  myConf = ConfigParser.SafeConfigParser()
  myConf.optionxform = str
  myConf.read(fullpath + '/jobs.conf.php')
  G_configs.update(myConf.items(section))

#
# Write a message line to the detail log
#
# detail log format: jobid | text
# vbars are encoded to %7C
# Can restore original text with urllib.unquote(text)
# Not going to worry about possibly missing a valid '%7C' in the original txt
#
def log(text):
  global DETAILLOG
  if (DETAILLOG == None):
    initDetailLog(G_appdir)
  text = text.replace('|', '%7C')
  logMsg = str(datetime.now()) + '|' + G_jobid + '|' + text
  DETAILLOG.info(logMsg)

#
# Write a message line to the summary log
#
# summary log format: time | jobid | options | status | text
# vbars in text are encoded to %7C
# option names and values are all fully urlencoded
# Can restore original text with urllib.unquote(text)
#
def logSummary(status, text):
  global SUMMARYLOG
  global G_options
  if (SUMMARYLOG == None):
    initSummaryLog(G_appdir)
  text = text.replace('|', '%7C')
  timeStr = time.strftime("%Y-%m-%d %H:%M:%S")
  options = ''
  for key, value in G_options.iteritems():
    k = urllib.quote(key)
    v = urllib.quote(value)
    nextOption = '"' + k + "=" + v + '", '
    options = options + nextOption
  logMsg = timeStr + '|' + G_jobid + '|' + options + '|' + status + '|' + text
  SUMMARYLOG.info(logMsg)

#
# Write opening statement (start time, job ID, options) to detail log
# Save job ID for tagging subsequent log messages
#
# Note that options are not encoded here because the detail log isn't
# really meant to be processed--better to leave it human-readable
#
def startLog(jobid, optionsList):
  global G_jobid, G_options
  G_jobid = jobid
  G_options = optionsList
  timeStr = time.strftime("%Y-%m-%d %H:%M:%S")
  log("Starting job " + jobid + " at " + timeStr)
  for key, value in G_options.iteritems():
    log("Option " + key + "=" + value)

#
# Write final statement (end status, reason) to detail and summary logs
#
# status is JOB_OK or JOB_FAILED or JOB_FAILED_FATAL
# reason is whatever message to be logged along with the final status
#
def endLog(status, reason):
  timeStr = time.strftime("%Y-%m-%d %H:%M:%S")
  log(reason)
  log("Ending job " + G_jobid + " at " + timeStr)
  log("==========")
  if (status == JOB_OK):
    logSummary("OK", reason)
  elif (status == JOB_FAILED):
    logSummary("FAILED", reason)
  else:
    logSummary("FATAL", reason)

#
#  Return text value from the given XML document node
#
#  (There should only be one text node present)
#
def getText(docNode):
  text = ""
  if (docNode.hasChildNodes()):
    nodelist = docNode.childNodes
    for node in nodelist:
      if node.nodeType == node.TEXT_NODE:
        text = text + node.data
  return text

#
#  Return name/value hash of all elements below the given document node
#
#
def getArray(docNode):
  hash = {}
  if (docNode.hasChildNodes()):
    nodelist = docNode.childNodes
    for node in nodelist:
      propname = node.nodeName
      propval = getText(node)
      hash[propname] = propval
  return hash

#
#  Read job file,
#  copy parameter/option values into dictionary parameters
#
#  Notable Python exceptions:
#    IOError - file not found?
#    ExpatError - bad XML
#
def readJobFile(fname, options):
  dom = parse(fname)
  options.update(getArray(dom.getElementsByTagName("options")[0]))
  dom.unlink()

def s3_connect(id, pw, bucketname):
  # Open connection to S3
  #conn=boto.connect_s3(id, pw)
  #conn=boto.connect_s3(id, pw, host=G_configs['S3_HOST'])
  # open existing bucket
  #return conn.create_bucket(bucketname)
  conn=boto3.resource('s3', region_name=G_configs['S3_LOCATION'], api_version=None, use_ssl=True, verify=None, endpoint_url=None, aws_access_key_id=id, aws_secret_access_key=pw, aws_session_token=None, config=None)
  return conn.Bucket(bucketname)

def s3_upload(id, pw, bucket, srcfqfname, tgtfname):
  bucket = s3_connect(id, pw, bucket)
  #k=Key(bucket)
  #k.key=tgtfname
  #k.set_contents_from_filename(srcfqfname)
  #k.set_acl('public-read')
  object = bucket.put_object(ACL='public-read', Body=open(srcfqfname, 'rb'),Key=tgtfname)

def s3_delete(id, pw, bucket, fname):
  bucket = s3_connect(id, pw, bucket)
  #bucket.delete_key(fname)
  response=bucket.delete_objects(Delete={'Objects': [{'Key': fname}]})

#
#	Return True/False
#
def copy_file(sourcefile, targetfile):
  try:
    shutil.copyfile(sourcefile, targetfile)
    os.chmod(targetfile, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH |
                         stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
    return True
  except IOError, e:
    if (e.errno == errno.EACCES):
      log("No access to target directory")
    return False

#
#	Return True/False
#
def move_file(sourcefile, targetfile):
  try:
    shutil.move(sourcefile, targetfile)
    os.chmod(targetfile, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH |
                         stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
    return True
  except IOError, e:
    if e.errno == errno.EACCES:
      log("No access to target directory")
    return False

#
#	Return True/False
#
def delete_file(targetfile):
  try:
    os.remove(targetfile)
    return True
  except Exception, e:
    if e.errno == errno.EACCES:
      log("No access to target directory")
    return False

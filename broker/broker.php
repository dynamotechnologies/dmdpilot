<?php

  define("LOGGING", True);	// Log status messages
  define("LOG_TRANSACTIONS", True);	// Log transaction details
  define("LOG_SEARCHREQ", False);	// Log verbose search requests
  define("LOG_SEARCHRESP", False);	// Log verbose search responses
  error_reporting(0);		// Turn off PHP error messages which will
				// mess up XMLRPC output
  putenv("TZ=America/New_York");	// Use standard timezone
  $TXID = mkUniqID();		// Generate ID for this request
  $START_TIME = microtime(TRUE);	// Capture start time for this request
  $REQUEST_XML = "";

/*
 * File: broker.php
 *
 * The service broker
 *
 * This creates an object that accepts XMLRPC service requests and then
 * sends corresponding requests to an appropriate server proxy object
 *
 * This object must:
 * - Maintain a mapping of services to handler functions (via config file)
 * - Accept requests for all published services (XMLRPC)
 * - Select an appropriate handler for each request
 * - Return responses and handle exceptions
 */
  $myRoot = realpath(dirname(__FILE__));
  $LOGDIR = realpath(dirname(__FILE__)) . "/../logs";
  $TMPDIR = realpath(dirname(__FILE__)) . "/../tmpdir";

  define("BROKER_SUMMARY_LOG", "${LOGDIR}/broker.log");
  define("BROKER_DETAIL_LOG", "${LOGDIR}/broker_detail.log");

// load headers
  require_once $myRoot . "/../includes/consts.php";

// load API function definitions
  require_once $myRoot . "/dummyAPI.php";
  require_once $myRoot . "/drmAPI.php";

// load proxy definitions
  require_once $myRoot . "/../interfaces/dummyIntf.php";
  require_once $myRoot . "/../interfaces/drmIntf.php";
  require_once $myRoot . "/../proxies/dummyProxy.php";
  require_once $myRoot . "/../proxies/drmProxy.php";

// create server proxies
  $dummyProxy = new DummyProxy;	// Testing
  $drmProxy = new DrmProxy;

// create mapping between services and handler functions
  $serviceList = array();
  require $myRoot . "/../includes/brokerConfig.inc";


  /*
   * Debugging function - store data in /usr/tmp/debuglog
   * Can also provide a "keyword" parameter that will be attached to the
   * end of the filename
   *
   * Returns name of stored file
   */
  function storeDebugFile($data, $keyword = '') {
    global $START_TIME;

    $logdir = "/usr/tmp/debuglog";
    mkdir($logdir);

    # open /usr/tmp/debuglog file based on ip addr and time
    $currtime = date('Ymd-His-T', $START_TIME);     # Request time
    $ipaddr = $_SERVER['REMOTE_ADDR'];
    $fname = "${logdir}/${ipaddr}_${currtime}";
    if (strlen($keyword) > 0) {
      $fname = $fname . '_' . $keyword;
    }
    $fname = $fname . '.debug';
    $fp = fopen($fname, "wb");
    # write request to tmp file
    fwrite($fp, $data);
    fclose($fp);
    return $fname;
  }

  /*
   * Map a service to a proxy object
   *
   * (All handler functions are methods of "proxy objects", which was to
   * make functions easier to manage if we had to organize code for
   * multiple servers.  But we only ever implemented services for two
   * servers so this became less of an advantage.)
   */
  function registerService($serviceName, $proxy) {
    global $serviceList;

    if (isset($serviceList[$serviceName])) {
      $serviceList[$serviceName][] = $proxy;
    } else {
      $serviceList[$serviceName] = array($proxy);
    }
  }

  /*
   * Generate an XMLRPC fault, return as struct
   */
  function makeFault($code, $msg) {
    $fault = array("faultCode" => $code, "faultString" => $msg);
    return $fault;
  }

  /*
   * Is the specified object a fault struct?
   */
  function isFault(&$obj) {
    return (
      is_array($obj) &&
      isset($obj['faultCode']) &&
      isset($obj['faultString']));
  }

  /*
   * Beautify an XML request object for printing to the log
   * (not recommended for very_large_request_object)
   */
  function prettifyRequest($xml) {
    $params = xmlrpc_decode_request($xml, $method);
    return xmlrpc_encode_request($method, $params);
  }

  /*
   * Inspect top-level parameter types to make sure they match one of
   * the expected function signatures (this used to be done automatically
   * but the functionality went away when we switched from PHPXMLRPC to
   * XMLRPC-EPI)
   *
   * Returns TRUE on success, throws an exception on failure
   *
   * Note that unlike the signature table used in PHPXMLRPC, these do not
   * contain an extra value for the return value type.
   */
  function validateSignature($method, $params) {
    $method_signatures = array(
      "getFile" => array(
          array("string", "struct"),	// w options
          array("string")),		// w/o options
  
      "getFileData" => array(
          array("string", "struct"),	// w options
          array("string")),		// w/o options
  
      "getFileMetadata" => array(
          array("string", "struct"),	// w options
          array("string")),		// w/o options
  
      "putFile" => array(
          array("base64", "struct")),
  
      "deleteFile" => array(
          array("string")),
  
      "putMergedFile" => array(
          array("array", "struct")),
  
      "replaceFileMetadata" => array(
          array("string", "struct", "struct"),	// w options
          array("string", "struct")),		// w/o options
  
      "publishFile" => array(
          array("string", "string", "struct"),	// w options
          array("string", "string")),		// w/o options
  
      "transferFile" => array(
          array("string", "base64", "struct"),	// w options
          array("string", "base64")),		// w/o options
  
      "searchDocs" => array(
          array("string", "struct")),
  
      "getNewFileList" => array(
          array("string")),
  
      "createNEPAMailingList" => array(
          array("string", "string")),
  
      "markNEPAAide" => array(
          array("string", "struct")),

      "unmarkNEPAAide" => array(
          array("string")),

      "updateNEPAAide" => array(
          array("string", "struct")),

      "echoMsg" => array(
          array("string")),

      "helloWorld" => array(
          array()),

      "zipPhaseDocs" => array(
          array("string", "string", "string")),

      "associateDoc" => array(
          array("string", "string")),

      "dissociateDoc" => array(
          array("string", "string"))

    );

    // Get a list of input parameter types
    $input_signature = array();
    foreach ($params as $idx => $p) {
      $input_signature[] = xmlrpc_get_type($p);
      // Workaround for empty-struct bug in XMLRPC libs
      if ($method == 'replaceFileMetadata') {
        if (xmlrpc_get_type($p) == 'array') {
          if (empty($params[$idx])) {
            $input_signature[$idx] = 'struct';
          }
        }
      }
    }

    if (!isset($method_signatures[$method])) {
      throw new Exception("validateSignature: Missing signature for " . $method, BROKER_SYSTEM_EXCEPTION);
    }

    // Does the input type list match any of our known signatures?
    foreach ($method_signatures[$method] as $sig) {
      if ($input_signature === $sig) {	// List compare
        return TRUE;	# Found a match
      }
    }
    // Couldn't find a signature that matched
    $badSig = implode(" ", $input_signature);
    throw new Exception("validateSignature: No method signature matches ${method} [${badSig}]", XMLRPC_BAD_PARAMETERS);
  }

  /*
   * Return a proxy object for the requested service
   *
   * If multiple proxies exist, there ought to be some clever logic to
   * determine which one is returned--right now the first in the list is
   * always returned.
   *
   * Throw an exception if no servers are available.
   */
  function getProxy($service) {
    global $serviceList;

    $proxyList = $serviceList[$service];
    if (isset($proxyList)) {
      return $proxyList[0];
    } else {
      throw new Exception("No server available", NO_SERVER_AVAIL_EXCEPTION);
    }
  }

  /*
   *  Create "tmpdir" at app level directory
   *  Log files and data/metadata (for dummy proxy) stored here
   */
  function mkTmpDir() {
    global $TMPDIR;

    // Store file
    if (!is_dir($TMPDIR)) {

      $oldmask = umask(0);
      $dirMade = mkdir($TMPDIR);
      umask($oldmask);

      if (!$dirMade) {
        throw new Exception("Can't create storage directory",
          BROKER_SYSTEM_EXCEPTION);
      }
    }
  }

  /*
   *  Generate a short (printable) unique ID
   *  This makes an ID about 15 chars long
   *  Previous version had a resolution of seconds, this turned out to be
   *  too short in some cases
   *  * Maximum "date" value is PHP_INT_MAX, or 2147483647
   *  * I don't think max process ID ever gets that large, but I don't know
   *  * what type it is nor if there's some kind of range limit
   */
  function mkUniqID() {
    $microtime = microtime();
    $parts = explode(' ', $microtime);
    $micro = $parts[0] * 1000000;
    $seconds = $parts[1];
    $pid = getmypid();
    $cvtSeconds = base_convert($seconds, 10, 36);
    $cvtMicro = base_convert($micro, 10, 36);
    $cvtPid = base_convert($pid, 10, 36);
    return "${cvtSeconds}-${cvtMicro}-${cvtPid}";
  }

  /*
   *  Store summary log messages
   *  Messages include:
   *  - Current time
   *  - Requesting IP address
   *  - Summary of result
   *  - Time elapsed processing request
   *
   *  This really needs:
   *  - Time of request (current time)
   *  - Request action
   *  - Result status (success/failure)
   *  - Key to detail log
   *  It would probably also be useful if we only allowed one line per
   *  summary message.
   *
   * REQ_TIME|TXID|ACTION|STATUS|IPADDR|ELAPSED_TIME
   *
   * Ex: 2008/11/02 14:22:35 EST|k8j9lg-1j3|getFile|Success|127.0.0.1|0.3
   *
   * $message parameter should consist of the ACTION|STATUS.
   * Please keep STATUS brief!  Error details go into the detail log.
   */
  function logMsg($message) {
    global $TXID;
    global $START_TIME;
    static $fhandle = null;
  
    if (LOGGING) {
      if ($fhandle == null) {
        mkTmpDir();
        $fhandle = fopen(BROKER_SUMMARY_LOG, "a+");
      }
      $datetime = date('Y/m/d H:i:s T', $START_TIME);	# Request time
      $tag = $TXID;
      $remoteaddr = $_SERVER['REMOTE_ADDR'];
      $time_elapsed = round(microtime(TRUE) - $START_TIME, 3);
      $logEntry = "${datetime}|${tag}|${message}|${remoteaddr}|${time_elapsed}";
      fwrite($fhandle, "$logEntry\n");
    }
  }

  /*
   *  Store transaction detail log messages
   */
  function logDetail($tag, $message) {
    global $TXID;
    global $START_TIME;
    static $fhandle = null;
  
    if (LOG_TRANSACTIONS) {
      if ($fhandle == null) {
        mkTmpDir();
        $fhandle = fopen(BROKER_DETAIL_LOG, "a+");
      }
      $remoteaddr = "Requesting IP: " . $_SERVER['REMOTE_ADDR'];
      $datetime = "Request time: " . date('Y/m/d H:i:s T', $START_TIME);
      $tag = "$tag $TXID";
      $logEntry = "${remoteaddr}\n${datetime}\n$message";
      $logEntry = explode("\n", $logEntry);
      $logEntry = preg_replace('/^/', "$tag ", $logEntry);
      $logEntry = implode("\n", $logEntry);
      fwrite($fhandle, "$logEntry\n\n");
    }
  }

  /*
   *  Take a params object and flatten it into a string for logging
   *  If a struct, flatten into key:value pairs and return bracketed []
   *  If a datetime, get scalar value (PHP will abort on this type if not
   *  handled in this manner)
   *  If a scalar, return the string
   */
  function flatten_params($params) {
    $result = "";

    if (is_array($params)) {
      $param_cnt = count($params);
      $parts = array();
      foreach ($params as $key => $value) {
        $member = "$key:" . flatten_params($value);
        $parts[] = $member;
      }
      return "[" . implode(',', $parts) . "]";
    } else if (xmlrpc_get_type($params) == "datetime") {
      return $params->scalar;
    } else {
      return strval($params);
    }
  }

  /*
   *  Route the method request to the appropriate API handler method
   *
   *  The API handler method will have exactly the same name as the
   *  request method.  This is handled by the default case in the
   *  switch() statement below.
   *
   *  Special cases are written out for APIs that have unique requirements--
   *  in most cases this is because the associated log messages must be
   *  redacted due to space limitations (e.g. the request contains an
   *  encoded binary file which would otherwise be logged in its entirety).
   *
   *  The default logic is to:
   *
   *  - Wrap up unhandled exceptions in a generic fault object
   *  - Write the request and response messages to the detail log
   *  - Write an entry into the summary log
   *  - Print the response XMLRPC message (gets sent back to requester)
   *
   *  Because all requests are mapped to this method, it can log the
   *  summary messages, recording either success or failure
   */
  function dispatch($method, $params, $appInfo) {
    global $REQUEST_XML;
    logMsg("broker.dispatch - ".$method);
    try {
      validateSignature($method, $params);
    } catch (Exception $e) {
      // Problem validating requested method or arguments
      $errCode = $e->getCode();
      $errMsg = $e->getMessage();
      $response = makeFault($errCode, $errMsg);

      # Log request
      logDetail("request", prettifyRequest($REQUEST_XML));

      # Log response
      logDetail("response", xmlrpc_encode($response));

      # Log summary
      logMsg("${method}|FAILURE - INVALID");

      return $response;
    }

    $response = makeFault(BROKER_SYSTEM_EXCEPTION,
      "Dispatch: No response from handler?");

    $logged_params = "";

    try {

      switch ($method) {
      case "getFile":
        logDetail("request", prettifyRequest($REQUEST_XML));
        $logged_params = flatten_params($params);
        $response = getFile($params);
        if (isFault($response)) {
          logDetail("response", xmlrpc_encode_request(NULL, $response));
        } else {
          $editResponse = array(
            "file"     => "[FILE DATA]",
            "metadata" => $response["metadata"]
          );
          logDetail("response", xmlrpc_encode_request(NULL, $editResponse));
        }
        break;
      case "getFileData":
        logDetail("request", prettifyRequest($REQUEST_XML));
        $logged_params = flatten_params($params);
        $response = getFileData($params);
        if (isFault($response)) {
          logDetail("response", xmlrpc_encode_request(NULL, $response));
        } else {
          $editResponse = array(
            "file" => "[FILE DATA]"
          );
          logDetail("response", xmlrpc_encode_request(NULL, $editResponse));
        }
        break;
      case "putFile":
        # Make a redacted version of the parameter list so that
        # the detail log is not huge
        $modified_params = array("[FILE DATA]", array_slice($params, 1));
        $MODIFIED_REQUEST_XML = xmlrpc_encode_request("putFile",
                                $modified_params);
        logDetail("request", $MODIFIED_REQUEST_XML);
        $response = putFile($params);
        $modified_params[] = "response:$response";
        $logged_params = flatten_params($modified_params);
        logDetail("response", xmlrpc_encode_request(NULL, $response));
        break;
      case "transferFile":
        # Make a redacted version of the parameter list so that
        # the detail log is not huge
        $modified_params = array($params[0], "[FILE DATA]", array_slice($params, 2));
        $MODIFIED_REQUEST_XML = xmlrpc_encode_request("transferFile",
                                $modified_params);
        logDetail("request", $MODIFIED_REQUEST_XML);
        $response = transferFile($params);
        $logged_params = flatten_params($modified_params);
        logDetail("response", xmlrpc_encode_request(NULL, $response));
        break;
      case "searchDocs":
        $editRequest = prettifyRequest($REQUEST_XML);
        if (LOG_SEARCHREQ == False) {
          $pattern = "/(<name>projectids<\/name>\s*<value>\s*)(<array>.*<\/array>)?(\s*<\/value>)/s";
          $editRequest = preg_replace($pattern,
            "\\1(...list of project IDs...)\\3", $editRequest);
        }
        logDetail("request", $editRequest);
        $logged_params = flatten_params($params);
        if (strlen($logged_params) > 240) {
          $logged_params = "Long params redacted";
        }
        $response = searchDocs($params);
        if (isFault($response)) {
          logDetail("response", xmlrpc_encode_request(NULL, $response));
        } else {
          $editResponse = $response;	# Use this to log full search responses
          if (LOG_SEARCHRESP == False) {
            $editResponse = array(
              "resultmetadata" => $response["resultmetadata"],
              "hitlist" => "[HITLIST DATA]"
            );
          }
          logDetail("response", xmlrpc_encode_request(NULL, $editResponse));
        }
        break;
      default:
        // Should be ok--unrecognized methods should be caught
        // by validateSignature()
        logDetail("request", prettifyRequest($REQUEST_XML));
        $logged_params = flatten_params($params);
        $response = $method($params);
        logDetail("response", xmlrpc_encode_request(NULL, $response));
        break;
      }
    } catch (Exception $e) {
      // Fatal unhandled error
      $errCode = $e->getCode();
      $errMsg = "Unhandled error: " . $e->getMessage();
      $response = makeFault($errCode, $errMsg);
      logDetail("response", xmlrpc_encode_request(NULL, $response));
    }

    # Write summary log entry
    $summarylog = "${method}:${logged_params}";
    if (isFault($response)) {
      logMsg("${summarylog}|FAILURE");
    } else {
      logMsg("${summarylog}|SUCCESS");
    }

    return $response;
  }

  /*
   * MAIN
   *
   * Create and activate XMLRPC server
   */
  $server = xmlrpc_server_create();
  foreach (array_keys($serviceList) as $service) {
    xmlrpc_server_register_method($server, $service, "dispatch");
  }

  # $REQUEST_XML = $HTTP_RAW_POST_DATA;			# for PHP < 5.2.2
  // $REQUEST_XML = file_get_contents('php://input');	# (seems to help)
  $REQUEST_XML = file_get_contents('php://input');	# for PHP 5.2.2+

  /*
  ### IN CASE OF EMERGENCY, UNCOMMENT ***
  # open /usr/tmp/debuglog file based on ip addr and time
  $currtime = date('Ymd-His-T', $START_TIME);     # Request time
  $ipaddr = $_SERVER['REMOTE_ADDR'];
  $fname = "/usr/tmp/debuglog/${ipaddr}_${currtime}.debug";
  $fp = fopen($fname, "wb");
  # write request to tmp file
  fwrite($fp, $REQUEST_XML);
  fclose($fp);
  */

  $opts = array('output_type' => 'php');
  $resp = xmlrpc_server_call_method($server, $REQUEST_XML, '', $opts);

  if (isFault($resp)) {
    if ($resp['faultCode'] <= -32000) {
      $debugFname = storeDebugFile($REQUEST_XML, 'badrequest');
      logMsg("BAD_REQUEST $debugFname|FAILURE");
      logDetail("request", "Bad request--moved to " . $debugFname);
      logDetail("response", xmlrpc_encode($resp));
    }
  }

  # In the event that this routing fails, no error message is logged.
  # This is because I'm trying to avoid unconditionally decoding the
  # XMLRPC message in $resp, which could be the text of a retrieved file.

  print xmlrpc_encode_request(NULL, $resp);

  xmlrpc_server_destroy($xmlrpc_server);
?>

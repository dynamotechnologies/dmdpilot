<?php

  /*
   * File: dummyAPI.php
   *
   * APIs for dummy (test) functions
   */

  /*
   * Service: echoMsg
   * Parameters: msg (string)
   * Returns: echoedMsg (string)
   */
  function echoMsg($params) {
    // Get parameters from XMLRPC message
    $msg = $params[0];

    $retval = "???";

    // Locate server
    // Request service
    try {
      $proxy = getProxy('echoMsg');
      $retval = $proxy->echoMsg($msg);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->code;
      $errMsg = "Error handling echoMsg: " . $e->string;
      return makeFault($errCode, $errMsg);
      // print_r($e, TRUE);	// DEBUG
    }

    return $retval;
  }

  function helloWorld($params) {
    $proxy = getProxy('helloWorld');
    $retval = $proxy->helloWorld();
    return $retval;
  }
  




?>

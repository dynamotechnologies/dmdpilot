<?php
  /*
   * File: drmAPI.php
   *
   * APIs for DRM functions
   *
   * In general, business logic goes here
   *
   * These functions should not throw Exceptions, instead XMLRPC faults
   * should be returned with
   *   return makeFault($errCode, $errMsg);
   *
   * However, calls to service functions on the proxy objects will
   * use exceptions to send back errors.  These should be caught.
   */

  require_once realpath(dirname(__FILE__)) . "/../includes/consts.php";

  function zipPhaseDocs($params) {
  	 
  	$parama = $params[0];
  	$paramb = $params[1];
  	$paramc = $params[2];
  	logMsg("Param 0 = ".$parama." Param 1 = ".$paramb." Param 2 = ".$paramc);
  	try{
  		$proxy = getProxy('zipPhaseDocs');
  		$retval = $proxy->zipPhaseDocs($parama, $paramb, $paramc);
  	} catch (Exception $e) {
  		//  		// Error processing request
  		logMsg("Err ".$e);
        $errCode = $e->getCode();
  		$errMsg = "Error handling zipLink: " . $e->getMessage();
  		return makeFault($errCode, $errMsg);
  
  		// print_r($e, TRUE);	// DEBUG
  	}
  
  	return $retval;
  }
  
  function associateDoc($params) {
  
  	$parama = $params[0];
  	$paramb = $params[1];
  	logMsg("associate Doc : Param 0 = ".$parama." Param 1 = ".$paramb);
  	try{
  		$proxy = getProxy('associateDoc');
  		$retval = $proxy->associateDoc($parama, $paramb);
  	} catch (Exception $e) {
  		//  		// Error processing request
  		logMsg("Err ".$e);
        $errCode = $e->getCode();
      	$errMsg = "Error handling associateDoc request: " . $e->getMessage();
        if ($errCode == FILETYPE_NOT_FOUND_EXCEPTION) {
          $errMsg = "Error handling associateDoc request: " . FILETYPE_NOT_FOUND_MSG;
        }
      return makeFault($errCode, $errMsg);
  	}
  
  	return $retval;
  }

  function dissociateDoc($params) {
  
  	$projectid = $params[0];
  	$fileid = $params[1];
  	try{
  		$proxy = getProxy('dissociateDoc');
  		$retval = $proxy->dissociateDoc($projectid, $fileid);
  	} catch (Exception $e) {
  		//  		// Error processing request
  		logMsg("error ".$e->string);
  		$errCode = $e->code;
  		$errMsg = "Error in dissociateDoc: " . $e->string;
  		return makeFault($errCode, $errMsg);
  
  		// print_r($e, TRUE);	// DEBUG
  	}
  
  	return $retval;
  }
  
  /*
   * Service: getFile
   * Parameters: fileID (string)
   *             options (struct)
   * Returns: struct {
   *            file (base64)
   *            metadata (struct)
   *          }
   */
  function getFile($params) {
    // Get parameters
    $fileID = $params[0];
    $options = array();
    if (count($params) > 1) {
      $reqOptions = $params[1];
      foreach ($reqOptions as $name => $val) {
        switch ($name) {
        case OPT_FILETYPE:
          $options[$name] = $val;	// Copy to internal options array
          break;
        default:
          // Bad option
          $errCode = INVALID_OPTION_EXCEPTION;
          $errMsg = "Bad option: " . htmlspecialchars($name);
          return makeFault($errCode, $errMsg);
        }
      }
    }

    // Create output parameters--these will be passed by reference
    $fileDataOut = "";
    $metadataOut = array();

    // Locate server
    // Request service
    try {
      $proxy = getProxy('getFile');
      $proxy->getFile($fileID, $options, $fileDataOut, $metadataOut);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling getFile request: " . $e->getMessage();
      if ($errCode == FILETYPE_NOT_FOUND_EXCEPTION) {
        $errMsg = "Error handling getFile request: " . FILETYPE_NOT_FOUND_MSG;
      }
      return makeFault($errCode, $errMsg);
    }

    // Build response
    xmlrpc_set_type($fileDataOut, "base64");

    $resp = array("file"     => $fileDataOut,
                  "metadata" => $metadataOut);
    return $resp;
  }

  /*
   * Service: getFileData
   * Parameters: fileID (string)
   *             options (struct)
   * Returns: file (base64)
   */
  function getFileData($params) {
    // Get parameters
    $fileID = $params[0];
    $options = array();

    if (count($params) > 1) {
      $reqOptions = $params[1];
      foreach ($reqOptions as $name => $val) {
        switch ($name) {
        case OPT_FILETYPE:
          $options[$name] = $val;	// Copy to internal options array
          break;
        default:
          // Bad option
          $errCode = INVALID_OPTION_EXCEPTION;
          $errMsg = "Bad option: " . htmlspecialchars($name);
          return makeFault($errCode, $errMsg);
        }
      }
    }

    $fileDataOut = "";

    // Locate server
    // Request service
    try {
      $proxy = getProxy('getFileData');
      $fileDataOut = $proxy->getFileData($fileID, $options);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling getFileData request: " . $e->getMessage();
      if ($errCode == FILETYPE_NOT_FOUND_EXCEPTION) {
        $errMsg = "Error handling getFileData request: " . FILETYPE_NOT_FOUND_MSG;
      }
      return makeFault($errCode, $errMsg);
    }

    // Build response
    xmlrpc_set_type($fileDataOut, "base64");
    return $fileDataOut;
  }

  /*
   * Service: getFileMetadata
   * Parameters: fileID (string)
   *             options (struct)
   * Returns: metadata (struct)
   */
  function getFileMetadata($params) {
    // Get parameters
    $fileID = $params[0];

    $options = array();
    if (count($params) > 1) {
      $reqOptions = $params[1];
      foreach ($reqOptions as $name => $val) {
        switch ($name) {
        case OPT_FILETYPE:
          $options[$name] = $val;	// Copy to internal options array
          break;
        default:
          // Bad option
          $errCode = INVALID_OPTION_EXCEPTION;
          $errMsg = "Bad option: " . htmlspecialchars($name);
          return makeFault($errCode, $errMsg);
        }
      }
    }

    $resp = array();

    // Locate server
    // Request service
    try {
      $proxy = getProxy('getFileMetadata');
      $resp = $proxy->getFileMetadata($fileID, $options);
    } catch (Exception $e) {
  		logMsg("Err ".$e);
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling getFileMetadata request: " . $e->getMessage();
      if ($errCode == FILETYPE_NOT_FOUND_EXCEPTION) {
        $errMsg = "Error handling getFileMetadata request: " . FILETYPE_NOT_FOUND_MSG;
      }
      return makeFault($errCode, $errMsg);
    }

    // Ensure that pubdate is returned as a datetime
    xmlrpc_set_type($resp['pubdate'], 'datetime');

    return $resp;
  }

  /*
   * Service: putFile
   * Parameters: file (base64)
   *             metadata (struct)
   * Returns: fileID (string)
   */
  function putFile($params) {
    // Get parameters
    $file = $params[0]->scalar;

    if (strlen($file) == 0) {
      return makeFault(EMPTY_FILE_EXCEPTION, "Empty file!");
    }

    $metadata = $params[1];

    try {
      validateMetadata($metadata);
      if (xmlrpc_get_type($metadata['pubdate']) == "datetime") {
        $metadata['pubdate'] = $metadata['pubdate']->scalar;
      }
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    /*
     * If this document appears to be an Aide
     * validate the NEPA Aide metadata values
     */
    try {
      if (isAide($metadata)) {
        validateAide($metadata);
      } else {
        validateNonAide($metadata);
      }
    } catch (Exception $e) {
      $errCode = $e->getCode();
      $errMsg = "Error handling putFile request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('putFile');
      $resp = $proxy->putFile($file, $metadata);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling putFile request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    /*
     * If this document appears to be an Aide
     * insert a UnitDocument record into the Datamart
     */
    if (isAide($metadata)) {
      // TBD - call markNEPAAide or some variant just to create the record
    }

    return $resp;
  }

  /*
   * Service: deleteFile
   * Parameters: fileID (string)
   * Returns: true (boolean)
   */
  function deleteFile($params) {
    // Get parameters
    $fileID = $params[0];

    // Locate server
    // Request service
    try {
      $proxy = getProxy('deleteFile');
      $proxy->deleteFile($fileID);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      // If file is already deleted, treat this as OK instead of an error
      // Return (boolean) False instead of a fault
      // Note:  We used to return an XMLRPC fault with a FILE_NOT_FOUND
      //        message, but PALS did not process these correctly and we
      //        patched the code to return a non-fault response containing
      //	the error in a string.
      //	CARA had issues with the response being either a boolean or
      //	a string so we have changed the behavior to return a boolean
      //	False with no explanatory message.  (MBR-19, 12/21/2011)
      if ($errCode == FILE_NOT_FOUND_EXCEPTION) {
        return False;
      }
      $errMsg = "Error handling deleteFile request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return TRUE;
  }

  /*
   * Service: putMergedFile
   * Parameters: docIDArray (array)
   *             metadata (struct)
   * Returns: fileID (string)
   */
  function putMergedFile($params) {
    // Get parameters
    $docIDArray = $params[0];
    $metadata = $params[1];

    // Set default filename to new merged file
    $metadata['filename'] = 'merged.pdf';

    try {
      validateMetadata($metadata);
      if (xmlrpc_get_type($metadata['pubdate']) == "datetime") {
        $metadata['pubdate'] = $metadata['pubdate']->scalar;
      }
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    /* Add source document IDs to moreMetadata */
    $docIDstr = implode(',', $docIDArray);
    $metadata['moreMetadata']['_srcDocIDs'] = $docIDstr;

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('putMergedFile');
      $resp = $proxy->putMergedFile($docIDArray, $metadata);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling putMergedFile request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  /*
   * Service: replaceFileMetadata
   * Parameters: docID (string)
   *             metadata (struct)
   *             options (struct)
   * Returns: true (boolean)
   */
  function replaceFileMetadata($params) {
    //
    // Get parameters
    //
    $id = $params[0];
    $newMetadata = $params[1];	// New metadata members
    $options = array();
    if (count($params) > 2) {
      $options = $params[2];
    }

    //
    // Validate request parameters
    //
    try {
      // Make sure we only have the following members:
      // author, title, appdoctype, moreMetadata, appsysid
      $validMembers = array("author", "title", "appdoctype", "moreMetadata", "appsysid");
      $invalidMembers = array_diff(array_keys($metadata), $validMembers);
      if (count($invalidMembers) > 0) {
        throw new Exception(
          "Invalid metadata field: " . implode(", ", $invalidMembers),
          INVALID_METADATA_FIELD_EXCEPTION);
      }

      validateMetadata($metadata, FALSE);
      if (xmlrpc_get_type($metadata['pubdate']) == "datetime") {
        $metadata['pubdate'] = $metadata['pubdate']->scalar;
      }
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    //
    // Validate options
    //
    $mergeMode = False;
    $mmFieldsToDelete = array();
    foreach ($options as $name => $val) {
      switch ($name) {
      case OPT_MERGEMM:
        if (is_bool($val) == False) {
          $errCode = INVALID_OPTION_EXCEPTION;
          $errMsg = "Merge option must be a boolean";
          return makeFault($errCode, $errMsg);
        }
        if ($val === True) {
          $mergeMode = True;
        }
        break;
      case OPT_DELETEMM:
        if (is_array($val) == False) {
          $errCode = INVALID_OPTION_EXCEPTION;
          $errMsg = "Delete option must be an array";
          return makeFault($errCode, $errMsg);
        }
        $mmFieldsToDelete = $val;
        break;
      default:
        // Bad option
        $errCode = INVALID_OPTION_EXCEPTION;
        $errMsg = "Bad option: " . htmlspecialchars($name);
        return makeFault($errCode, $errMsg);
      }
    }

    //
    // Get current file metadata
    // (use PDF file to get current PDF file size)
    //
    $currentMD = null; 
    try {
      $pdfopt = array('fileType' => 'PDF');
      $proxy = getProxy('getFileMetadata');
      $currentMD = $proxy->getFileMetadata($id, $pdfopt);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = $e->getMessage();
      if ($errCode == FILETYPE_NOT_FOUND_EXCEPTION) {
        // Cannot modify document metadata until file is RELEASED
        return makeFault($errCode, "File conversion incomplete for ${id}, cannot modify metadata");
      } else {
        return makeFault($errCode, "Error handling replaceFileMetadata request: ${errMsg}");
      }
    }

    $moreMetadata = $currentMD["moreMetadata"];

    //
    // Delete MMD fields
    //
    foreach ($mmFieldsToDelete as $fieldname) {
      if (array_key_exists($fieldname, $moreMetadata)) {
        unset($moreMetadata[$fieldname]);
      }
    }

    //
    // Merge MMD fields
    //
    if ($mergeMode) {
      if (isset($newMetadata["moreMetadata"])) {
        // I don't trust the behavior of PHP array_merge()
        foreach ($newMetadata["moreMetadata"] as $key => $val) {
          $moreMetadata[$key] = $val;
        }
      }
    } else {
      // Note this undoes any delete requests previously applied
      if (isset($newMetadata["moreMetadata"])) {
        $moreMetadata = $newMetadata["moreMetadata"];
      }
    }

    //
    // Build complete metadata struct by merging all fields
    //
   $metadata = array();
   $fieldsToMerge = array("title",
                          "author",
                          "pubdate",
                          "appsysid",
                          "appdoctype",
                          "filename",	// retrieved from Stellent
                          "filesize");	// retrieved from Stellent
   foreach ($fieldsToMerge as $field) {
     $metadata[$field] = (isset($newMetadata[$field])) ? $newMetadata[$field] : $currentMD[$field];
   }

   $metadata['moreMetadata'] = $moreMetadata;

   //
   // Make sure request does not change the document status from Aide to
   // non-Aide, or vice versa
   //
   if (isAide($currentMD) !== isAide($metadata)) {
      return makeFault(INVALID_OPTION_EXCEPTION,
        "Error: Request cannot alter NEPA Aide metadata");
   }

   try {
     if (isAide($metadata)) {
       validateAide($metadata);
     } else {
       validateNonAide($metadata);
     }
   } catch (Exception $e) {
     $errCode = $e->getCode();
     $errMsg = "Error handling replaceFileMetadata request: " . $e->getMessage();
     return makeFault($errCode, $errMsg);
   }

    // Locate server
    // Request service
    try {
      $proxy = getProxy('replaceFileMetadata');
      $retval = $proxy->replaceFileMetadata($id, $metadata, $options);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling replaceFileMetadata request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return TRUE;
  }

  /*
   * Function: validateMetadata
   * Parameters: metadata (struct)
   *             requireAllFields (boolean, defaults to TRUE)
   * Returns: Nothing
   * Notes: * Expects all fields to be present, this condition is relaxed
   *          if FALSE is submitted for the requireAllFields parameter
   *        * Throws exceptions when metadata is invalid:
   *            MISSING_METADATA_EXCEPTION
   *            TITLE_LENGTH_EXCEPTION
   *            FILENAME_LENGTH_EXCEPTION
   *            AUTHOR_LENGTH_EXCEPTION
   *            BAD_DATE_FORMAT_EXCEPTION
   */
  function validateMetadata($metadata,
                            $requireAllFields = TRUE) {

    $fileTypeBlacklist = array("", "ad", "adp", "asp", "bas", "bat", "chm", "cmd", "com", "cpl", "crt", "exe", "hlp", "hta", "inf", "ins", "isp", "js", "jse", "lnk", "mdb", "mde", "msc", "msp", "msi", "mst", "pcd", "pif", "reg", "scr", "sct", "shb", "shs", "url", "vb", "vbe", "vbs", "vss", "vst", "vsw", "ws", "wsc", "wsf", "wsh");

    $metadataFields = array(
      'filename',
      'title', 
      'author', 
      'pubdate', 
      'appsysid', 
      'appdoctype',
      'moreMetadata');

    // Look for invalid fields
    foreach (array_keys($metadata) as $fieldname) {
      if (!in_array($fieldname, $metadataFields)) {
        throw new Exception(
          "Invalid metadata field: " . $fieldname,
          INVALID_METADATA_FIELD_EXCEPTION);
      }
    }

    // Validate required metadata struct fields
    if ($requireAllFields == TRUE) {

      $missingFields = array();
      
      foreach ($metadataFields as $fieldname) {
        if (!array_key_exists($fieldname, $metadata)) {
          $missingFields[] = $fieldname;
        }
      }

      if (count($missingFields) > 0) {
        throw new Exception(
          "Missing metadata fields: " . join(', ', $missingFields),
          MISSING_METADATA_EXCEPTION);
      }
    }


    // Validate "filename" extension
    if (array_key_exists('filename', $metadata)) {
      $filename = $metadata['filename'];

      $matches = array();
      $cnt = preg_match("/\.([^\.]*)$/", $filename, $matches);

      if ($cnt == 0) {
        throw new Exception(
          "File name has no extension", DISALLOWED_FILE_TYPE_EXCEPTION);
      } else {

        $ext = strtolower(trim($matches[1]));
        if (in_array($ext, $fileTypeBlacklist)) {
          throw new Exception(
            "File has bad extension ." . $ext,
            DISALLOWED_FILE_TYPE_EXCEPTION);
        }
      }
    }


    // Validate "filename"
    if (array_key_exists('filename', $metadata)) {
      $filename	= $metadata['filename'];
      if (strlen(utf8_decode($filename)) > MAX_FILENAME_LENGTH) {
        throw new Exception(
          "Filename too long, may not exceed " . MAX_FILENAME_LENGTH . " characters",
          FILENAME_LENGTH_EXCEPTION);
      }
    }

    // Validate "title"
    if (array_key_exists('title', $metadata)) {
      $title    = $metadata['title'];
      if (strlen(utf8_decode($title)) > MAX_TITLE_LENGTH) {
        throw new Exception(
          "Title too long, may not exceed " . MAX_TITLE_LENGTH . " characters",
          TITLE_LENGTH_EXCEPTION);
      }
    }

    // Validate "author"
    if (array_key_exists('author', $metadata)) {
      $author   = $metadata['author'];
      if (strlen(utf8_decode($author)) > MAX_AUTHOR_LENGTH) {
        throw new Exception(
          "Author too long, may not exceed " . MAX_AUTHOR_LENGTH . " characters",
          AUTHOR_LENGTH_EXCEPTION);
      }
    }

    // Validate "pubdate"
    if (array_key_exists('pubdate', $metadata)) {
      $pubdate = $metadata['pubdate'];
      if (xmlrpc_get_type($pubdate) == "datetime") {
        $pubdate = $pubdate->scalar;
      }

      // Format should be YYYYMMDDTHH:MM:SS
      $format = '%Y%m%dT%H:%M:%S';
      $result = strptime($pubdate, $format);
      if (! $result) {
        throw new Exception(
          "Invalid date format: ${pubdate}",
          BAD_DATE_FORMAT_EXCEPTION);
      }
    }

    // Validate "moreMetadata"

    // For the most part we don't validate moreMetadata
    // but here's where we handle the exceptions:
    // moreMetadata['docdate'] must be in YYYY-MM-DDTHH:MM:SSZ format

    // TODO: Need a new function to validate moreMetadata when it is
    // required, i.e. putFile and putMergedFile.  moreMetadata is not
    // currently required for replaceFileMetadata

    // moreMetadata is still arbitrary, but if it includes a "projectid"
    // field then it must conform to the metadata normalization scheme.
    //
    // Normalization rules for different appsysids allow different 
    // fields to be omitted, so virtually none of the moreMetadata fields
    // are universally required.

    if (array_key_exists('moreMetadata', $metadata)) {

      $moreMetadata = $metadata['moreMetadata'];
      if (is_array($moreMetadata) == False) {
        throw new Exception(
          "moreMetadata has bad type: " . gettype($moreMetadata),
          INVALID_METADATA_FIELD_EXCEPTION);
      }

      if (array_key_exists('docdate', $moreMetadata)) {
        $docdate = $moreMetadata['docdate'];
        $format = '%Y-%m-%dT%H:%M:%SZ';
        $result = strptime($docdate, $format);
        if (! $result) {
          throw new Exception(
            "Invalid docdate format: ${docdate}",
            BAD_DATE_FORMAT_EXCEPTION);
        }
      }

/*
      // Limit field length to 255 chars per field
      foreach ($moreMetadata as $key=>$val) {
        // max 255 characters
        $lim = 255;
        if (strlen($val) > $lim) {
          throw new Exception(
            "moreMetadata field $key is too long! Reduce to $lim characters",
            INVALID_METADATA_FIELD_EXCEPTION);
        }
      }
*/
    }
  }

  /*
   * Given a metadata array, determine if this is an aide
   *
   * Returns True/False
   *
   * This just checks to see if a "unit" value exists in moreMetadata.
   * It does not guarantee that the aide metadata is valid.
   */
  function isAide($metadata) {
    if (array_key_exists("moreMetadata", $metadata) === False) {
      return False;
    }
    $mM = $metadata['moreMetadata'];
    if (array_key_exists("unit", $mM) === False) {
      return False;
    }
    return True;
  }

  /*
   * Ensure that aide record contains all necessary metadata fields
   *
   * Returns True, throws exceptions to indicate validation failures
   *
   * Set requireAll to True all aide fields are required, or to False
   * if aide fields are not all required (e.g. update)
   *
   * Set aideFieldsOnly to True if the metadata structure cannot contain
   * non-aide fields, or to False if we don't care about other fields
   */
  function validateAide($metadata, $requireAll=True, $aideFieldsOnly=False) {
      if (array_key_exists("moreMetadata", $metadata) === False) {
        throw new Exception(
          "Document is missing moreMetadata",
          MISSING_METADATA_EXCEPTION);
      }
      $moreMetadata = $metadata['moreMetadata'];
      $aideFields = array(
        'unit',			// unit code
        'analtype', 		// one of [CE|EA|EIS|All]
        'aidetype', 		// one of [Template|Checklist|Guidance|Example]
        'aidesummary',		// text, max 250 characters
        'avgrank',		// float
        'totalreviews');	// int

      // Ensure no non-aide fields are present (if required)
      if ($aideFieldsOnly === True) {
          $unwanted_keys = array_keys($metadata);
          $mmkey = array_search('moreMetadata', $unwanted_keys);
          unset($unwanted_keys[$mmkey]);
          if (count($unwanted_keys) > 0) {
              $badfields = implode(", ", $unwanted_keys);
              throw new Exception(
                "NEPA aide contains extra properties in metadata: $badfields",
                INVALID_METADATA_FIELD_EXCEPTION);
          }

          $unwanted_keys = array();
          foreach (array_keys($moreMetadata) as $fieldname) {
              $key = array_search($fieldname, $aideFields);
              if ($key === False) {
                  $unwanted_keys[] = $fieldname;
              }
          }
          if (count($unwanted_keys) > 0) {
              $badfields = implode(", ", $unwanted_keys);
              throw new Exception(
                "NEPA aide contains extra properties in moreMetadata: $badfields",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      // Ensure all fields are present (if required)
      if ($requireAll === True) {
          foreach (array_keys($moreMetadata) as $fieldname) {
              $key = array_search($fieldname, $aideFields);
              if ($key !== False) {
                  unset($aideFields[$key]);
              }
          }
          if (count($aideFields) > 0) {
              $badfields = implode(", ", $aideFields);
              throw new Exception(
                "NEPA aide document is missing metadata : $badfields",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      // Validate field types
      if (array_key_exists('unit', $moreMetadata)) {
          $unit = $moreMetadata['unit'];
          // unit code
          $pattern = "/^(\d\d){2,4}$/";	// dddd, dddddd, or dddddddd
          if (preg_match($pattern, $unit) === 0) {
              throw new Exception(
                "Unit ID has bad format : $unit",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      if (array_key_exists('analtype', $moreMetadata)) {
          $analtype = $moreMetadata['analtype'];
          // one of [CE|EA|EIS|All]
          $pattern = "/^((CE)|(EA)|(EIS)|(All))$/";
          if (preg_match($pattern, $analtype) === 0) {
              throw new Exception(
                "Analysis type has bad value : $analtype",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      if (array_key_exists('aidetype', $moreMetadata)) {
          $aidetype = $moreMetadata['aidetype'];
          // one of [Template|Checklist|Guidance|Example]
          $pattern = "/^((Template)|(Checklist)|(Guidance)|(Example))$/";
          if (preg_match($pattern, $aidetype) === 0) {
              throw new Exception(
                "Aide type has bad value : $aidetype",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      if (array_key_exists('aidesummary', $moreMetadata)) {
          $aidesummary = $moreMetadata['aidesummary'];
          // text, max 250 characters
          $lim = 250;
          if (strlen($aidesummary) > $lim) {
              throw new Exception(
                "Aide summary is too long! Reduce to $lim characters",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      if (array_key_exists('avgrank', $moreMetadata)) {
          $avgrank = $moreMetadata['avgrank'];
          // float
          if (is_numeric($avgrank) === False) {
              throw new Exception(
                "Average rank has non-numeric format: $avgrank",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      if (array_key_exists('totalreviews', $moreMetadata)) {
          $totalreviews = $moreMetadata['totalreviews'];
          // int
          if (is_numeric($totalreviews) === False) {
              throw new Exception(
                "Total reviews has non-numeric format: $totalreviews",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
          if (intval($totalreviews) != $totalreviews) {
              throw new Exception(
                "Total reviews has non-integer format: $totalreviews",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }

      return True;
  }

  /*
   * Ensure that non-aide record does not tamper with aide metadata fields
   *
   * Returns True, throws exceptions to indicate validation failures
   */
  function validateNonAide($metadata) {
      if (array_key_exists("moreMetadata", $metadata) === False) {
        throw new Exception(
          "Document is missing moreMetadata",
          MISSING_METADATA_EXCEPTION);
      }
      $moreMetadata = $metadata['moreMetadata'];
      $aideFields = array(
        'unit',			// unit code
        'analtype', 		// one of [CE|EA|EIS|All]
        'aidetype', 		// one of [Template|Checklist|Guidance|Example]
        'aidesummary',		// text, max 250 characters
        'avgrank',		// float
        'totalreviews');	// int

      // Ensure no aide fields are present
      foreach (array_keys($moreMetadata) as $fieldname) {
          $key = array_search($fieldname, $aideFields);
          if ($key !== False) {
              throw new Exception(
                "Tampering with reserved aide field : $fieldname",
                INVALID_METADATA_FIELD_EXCEPTION);
          }
      }
      return True;
  }

  /*
   * Service: publishFile
   * Parameters: agent (string)
   *             docID (string)
   *             options (struct)
   * Returns: fileID (string)
   */
  function publishFile($params) {
    // Get parameters
    $agent = $params[0];
    $docID = $params[1];
    $options = array();
    if (count($params) > 2) {
      $options = $params[2];
    }

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('publishFile');
      $resp = $proxy->publishFile($agent, $docID, $options);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling publishFile request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  /*
   * Service: transferFile
   * Parameters: agent (string)
   *             file (base64)
   *             options (struct)
   * Returns: true (boolean)
   *
   * Note that unlike putFile, this function never raises
   * the empty file exception
   */
  function transferFile($params) {
    // Get parameters
    $agent = $params[0];
    $file = $params[1]->scalar;
    $options = $params[2];
    if (count($params) > 2) {
      $options = $params[2];
    }

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('transferFile');
      $resp = $proxy->transferFile($agent, $file, $options);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling transferFile request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  /*
   * This function validates that:
   * 1. All required options are present
   * 2. All options are of the correct type
   * 3. No options are unaccounted for
   *
   * Options is the array to be tested
   *
   * The conditions array has the following format:
   * {
   *   name => { test, required }
   * }
   *
   * test is the name of a predicate function (returns T/F) that verifies
   *   the type of the named option
   * required is a boolean that indicates if the field must be present
   */
  function validateOptions($options, $conditions) {

    /* Make sure required options are present */
    foreach (array_keys($conditions) as $key) {
      if ($conditions[$key][1]) {
        if (array_key_exists($key, $options) == False) {
          throw new Exception(
            "Missing option: " . $key, INVALID_OPTION_EXCEPTION);
        }
      }
    }

    $optionsChecked = Array();

    /* Make sure all options are of the correct type */
    foreach (array_keys($options) as $option) {
      if (array_key_exists($option, $conditions)) {
        array_push($optionsChecked, $option);
        
        $test = $conditions[$option][0];
        $req  = $conditions[$option][1];
        if ($test($options[$option]) == False) {
          throw new Exception(
            "Wrong type for option ${option}",
            INVALID_OPTION_EXCEPTION);
        }
      } else {
        /* Make sure no options are unaccounted for */
        throw new Exception(
          "Invalid option: ${option}", INVALID_OPTION_EXCEPTION);
      }
    }
  }

  /*
   * Service: searchDocs
   * Parameters: querystring (string)
   *             options (struct)
   * Returns: struct containing {
   *		resultmetadata (struct)
   *		hitlist (array)
   *		}
   *
   * Options allowed from spec are:
   * indexname (string, must be PALSDOCS)
   * countitems (int optional, defaults to 10)
   * startitem (int optional, defaults to 0)
   * pagenum (int optional, defaults to 0)
   * collapse (boolean optional, defaults to 0)
   * projectids (array int optional)
   *
   * collapse and projectids are intended to be specific to PALSDOCS.
   */
  function searchDocs($params) {
    // Get parameters
    $querystring = $params[0];
    $options = $params[1];

    // Validate options
    try {
      validateOptions($options,
        Array(
          OPT_INDEXNAME  =>	Array('is_string', True),
          OPT_COUNTITEMS =>	Array('is_int', False),
          OPT_STARTITEM  =>	Array('is_int', False),
          OPT_PAGENUM    =>	Array('is_int', False),
          OPT_COLLAPSE   =>	Array('is_bool', False),
          OPT_PROJECTIDS =>	Array('is_array', False)
      ));

      if ($options['indexname'] !== "PALSDOCS") {
        throw new Exception(
          "Invalid index name: " . $options['indexname'],
          INVALID_OPTION_EXCEPTION);
      }
    }
    catch (Exception $e) {
      $errCode = $e->getCode();
      $errMsg = $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('searchDocs');
      $resp = $proxy->searchDocs($querystring, $options);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling searchDocs request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  /*
   * Service: getNewFileList
   * Parameters: jobid (string)
   * Returns: struct containing {
   *		resultmetadata (struct)
   *		filelist (array)
   *		}
   */
  function getNewFileList($params) {
    // Get parameters
    $jobid = $params[0];

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('getNewFileList');
      $resp = $proxy->getNewFileList($jobid);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling getNewFileList request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  function createNEPAMailingList($params) {
    // Get parameters
    $palsid = $params[0];
    $shortname = $params[1];

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('createNEPAMailingList');
      $resp = $proxy->createNEPAMailingList($palsid, $shortname);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling createNEPAMailingList request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  /*
   * Service: markNEPAAide
   *
   * Verify input list only contains NEPA Aide moreMetadata fields
   * Fields are all required
   * Merges all NEPA Aide metadata
   * Creates Datamart record (if not already present)
   * (Does not alter metadata if record already present)
   */
  function markNEPAAide($params) {
    // Get parameters
    $docID = $params[0];
    $moreMetadata = $params[1];

    $resp = "CODE_ERROR";	# This should never get returned

    // Validate input
    try {
      $metadata = array('moreMetadata' => $moreMetadata);
      validateAide($metadata, True, True);
    } catch (Exception $e) {
      $errCode = $e->getCode();
      $errMsg = "Error handling markNEPAAide request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    // Proxy function will:
    // Merge with existing metadata
    // Create the datamart record if necessary

    // Locate server
    // Request service
    try {
      $proxy = getProxy('markNEPAAide');
      $resp = $proxy->markNEPAAide($docID, $moreMetadata);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling markNEPAAide request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  /*
   * Deletes all NEPA Aide metadata
   * Deletes Datamart record (if present)
   */
  function unmarkNEPAAide($params) {
    // Get parameters
    $docID = $params[0];

    $resp = "CODE_ERROR";	# This should never get returned

    // Locate server
    // Request service
    try {
      $proxy = getProxy('unmarkNEPAAide');
      $resp = $proxy->unmarkNEPAAide($docID);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling unmarkNEPAAide request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

  /*
   * Only NEPA aide metadata fields may be updated with this function
   * Request may not contain non-aide metadata fields
   */
  function updateNEPAAide($params) {
    // Get parameters
    $docID = $params[0];
    $moreMetadata = $params[1];

    $resp = "CODE_ERROR";	# This should never get returned

    try {
      $metadata = array('moreMetadata' => $moreMetadata);
      validateAide($metadata, False, True);
    } catch (Exception $e) {
      $errCode = $e->getCode();
      $errMsg = "Error handling updateNEPAAide request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    // Locate server
    // Request service
    try {
      $proxy = getProxy('updateNEPAAide');
      $resp = $proxy->updateNEPAAide($docID, $moreMetadata);
    } catch (Exception $e) {
      // Error processing request
      $errCode = $e->getCode();
      $errMsg = "Error handling updateNEPAAide request: " . $e->getMessage();
      return makeFault($errCode, $errMsg);
    }

    return $resp;
  }

?>

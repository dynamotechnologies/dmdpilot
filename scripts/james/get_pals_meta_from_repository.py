import sys
import urllib
import simplejson

# This script accepts a text file as input. The text file should contain a list of doc id's. 
# It queries the repository for each doc id and outputs additional metadata with the original list


#
#
#
# Update these globals before using this script
delimiter='|'
ucmurl='http://sysadmin:idc@vm-fs:8001/web/idcplg'
# ucmurl='http://sysadmin:p1cgorcl@localhost/web/idcplg'
# ucmurl='http://sysadmin:p1cgorcl@10.0.15.12/web/idcplg'
errorLabel = '--error--'
deletedLabel = '--deleted--'



# Print usage information for this script
def usage():
    print "Usage: get_pals_meta_from_repository <name of csv file to parse>"
    sys.exit()

# Retrieve the field specified by fieldname from xml string s
def getField(s, fieldname):
    if len(s)<=0 or len(fieldname)<=0:
        return ''
    start = s.find('<idc:field name=\"'+fieldname+'\">')
    if start>=0:
        start=start+19+len(fieldname)
    end = s[start:].find('</idc:field>')
    if start>0 and start<len(s) and (start+end)<len(s):
        return s[start:start+end]
    else:
        return ''

# Talk to the repository. Get the metadata for the content item specified by id
def getDocMeta(id):
    global ucmurl
    global delimiter
    global errorLabel
    global deletedLabel
    page = urllib.urlopen(ucmurl+'?IdcService=DOC_INFO_BY_NAME&dDocName='+id+'&IsSoap=1')
    xml = page.read()
 #   print xml
 #   sys.exit()

    if "<idc:field name=\"StatusCode\">" in xml and "Unable to find latest revision of" not in xml:
        xMoreMetadata=errorLabel
        xOriginalAuthor=errorLabel
        xPubDate=errorLabel
        xAppSystemID=errorLabel
        xAppDocumentType=errorLabel
    elif "Unable to find latest revision of" in xml:
        xMoreMetadata=deletedLabel
        xOriginalAuthor=deletedLabel
        xPubDate=deletedLabel
        xAppSystemID=deletedLabel
        xAppDocumentType=deletedLabel
    else:
        xMoreMetadata = getField(xml,'xMoreMetadata')
        xOriginalAuthor = getField(xml,'xOriginalAuthor')
        xPubDate = getField(xml,'xPubDate')
        xAppSystemID = getField(xml,'xAppSystemID')
        xAppDocumentType = getField(xml,'xAppDocumentType')
    page.close()
    return xMoreMetadata+delimiter+xOriginalAuthor+delimiter+xPubDate+delimiter+xAppSystemID+delimiter+xAppDocumentType

# Loop through all xMoreMetadata values and extract the key names
def getMmdKeys(d):
    global errorLabel
    global deletedLabel
    mmdKeys = list()
    for id in d.iterkeys():
        mmd = d[id][1].replace('&quot;', '"')
        if (errorLabel not in mmd and deletedLabel not in mmd) and ('[]' in mmd or mmd[0]=='{'):
            for r in simplejson.loads(mmd):
                if r not in mmdKeys: mmdKeys.append(r)
    return mmdKeys

# Extract and return a specific sub-field from xMoreMetadata
def getMmdVal(mmd, field):
    global errorLabel
    global deletedLabel
    if errorLabel in mmd: return errorLabel
    if deletedLabel in mmd: return deletedLabel
    if (mmd[0]=='{') and field in mmd:
        return simplejson.loads(mmd.replace('&quot;', '"'))[field]
    return ''

# Verify number of arguments
if len(sys.argv)>1:
    filename = sys.argv[1]
else:
    print "Error: Filename not specified."
    usage()

# Loop through the file and generate a dictionary of content items -> metadata
dict = {}
lines = open(filename).read().splitlines()
for line in lines:
    dDocName=line.split(delimiter)[1]
    if dDocName in dict:
        oldrow = dict[dDocName]
        dict[dDocName] = [oldrow[0],oldrow[1],oldrow[2],oldrow[3],oldrow[4],oldrow[5],oldrow[6]+1]
    else:
        line=line+delimiter+getDocMeta(dDocName)
        dict[dDocName] = [line.split(delimiter)[0],
            line.split(delimiter)[2],
            line.split(delimiter)[3],
            line.split(delimiter)[4],
            line.split(delimiter)[5],
            line.split(delimiter)[6],
            1]

mmdKeys = getMmdKeys(dict)

# Print the header
print ''
header = 'ActionDate|dDocName|xMoreMetadata|xOriginalAuthor|xPubDate|xAppSystemID|xAppDocumentType|NumDownloads'
for m in mmdKeys:
    header += '|'+m
print header

# Loop through the dictionary and print each entry, expanding sub-fields from xMoreMetadata
for key in dict.iterkeys():
    line = dict[key][0]+delimiter+key+delimiter+dict[key][1]+delimiter+dict[key][2]+delimiter+dict[key][3]+delimiter+dict[key][4]+delimiter+dict[key][5]+delimiter+str(dict[key][6])
    for k in mmdKeys:
        line += delimiter+getMmdVal(dict[key][1], k)
    print line



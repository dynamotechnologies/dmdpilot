set echo off
set feedback off
set linesize 200
set pagesize 0
set sqlprompt ''
set trimspool on
spool palsdoc_pdf_sizes.csv

SELECT dDocName||'|'||dExtension||'|'||dFileSize||'|'||xMoreMetadata||'|'||xOriginalAuthor||'|'||xPubDate||'|'||xAppSystemID||'|'||xAppDocumentType
FROM Revisions r INNER JOIN DocMeta m
ON r.did=m.did
INNER JOIN Documents d
ON r.did=d.did
WHERE dRevRank=0 and 
( xAppDocumentType like 'Appeal Documents' ) and dIsPrimary != 1
ORDER BY dFileSize DESC;

spool off

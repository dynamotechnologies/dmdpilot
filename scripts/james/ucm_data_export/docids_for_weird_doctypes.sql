set echo off
set feedback off
set linesize 200
set pagesize 0
set sqlprompt ''
set trimspool on
spool docids_for_weird_doctypes.csv

select dDocName
from revisions r inner join docmeta m
on r.did=m.did
where drevrank=0 
and xappdocumenttype like 'EIS'
or xappdocumenttype like 'PDF'
or xappdocumenttype like 'application/msword'
or xappdocumenttype like 'application/octet-stream'
or xappdocumenttype like 'application/pdf'
or xappdocumenttype like 'bmp'
or xappdocumenttype like 'doc'
or xappdocumenttype like 'docx'
or xappdocumenttype like 'jpg'
or xappdocumenttype like 'pdf'
or xappdocumenttype like 'pps'
or xappdocumenttype like 'rtf'
or xappdocumenttype like 'shtml'
or xappdocumenttype like 'xls';

spool off

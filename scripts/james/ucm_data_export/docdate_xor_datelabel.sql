set echo off
set feedback off
set linesize 200
set pagesize 0
set sqlprompt ''
set trimspool on
spool docdate_xor_datelabel.csv

SELECT dDocName||'|'||xMoreMetadata||'|'||xOriginalAuthor||'|'||xPubDate||'|'||xAppSystemID||'|'||xAppDocumentType
FROM Revisions r INNER JOIN DocMeta m
ON r.did=m.did
WHERE dRevRank=0 and 
(
((xMoreMetadata like '%datelabel":""%' or
xMoreMetadata not like '%datelabel%')
and
xMoreMetadata like '%docdate%' and
xMoreMetadata not like '%docdate":""%')
or
((xMoreMetadata like '%docdate":""%' or
xMoreMetadata not like '%docdate%')
and
xMoreMetadata like '%datelabel%' and
xMoreMetadata not like '%datelabel":""%')
);

spool off

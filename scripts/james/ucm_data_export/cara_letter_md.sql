
set echo off
set feedback off
set linesize 9999
set pagesize 0
set sqlprompt ''
set trimspool on
spool cara_letter_md.csv

select r.ddocname||'|'||r.ddoctitle||'|'||d.doriginalname||'|'||m.xmoremetadata
from Revisions r, DocMeta m, Documents d
where r.did = d.did and d.did = m.did
and d.disprimary=1
and m.xappsystemid like '6' and m.xmoremetadata like '%caraletterid%'
order by r.ddocname;

spool off


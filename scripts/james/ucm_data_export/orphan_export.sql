set echo off
set feedback off
set linesize 200
set pagesize 0
set sqlprompt ''
set trimspool on
spool orphans.csv

SELECT dDocName||'|'||xMoreMetadata||'|'||xOriginalAuthor||'|'||xPubDate||'|'||xAppSystemID||'|'||xAppDocumentType
FROM Revisions r INNER JOIN DocMeta m
ON r.did=m.did
WHERE dRevRank=0 and 
(xAppDocumentType not like 'Scoping Documents' and
xAppDocumentType not like 'Appeal Documents' and
xAppDocumentType not like 'Decision Documents' and
xAppDocumentType not like 'Analysis Documents' and
xAppDocumentType not like 'Supporting Documents');

spool off


set echo off
set feedback off
set linesize 2000
set pagesize 0
set sqlprompt ''
set trimspool on
spool non_pdf_documents.csv

select d.dextension||'|'||r.ddocname||'|'||m.xMoreMetadata from REVISIONS r, DOCMETA m, DOCUMENTS d
where r.DID = m.DID
and d.DID = r.DID
and r.DREVRANK = 0
and d.DISPRIMARY=0
and d.dextension not like 'pdf'
order by d.dextension, r.ddocname;

spool off

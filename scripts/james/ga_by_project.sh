#!/usr/bin/python

import sys

def usage():
    print "Usage: ga_by_project.py <name of csv file to parse>"
    sys.exit()

def collapse(biglist):
    newlist = []
    countlist = []
    for a in range(0,len(biglist)):    # Loop through original list.
        countlist.append(biglist[a][0])  # Countlist is used to count duplicates.
        if a==0: 
            newlist.append(biglist[a])  # Newlist is going to be the return value
            continue
        match=0
        for b in range(0, len(newlist)):  # For each row in biglist, 
            if newlist[b][0]==biglist[a][0]:   # check to see if it already exists in newlist.
                match=1; break;
        if match:
            for c in range(1,6): # If so, add the values together (no duplicates allowed in newlist).
                newlist[b][c] = str(float(newlist[b][c]) + float(biglist[a][c]))
       #         print "DEBUG: ",newlist
        else:
            newlist.append(biglist[a]) # Otherwise, just add the current row to newlist.
    for d in range(0, len(newlist)):  # Then go through newlist (columns 3-6),
        for e in range(3,6):   #  and average out all those rows we added together. Column 0 is the project id and 
            newlist[d][e]=float(newlist[d][e])/float(countlist.count(newlist[d][0]))  #   columns 1-2 are aggregate.
    return newlist


if len(sys.argv)>1:
    filename = sys.argv[1]
else:
    print "Error: Filename not specified."
    usage()

output = []
FILE = open(filename,"r")
while FILE:
    line = FILE.readline()
    if len(line)==0: break;
    vals = line.split(',')
    tmp = vals[0].split("?project=")
    if len(tmp)>1 and len(tmp[1])>0 and tmp[1].isdigit():
        vals[0] = tmp[1]
        output.append(vals)
    else:
 #       print "Line ignored: " + line
        continue
FILE.close()

output = collapse(output)
print "Project,Pageviews,Unique Pageviews,Avg. Time on Page,Bounce Rate,% Exit",
for o in output:
    print ""
    for n in range(0,6):
        print str(o[n])+",",




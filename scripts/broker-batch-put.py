import os
import base64
import mechanize


# curl -i -X POST -H 'Content-Type: text/xml' -u "br0k3r:s3rv1c3" -d @payload.xml http://dmd-tst.ecosystem-management.org/drmtest/broker/broker.php

def stalluser():
    var = raw_input("\nPress ENTER to continue.")

rootdir = 'docs'
author = 'James'
brokerpath = "http://dmd-tst.ecosystem-management.org/drmtest/broker/broker.php"
brokeruid = 'br0k3r'
brokerpwd = 's3rv1c3'
logfilename = 'out.log'

filenames = []
for item in os.listdir(rootdir):
    if os.path.isfile(os.path.join(rootdir, item)):
        filenames.append(item)

print '\n\nOkay, let\'s do this. Uploading '+ str(len(filenames)) + ' files to '+ brokerpath +'.\nA log of requests and responses will be written to \'' + logfilename + '\'.'
stalluser()

br = mechanize.Browser()
br.add_password(brokerpath, brokeruid, brokerpwd)
br.set_handle_robots(False)

logfile = open(logfilename,"w")
for f in filenames:
    print '\n'+f
    print 'Reading file...'
    infile = open(rootdir+'/'+f,"r")
    filecontents = infile.read()
    print 'Encoding file (base64)...'
    encodedcontents = base64.encodestring(filecontents)
    infile.close()
    data = '<?xml version="1.0"?><methodCall><methodName>putFile</methodName><params><param><value><base64>'+encodedcontents+'</base64></value></param><param><value><struct><member><name>appsysid</name><value><string>FS-EMC-PALS</string></value></member><member><name>appdoctype</name><value><string>TEST</string></value></member><member><name>filename</name><value><string>'+f+'</string></value></member><member><name>title</name><value><string>'+f+'</string></value></member><member><name>author</name><value><string>'+author+'</string></value></member><member><name>pubdate</name><value><string>20101127T00:00:00</string></value></member><member><name>moreMetadata</name><value><struct><member><name>projectID</name><value>0001</value></member><member><name>decisionID</name><value>101105</value></member></struct></value></member></struct></value></param></params></methodCall>'
    logfile.write("\n\nPAYLOAD:\n")
    logfile.write(data)
    print 'Sending request...'
    r = br.open(brokerpath, data)
    rdata = r.read()
    logfile.write("\n\nRESPONSE:\n")
    logfile.write(rdata)
    print rdata
    print 'Done with '+f
logfile.close()

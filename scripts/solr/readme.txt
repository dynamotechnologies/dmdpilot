
- Create user solr
sudo useradd -m solr

- Extract solr-dist to a central location

- Copy schema.xml to solr/conf/

- Rename/copy solrconfig.xml.master or solrconfig.xml.slave to solr/conf/solrconfig.xml

- Copy solrstart.sh and put it in the same directory as solr's start.jar file

- Set permissions on solrstart.sh
sudo chmod +x /efs/phe/emnepa/trial/solr/svc/startsolr.sh

- Edit init-solr and make sure JAVA_HOME and SOLR_HOME settings are valid

- Make the init script executable
chmod 755 /etc/init.d/solr

- Rename/copy init-solr to /etc/init.d/solr

- Set owner/group info
sudo chown -R solr /efs/phe/emnepa/trial/solr
sudo chgrp -R solr /efs/phe/emnepa/trial/solr

- Configure the service to start at boot
chkconfig --add solr
chkconfig solr on
chkconfig --list solr

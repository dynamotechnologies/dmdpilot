#!/bin/sh
#

# Select the appropriate instance name, which should match the directory name
ucminstance=coreprod
# ucminstance=coretest

# the path to the UCM installation directory
ucmdir=/app/ucm/$ucminstance

cd $ucmdir/admin/etc/
./idcadmin_start
cd $ucmdir/etc/
./idcserver_start

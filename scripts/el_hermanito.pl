#!/usr/bin/perl

#
# This is a monitor script we keep running on a local POCG server to check
# the status of the Broker, Stellent, and other services
#
# Sample crontab line: 5 7-19 * * * perl /var/www/el_hermanito.pl 2>&1 >> /tmp/hermanito.log
#

$now = localtime;
print "Started execution at $now\n";

use LWP::Simple;
use POSIX qw(strftime);
$SENDMAIL = "/usr/sbin/sendmail -t";
# $URL = 'http://localhost/checkit.php';
$URL = 'http://br0k3r:s3rv1c3@data.ecosystem-management.org/checkit.php';

my $date = strftime('%Y/%m/%d', localtime);
my $datetime = strftime('%Y/%m/%d %H:%M:%S', localtime);
$SUBJECT = "Subject: el_hermanito - System alert for dmdpilot $date";
$REPLY_TO = "Reply-to: mhsu\@phaseonecg.com";
# $SEND_TO = "To: mhsu\@phaseonecg.com, jshields\@phaseonecg.com";
# $SEND_TO = "To: deokman\@gmail.com, james\@makaisoftware.com, sam.collins\@gmail.com, fs-drm-alerts\@phaseonecg.com";
# $SEND_TO = "To: deokman\@gmail.com, james\@makaisoftware.com, sam.collins\@gmail.com";
# $SEND_TO = "To: mhsu\@phaseonecg.com";
# $SEND_TO = "To: deokman\@gmail.com";
$SEND_TO = "To: deokman\@gmail.com, sam.collins\@gmail.com";

# Grab web page /var/www/html/checkit.php
my $content = get $URL;

# die "Couldn't get $URL" unless defined $content;
$content = "No response from $URL" unless defined $content;

# Copy to /var/www/html/checkit/pilot-latest.html
open FILE, ">/var/www/html/checkit/pilot-latest.html";
print FILE "<div>$datetime<div>";
print FILE "$content";
close FILE;

# Scan for "We Have Problems!"
# if ($content =~ m/We\ Have\ Problems/) {
if ($content !~ m/Everything is functioning OK/) {

  # Email to james and me
  open (SENDMAIL, "|$SENDMAIL") or die "Cannot open $SENDMAIL $!";
  print SENDMAIL "$REPLY_TO\n";
  print SENDMAIL "$SUBJECT\n";
  print SENDMAIL "$SEND_TO\n";
  print SENDMAIL "Content-type: text/html\n\n";
  print SENDMAIL "$content";
  close (SENDMAIL);

  print "Error condition, message sent\n";

} else {

  print "No message sent\n";

}

$now = localtime;
print "Finished execution at $now\n";


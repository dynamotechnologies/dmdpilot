#!/usr/bin/python

import os
import time
import boto

#
# This script requires the boto python library for Amazon Web Services.
#    svn checkout http://boto.googlecode.com/svn/trunk/ /u001/packages/boto
#    sudo python setup.py install


# Constants
access_key_id="1JTETT70V8M3H7EZ6QG2"
secret_access_key="9tx4kfX7wJ9WR9vJQ5btsxKshJcWNQX6M3wjv+X7"
instance_id = 'i-e7d3659a'
volume_id = 'vol-33016349'
elastic_ip = '184.73.210.76'


# Connect to EC2
print "Connecting to EC2..."
ec2_conn = boto.connect_ec2(access_key_id, secret_access_key)

# TODO: detect errors connecting to EC2

# TODO: make sure the instance is not already started

# Start the instance
print "Starting instance " + instance_id
instances = ec2_conn.start_instances([instance_id])


inst = instances[0]
inst.update()
print "Sleeping for 15 minutes while instance boots up..."
time.sleep(900)
inst.update()

# TODO: Verify the instance started successfully
#	if inst.update() != u'running' die loudly


inst.use_ip(elastic_ip)
time.sleep(120)
# TODO: hande error if ip allocation not successful



# TODO: Verify connectivity


os.system("su - stellent -c '/app/ucm/coreprod/bin/IdcCommand -f /app/ucm/ucm_export.cfg -u sysadmin'")
# Expected output:
#    4/6/10: Success executing service EXPORT_ARCHIVE.


# TODO: Detect absence of string "Success executing service EXPORT_ARCHIVE." and die loudly

print "Sleeping for 40 minutes while content is transferred..."
time.sleep(2400)


# TODO: Detect archive transfer/import completion




# Stop the instance
ec2_conn.stop_instances([instance_id])
time.sleep(120)

# TODO: Verify the instance stopped successfully

# TODO: Update checkit.php


# Create snapshot
# Disabled in favor of the weekly snapshot script do_aws_snapshot.sh
#
# volumes = ec2_conn.get_all_volumes(volume_id)
# vol = volumes[0]
# snap = vol.create_snapshot('DMD-DR Daily Backup')


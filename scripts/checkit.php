<?php

/*
 *	1. Turn off page cache
 *	2. Run checkit_report.php (this produces the HTML output)
 *	3. If there was an error, write the output to /usr/tmp/checkit.log
 */
  $logfile		= "/usr/tmp/checkit.log";
//  $serverpath	= "http://localhost";
  $serverpath	= "http://br0k3r:s3rv1c3@localhost";

  $logfp = fopen($logfile, "w");
  if (!$logfp) {
    exit("Cannot open logfile $logfile!");
  }

  // Turn off page cache
  header("Cache-Control: no-cache, must-revalidate");
  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

  // Run report
  $output = file_get_contents($serverpath . "/checkit_report.php");

  // If there was an error, save a copy of the output
  if (!strpos($output, "functioning")) {	// Look for a keyword
    fprintf($logfp, "Last error occurred: " . date("Y-m-d h:i:s\n"));
    fprintf($logfp, $output);
  }
  print $output;
?>

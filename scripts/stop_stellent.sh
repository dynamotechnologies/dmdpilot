#!/bin/sh
#

# Select the appropriate instance name, which should match the directory name
ucminstance=coreprod
# ucminstance=coretest

# the path to the UCM installation directory
ucmdir=/app/ucm/$ucminstance

cd $ucmdir/etc/
./idcserver_stop
cd $ucmdir/admin/etc/
./idcadmin_stop

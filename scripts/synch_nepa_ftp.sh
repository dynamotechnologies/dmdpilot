#!/bin/sh
SRCDIR=/var/ftp/nepaftp
DSTDIR=/var/www/html/nepaftp

file="$SRCDIR/test.xml"
if [ -e $file ]; then
  cp -f $file $DSTDIR/
fi

file="$SRCDIR/appeals.xml"
if [ -e $file ]; then
  cp -f $file $DSTDIR/
fi

file="$SRCDIR/objections.xml"
if [ -e $file ]; then
  cp -f $file $DSTDIR/
fi

file="$SRCDIR/units.xml"
if [ -e $file ]; then
  cp -f $file $DSTDIR/
fi

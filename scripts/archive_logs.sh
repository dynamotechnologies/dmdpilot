#!/bin/bash -x

# Run script weekly or biweekly at 1 AM (running from /etc/cron.weekly is OK)
# Should be run as dmdbroker (apache prevented from running cron jobs by
# default)
# Copies all logfiles into /var/www/archived_dmd_logs into a subdirectory
# named logs-YYYYMMDD-YYYYMMDD denoting start/end log times (extracted from
# broker.log

# Script should exit if
# - Not running as dmdbroker
# - Cannot write to archive dir
# - Cannot find log files to extract timestamps
# - Failed to copy files completely

ARCHIVE_DIR=/var/www/archived_dmd_logs
# APP_DIR=/var/www/html/dmdpilot	# PILOT
# APP_DIR=/var/www/html/drmtest		# TEST
SCRIPT_LOG=/tmp/archive_logs.txt
MAILLIST="deokman@gmail.com"

hostname=$(/bin/hostname -s)

(
if [ ! -d "$APP_DIR" ]; then
  echo "Can't find app directory $APP_DIR, is it configured correctly?"
  exit 1
fi

if [ "$(whoami)" != "dmdbroker" ]; then
  echo "Must run as dmdbroker, exiting"
  exit 1
fi

log_dir=$APP_DIR/logs
summary_log=$log_dir/broker.log
first_date=$(head -1 $summary_log | cut -f1 -d\| | cut -f1 -d\  | tr -d / );
last_date=$(tail -1 $summary_log | cut -f1 -d\| | cut -f1 -d\  | tr -d / );
date_range="logs-${first_date}-${last_date}"

target_dir="$ARCHIVE_DIR/$date_range"
if [ -d "$target_dir" ]; then
  # directory already exists!?  pick another name
  for i in $(seq -w 99); do
    new_dir="${target_dir}_$i"
    if [ ! -d "$new_dir" ]; then
      target_dir=$new_dir
      break
    fi
  done
fi

mkdir -p $target_dir 2>&1
if [ "$?" -ne 0 ]; then
  echo "Cannot write to $target_dir, exiting"
  exit 1
fi

cp $log_dir/*.log $target_dir 2>&1
if [ "$?" -ne 0 ]; then
  echo "Error copying log files to $target_dir, exiting"
  exit 1
fi

# If we got this far, OK to clean up log dir
rm -f $log_dir/*.log 2>&1
if [ "$?" -ne 0 ]; then
  echo "Error removing old log files from $log_dir, exiting"
  exit 1
fi

) 2>&1 > $SCRIPT_LOG

if [ "$?" -ne 0 ]; then
  cat $SCRIPT_LOG | /bin/mail -s "$hostname broker log rotation failure" $MAILLIST
else
  cat /dev/null | /bin/mail -s "$hostname broker logs rotated OK" $MAILLIST
fi

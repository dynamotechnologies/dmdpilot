#!/bin/bash


##################################
# Update this setting to point to Xvfb
#
# Pilot:		/usr/X11R6/bin/Xvfb :1 -fbdir /usr/tmp &
# Dev/Test:	/usr/bin/Xvfb :1 -fbdir /usr/tmp &
/usr/X11R6/bin/Xvfb :1 -fbdir /usr/tmp &
#
##################################


XVFB_PID=`jobs -p | tail -1`

export DISPLAY=:1.0

/usr/local/OpenOffice.org1.1.5/soffice &
OO_PID=`jobs -p | tail -1`

/stellent/idcrefinery/connections/main/IdcRefinery &
REFINERY_PID=`jobs -p | tail -1`

echo "#/bin/bash
# PID_REFINERY $REFINERY_PID
# PID_OO $OO_PID
# PID_XVFB $XVFB_PID
kill -HUP $REFINERY_PID
kill $OO_PID
kill $XVFB_PID" > /usr/tmp/stop_refinery

echo "<?php
\$PID_REFINERY=$REFINERY_PID;
\$PID_OO=$OO_PID;
\$PID_XVFB=$XVFB_PID;
?>" > /usr/tmp/drm_processes.php

chmod 766 /usr/tmp/stop_refinery
chmod 766 /usr/tmp/drm_processes.php

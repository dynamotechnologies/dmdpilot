#!/bin/sh
#
#
#

REMOTEHOST=localhost ### set this to the dns name or IP of the remote backup host
BACKUPDIR=/opt/backup
WORKDIR=$BACKUPDIR/working
TARDIR=$BACKUPDIR/tars
JIRA_HOME=/opt/jira/home
NOW=`date +"%Y.%m.%d-%H.%M.%S"`
OUTFILE=$TARDIR/$HOSTNAME.$NOW.tgz

# Create the working directory
mkdir -p $WORKDIR

# Copy the jira home directory to the working directory
cp -R $JIRA_HOME $WORKDIR/jira/



#
### Copy other files to WORKDIR here
#



# tar/zip the working directory
tar -czPf $OUTFILE $WORKDIR

# synch with pocg-tst1
rsync -a -e ssh $TARDIR backup@$REMOTEHOST:~/$HOSTNAME

# Clean up
rm -rf $WORKDIR
rm -f $OUTFILE


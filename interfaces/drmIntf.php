<?php

/*
 * File: drmIntf.php
 *       Define interface for DRM functions
 */

  interface drmIntf {
    public function zipPhaseDocs($projectid, $caraphaseid, $user);
    public function associateDoc($projectid,  $docid);
    public function dissociateDoc($projectid, $docid);
    public function getFile($id, $options, &$fileOut, &$metadataOut);
    public function getFileData($id, $options);
    public function getFileMetadata($id, $options);
    public function putFile($file, $metadata, $options);
    public function deleteFile($id);
    public function putMergedFile($idList, $metadata);
    public function replaceFileMetadata($id, $newMetadata, $options);
    public function publishFile($agent, $id, $options);
    public function transferFile($agent, $file, $options);
    public function searchDocs($querystring, $options);
    public function getNewFileList($jobid);
    public function createNEPAMailingList($jobid, $shortname);
    public function markNEPAAide($id, $moreMetadata);
    public function unmarkNEPAAide($id);
    public function updateNEPAAide($id, $moreMetadata);
  }

?>

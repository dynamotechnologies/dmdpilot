<?php

/*
 * File: dummyIntf.php
 *       Define dummy interface for debugging
 */

  interface dummyIntf {
    public function helloWorld();
    public function echoMsg($parameter);
  }

?>

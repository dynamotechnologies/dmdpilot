<?php

require_once realpath(dirname(__FILE__)) . "/../includes/consts.php";
require_once realpath(dirname(__FILE__)) . "/Http.php";

// May need to uncomment below if being used apart from the broker
// date_default_timezone_set("America/New_York");  // already set in broker.php

/*
 *	PHP Object encapsulating access to Datamart
 *
 *	API methods:		addProjectDocument($projectid,$docid,$metadata)
 *				deleteProjectDocument($projectid,$docid)
 *
 *	private methods:	http_get(url)
 *				http_post(url, payload)
 *				http_put(url, payload)
 *				http_delete(url)
 *
 *	payload is an XML string
 *
 *	Errors may come from 3 sources:
 *	- Datamart (invalid request, inconsistent data)
 *	- Web Server (bad URL, timeouts)
 *	- cURL lib (internal errors)
 *
 *	Internal to this class, GovDelivery and Web Server errors are thrown
 *	as HttpExceptions, while Curl errors are thrown as CurlExceptions.
 *	Classes using the public API methods probably aren't too interested
 *	in these distinctions, so convert everything to standard Exception
 *	in the public methods.
 */

class Datamart {

  /* From consts.php */
  private $URL_PREFIX = DATAMART_PREFIX;
  private $USERPWD = DATAMART_USERPWD;

  private $AUTH_USERPWD;

  function __construct() {
    if (($this->URL_PREFIX === "DATAMART_PREFIX") ||
        ($this->USERPWD === "DATAMART_USERPWD")) {
      throw new Exception(
        "Datamart access not configured, please notify admin",
        BROKER_SYSTEM_EXCEPTION);
    }

    $this->AUTH_USERPWD = utf8_encode($this->USERPWD);
  }

  function http_get($url)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Return a variable instead of posting it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(200), $result);
    curl_close($ch);
    return $result;
  }

  function http_post($url, $payload)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Return a variable instead of posting it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml"));
    curl_setopt($ch, CURLOPT_POSTFIELDS, "$payload");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(201), $result);	// created
    curl_close($ch);
    return $result;
  }

  function http_put($url, $payload)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Return a variable instead of posting it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PUT, true);
    curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml"));
    $putData = tmpfile();
    fwrite($putData, $payload);
    fseek($putData, 0);
    curl_setopt($ch, CURLOPT_INFILE, $putData);
    curl_setopt($ch, CURLOPT_INFILESIZE, strlen($payload));
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(200), $result);
    curl_close($ch);
    return $result;
  }

  function http_delete($url)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(204), $result);	// no content
    curl_close($ch);
    return $result;
  }

  /*
   *  Check the status of the last cURL request, given a list of HTTP
   *  response codes that indicate success.
   *
   *  cURL errors are thrown as Exceptions.
   *  HTTP errors are thrown as HttpExceptions.
   */
  function checkErrors($curl, $goodStatusList, $svrResponse) {
    if (curl_errno($curl) !== 0) {
      // Curl error
      throw new CurlException(
        curl_error($curl),
        curl_errno($curl)
      );
    }
    $info = curl_getinfo($curl);
    $httpcode = $info["http_code"];
    if (! in_array($httpcode, $goodStatusList)) {
      throw new HttpException(
        $this->makeErrorMsg($httpcode, $svrResponse),
        $httpcode);
    }
  }

  /*
   *  Create an error message
   */
  function makeErrorMsg($code, $svrResponse) {
    // return "HTTP error $code";
    return "$svrResponse";
  }

  /*
   *  Generate a new ProjectDocument record as XML
   */
  function newDocument($projectid, $docid, $metadata) {
    $title = $metadata['title'];
    $author = $metadata['author'];

    $moreMetadata = $metadata['moreMetadata'];

    $docauthor = $moreMetadata['docauthor'];
    $addressee = $moreMetadata['addressee'];

    $docdate=$moreMetadata['docdate'];
    $datelabel=$moreMetadata['datelabel'];
    $description = "";
    if (isset($moreMetadata['docsummary'])) {
      $description = $moreMetadata['docsummary'];
    }

    $sensitiveflag = 0;
    if (isset($moreMetadata['sensitiveflag'])) {
      $flag = strtolower(trim($moreMetadata['sensitiveflag']));
      switch ($flag) {
        case "true":
          $sensitiveflag = 1;
          $sensitiverat = $moreMetadata['sensitiverat'];
          break;
        case "false":
          $sensitiveflag = 0;
          break;
        case "1":
          $sensitiveflag = 1;
          $sensitiverat = $moreMetadata['sensitiverat'];
          break;
        case "0":
          $sensitiveflag = 0;
          break;
      }
    }

    $pubflag = 0;
    if (isset($moreMetadata['pubflag'])) {
      // Requester should make sure input is restricted to 1/0
      $pubflag = $moreMetadata['pubflag'] ? 1 : 0;
    }

    $wwwlink = "";
    if (isset($moreMetadata['wwwlink'])) {
      $wwwlink = $moreMetadata['wwwlink'];
    }
    $phaseid = "";
    if (isset($moreMetadata['caraphaseid'])){
        $phaseid = $moreMetadata['caraphaseid'];
    }

    // If there is a filesize member in the metadata, assume this is a PDF,
    // go ahead and set pdffilesize.

    $pdffilesize = "";
    if (isset($metadata['filesize'])) {
      $filesize = round($metadata['filesize'] / 1024);
      if ($filesize == 0) { $filesize = 1; }
      $pdffilesize = "${filesize}kb";
    }

    $response =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<projectdocument xmlns=\"http://www.fs.fed.us/nepa/schema\">
    <docid>" . htmlspecialchars($docid, ENT_QUOTES, 'UTF-8') . "</docid>
    <projecttype>nepa</projecttype>
    <projectid>" . htmlspecialchars($projectid, ENT_QUOTES, 'UTF-8') . "</projectid>
    <pubflag>" . htmlspecialchars($pubflag, ENT_QUOTES, 'UTF-8') . "</pubflag>
    <wwwlink>" . htmlspecialchars($wwwlink, ENT_QUOTES, 'UTF-8') . "</wwwlink>
    <docname>" . htmlspecialchars($title, ENT_QUOTES, 'UTF-8') . "</docname>
    <datelabel>" . htmlspecialchars($datelabel, ENT_QUOTES, 'UTF-8') . "</datelabel>
    <docdate>" . htmlspecialchars($docdate, ENT_QUOTES, 'UTF-8') . "</docdate>
    <addedby>" . htmlspecialchars($author, ENT_QUOTES, 'UTF-8') . "</addedby>
    <description>" . htmlspecialchars($description, ENT_QUOTES, 'UTF-8') . "</description>
    <pdffilesize>" . htmlspecialchars($pdffilesize, ENT_QUOTES, 'UTF-8') . "</pdffilesize>
    <sensitiveflag>" . htmlspecialchars($sensitiveflag, ENT_QUOTES, 'UTF-8') . "</sensitiveflag>
     <sensitivityrationale>" . htmlspecialchars($sensitiverat, ENT_QUOTES, 'UTF-8') . "</sensitivityrationale>
    <author>" . htmlspecialchars($docauthor, ENT_QUOTES, 'UTF-8') . "</author>
    <addressee>" . htmlspecialchars($addressee, ENT_QUOTES, 'UTF-8') . "</addressee>
	<phaseid>" . htmlspecialchars($phaseid, ENT_QUOTES, 'UTF-8') . "</phaseid>
</projectdocument>
";
    return $response;
  }

/***********************************
 *   Public functions start here
 ***********************************/

  /*
   *  Create a new ProjectDocument
   *
   *  This generates a value for wwwlink
   */
  function addProjectDocument($projectid, $docid, $metadata) {

    // FIXME: This will be deprecated someday (soon?)
    // Compute new wwwlink value
    $wwwlink = $this->makeLinkURL($projectid, $docid);
    $metadata['moreMetadata']['wwwlink'] = $wwwlink;

    $req = $this->newDocument($projectid, $docid, $metadata);
    $url = $this->URL_PREFIX . "/projects/nepa/$projectid/docs";
    try {
      $response = $this->http_post($url, $req);
      return $response;
    } catch (HTTPException $e) {
      if ($e->getCode() == 409) {	// Record already exists?
	//
	// Possibilities:	Missing project record?
	//			(we already know project exists because
	//			we had to get categories)
	//			Record already exists?
	//			(docid is being created from DMD and
	//			should already be unique
        throw new Exception("Error adding $docid to the Datamart, "
	  . "to project nepa/$projectid " . $e->getMessage(),
          DATAMART_REFERENCE_VIOLATION);
      }
      throw new Exception("Error adding $docid to the Datamart, "
	                  . "to project nepa/$projectid "
                          . "HTTP error code " . $e->getCode() . " "
                          . $e->getMessage(), DATAMART_SERVER_EXCEPTION);
    } catch (CurlException $e) {
      throw new Exception("Error adding $docid to the Datamart, "
	                  . "to project nepa/$projectid "
                          . "CURL error code " . $e->getCode() . " "
                          . $e->getMessage(), DATAMART_SERVER_EXCEPTION);
    }

  }

  function addToContainerDocs($projectid, $docid, $metadata){

  	$fp = fopen("/var/tmp/contdoclog.txt", "w");
	fwrite($fp, "starting");

  	$contid = "";
    $moreMetadata = $metadata['moreMetadata'];
    if (isset($moreMetadata['level3id'])) {
         $contid = $moreMetadata['level3id'];
    }else if (isset($moreMetadata['level2id'])) {
    	$contid = $moreMetadata['level2id'];
    }else if (isset($metadata['appdoctype'])){
	$apptype = $metadata['appdoctype'];
	if (strcmp( $apptype, 'Pre-Scoping') == 0){
		$contid = "-1";
	}else if (strcmp( $apptype, 'Scoping') == 0){
		$contid = "-2";
	}else if (strcmp($apptype, 'Analysis') == 0){
		$contid = "-3";
	}else if (strcmp($apptype, 'Supporting') == 0) {
		$contid = "-4";
	}else if (strcmp($apptype, 'Decision') == 0) {
		$contid = "-5";
	}else if (strcmp($apptype, 'Post-Decision') == 0){
		$contid = "-6";
	}else if (strcmp($apptype, 'Assessment') == 0){
		$contid = "-7";
	}else if (strcmp($apptype, 'Forest Plan') == 0){
		$contid = "-8";
	}else if (strcmp($apptype, 'Litigation') == 0){
		$contid = "89";
	}
    }

    if ($contid == ''){
	fwrite($fp, "no containter id \n" );
    }

fwrite($fp, "\n contid = $contid \n");
  	$req = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
  			. "<containerdocs xmlns=\"http://www.fs.fed.us/nepa/schema\">"
  			. "<containerdoc parentcontid=\"" . $contid
  			. "\" docid=\"" . $docid
  			. "\" order=\"1\" /></containerdocs>";
fwrite($fp, "\n req = $req");
  	$url = $this->URL_PREFIX . "/projects/nepa/$projectid/containers/updatecontainerdocs";
fwrite($fp, "\n url = $url\n");
//For some reason, HTTP code 200 is interpreted as an exception?
  	try {
  		$response = $this->http_post($url, $req);
  		return $response;
  	} catch (Exception $e) {
//  		throw new Exception("Error creating containerdoc - "
//  				 . $e->getCode() . " "
//  				 . $e->getMessage(), DATAMART_SERVER_EXCEPTION);
  	}
  }

public function endswith($string, $test){

	$strlen = strlen($string);
	$testlen = strlen($test);
	if ($testlen > $strlen) return false;
	return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;

}



  /*
   *  Update a ProjectDocument
   *
   *  The pubflag and wwwlink values are being removed from DMD metadata
   *  so these values will not be updated from the Broker
   *
   *  The filesize value from the metadata parameter will replace
   *  the value of pdffilesize in the ProjectDocument
   */
  function updateProjectDocument($projectid, $docid, $metadata) {

    // Get old projectdocument record
    // Don't allow updates of pubflag or wwwlink from the Broker
    $oldprojdoc = $this->getProjectDocument($projectid, $docid);
    $newmetadata = array(
      'title' => $metadata['title'],
      'filesize' => $metadata['filesize'],
      'moreMetadata' => array(
        'pubflag' => $oldprojdoc['pubflag'],
        'wwwlink' => $oldprojdoc['wwwlink'],
        'docsummary' => $oldprojdoc['description'],
        'sensitiveflag' => $oldprojdoc['sensitiveflag']
      )
    );
//If this is a zip file, change this link to match the filename.
if (isset($metadata['filename']) ){
	if ($this->endswith($metadata['filename'], "zip") ){
	$filename = $metadata['filename'];
	$wwwlink = $newmetadata['moreMetadata']['wwwlink'];
	$arr = explode('/', $wwwlink);
	array_pop($arr);
	$url = implode('/', $arr);
	$newwwlink = $url.'/'.$filename;
	$newmetadata['moreMetadata']['wwwlink'] = $newwwlink;
	}
}



    // Update new values
    $mm = $metadata['moreMetadata'];
    if (isset($mm['docsummary'])) {
      $newmetadata['moreMetadata']['docsummary'] = $mm['docsummary'];
    }
    if (isset($mm['sensitiveflag'])) {
      $newmetadata['moreMetadata']['sensitiveflag'] = $mm['sensitiveflag'];
    }

    if (isset($mm['docauthor'])) {
      $newmetadata['moreMetadata']['docauthor'] = $mm['docauthor'];
    }
    if (isset($mm['addressee'])) {
      $newmetadata['moreMetadata']['addressee'] = $mm['addressee'];
    }
    if (isset($mm['docdate'])) {
      $newmetadata['moreMetadata']['docdate'] = $mm['docdate'];
    }
    if (isset($mm['datelabel'])) {
      $newmetadata['moreMetadata']['datelabel'] = $mm['datelabel'];
    }
    // Store new record
    $req = $this->newDocument($projectid, $docid, $newmetadata);
    $url = $this->URL_PREFIX . "/projects/nepa/$projectid/docs/$docid";
    try {
      $response = $this->http_put($url, $req);
      return $response;
    } catch (HTTPException $e) {
      throw new Exception("Error updating $docid in the Datamart, "
	                  . "for project nepa/$projectid "
                          . "HTTP error code " . $e->getCode() . " "
                          . $e->getMessage(), DATAMART_SERVER_EXCEPTION);
    } catch (CurlException $e) {
      throw new Exception("Error adding $docid to the Datamart, "
	                  . "to project nepa/$projectid "
                          . "CURL error code " . $e->getCode() . " "
                          . $e->getMessage(), DATAMART_SERVER_EXCEPTION);
    }
  }

  /*
   * Updates metadata for all associated ProjectDocument records.
   */
  function updateProjectDocuments($docid, $metadata){
  	$url = $this->URL_PREFIX . "/projectdocs/nepa/$docid";
  	try {
  		$xml = $this->http_get($url);
  		$dom = new SimpleXMLElement($xml);

  		foreach ($dom->projectdocument as $projdoc){

  			$this->updateProjectDocument($projdoc->projectid, $docid, $metadata);
  		}

  	} catch (Exception $e) {
  		throw new Exception("Error getting URL " . $url,
  				DATAMART_SERVER_EXCEPTION);
  	}
  }

  /*
   *	Remove a document
   */
  function deleteProjectDocument($projectid, $docid) {
    $url = $this->URL_PREFIX . "/projects/nepa/$projectid/docs/$docid";
    try {
      $response = $this->http_delete($url);
    } catch (HTTPException $e) {
      if ($e->getCode() == 404) {	// Not found
        throw new Exception("Delete failed for nepa/$projectid, doc $docid, "
                            . "record not found", DATAMART_RECORD_NOT_FOUND);
      }
      if ($e->getCode() == 409) {	// Ref constraint conflict?
	// A delete request failed because the document has dependent
	// rows--cannot delete record, throw an error
        throw new Exception($e->getMessage(),
          DATAMART_REFERENCE_VIOLATION);
      }
      throw new Exception("HTTP error code " . $e->getCode() . " " .
                          $e->getMessage(), DATAMART_SERVER_EXCEPTION);
    } catch (CurlException $e) {
      throw new Exception("CURL error code " . $e->getCode() . " " .
                          $e->getMessage(), DATAMART_SERVER_EXCEPTION);
    }
    return $response;
  }

  /*
   *  Return the elements of a projectdocument record as an array
  */
  function deleteProjectDocuments($docid) {

  	$url = $this->URL_PREFIX . "/projectdocs/nepa/$docid";
  	try {
  		$xml = $this->http_get($url);
  		$dom = new SimpleXMLElement($xml);

  		foreach ($dom->projectdocument as $projdoc){

  			$this->deleteProjectDocument($projdoc->projectid, $docid);
  		}
   	} catch (Exception $e) {
  		throw new Exception("Error getting URL " . $url,
  				DATAMART_SERVER_EXCEPTION);
  	}
      fclose($fp);

  }

  /*
   *  Return the elements of a projectdocument record as an array
   */
  function getProjectDocument($projectid, $docid) {
    $url = $this->URL_PREFIX . "/projects/nepa/$projectid/docs/$docid";
    try {
      $xml = $this->http_get($url);
      $dom = new SimpleXMLElement($xml);
      $projectdocument = array(
        "docid" => strval($dom->docid),
        "projecttype" => strval($dom->projecttype),
        "projectid" => strval($dom->projectid),
        "pubflag" => strval($dom->pubflag),
        "wwwlink" => strval($dom->wwwlink),
        "docname" => strval($dom->docname),
        "description" => strval($dom->description),
        "pdffilesize" => strval($dom->pdffilesize),
        "sensitiveflag" => strval($dom->sensitiveflag)
      );
      return $projectdocument;
    } catch (Exception $e) {
      throw new Exception("Error getting URL " . $url,
                          DATAMART_SERVER_EXCEPTION);
    }
  }

  /*
   * Get the projectdocumentid value for the project
   *
   * Note that some test data projects may be missing this value, but all
   * true NEPA projects from PALS should have a valid value here
   */
  function getProjDocId($projectid) {
    $url = $this->URL_PREFIX . "/projects/nepa/$projectid";
    try {
      $xml = $this->http_get($url);
      $dom = new SimpleXMLElement($xml);
      return strval($dom->nepainfo->projectdocumentid);
    } catch (Exception $e) {
      throw new Exception("Error getting URL $url; " . $e->getMessage(),
                          DATAMART_SERVER_EXCEPTION);
    }
  }

  /*
   * Get the project record as a SimpleXML DOM
   */
  function getProjectDOM($projectid) {
    $url = $this->URL_PREFIX . "/projects/nepa/$projectid";
    try {
      $xml = $this->http_get($url);
      $dom = new SimpleXMLElement($xml);
      return $dom;
    } catch (Exception $e) {
      throw new Exception("Error getting URL $url; " . $e->getMessage(),
                          DATAMART_SERVER_EXCEPTION);
    }
  }

  /*
   * Get the project record as a SimpleXML DOM
  */
  function getUnitDOM($unitid) {
  	$url = $this->URL_PREFIX . "/ref/units/$unitid";
  	try {
  		$xml = $this->http_get($url);
  		$dom = new SimpleXMLElement($xml);
  		return $dom;
  	} catch (Exception $e) {
  		throw new Exception("Error getting URL $url; " . $e->getMessage(),
  				DATAMART_SERVER_EXCEPTION);
  	}
  }

  /*
   * Get the project record as a SimpleXML DOM
  */
  function getPurposeDOM($purposeid) {
  	$url = $this->URL_PREFIX . "/ref/purposes/$purposeid";
  	try {
  		$xml = $this->http_get($url);
  		$dom = new SimpleXMLElement($xml);
  		return $dom;
  	} catch (Exception $e) {
  		throw new Exception("Error getting URL $url; " . $e->getMessage(),
  				DATAMART_SERVER_EXCEPTION);
  	}
  }

  /*
	* Get the MLMProject record (if exists)
   */
  function getMLMDOM($mlmid){
  	$url = $this->URL_PREFIX . "/mlm/projects/$mlmid";
  	try {
  		$xml = $this->http_get($url);
  		$dom = new SimpleXMLElement($xml);
  		return $dom;
  	} catch (Exception $e) {
  		throw new Exception("Error getting URL $url " . $e->getMessage(),
  				DATAMART_SERVER_EXCEPTION);
  	}
  }

  /*
   * Generate a value for wwwlink
   *
   * Eventually we will get rid of the wwwlink field and the website that
   * displays this value will generate it by itself
   */
  function makeLinkURL($projectid, $docid) {
    $projdocid = $this->getProjDocId($projectid);
    $wwwlink = "http://" . DMD_CACHE . "/${projdocid}_${docid}.pdf";
    return $wwwlink;
  }
  /*
   * Create MLMProject entity in DataMart
   */
  function addMLMProject($projectid, $mlmid){
  	$url = $this->URL_PREFIX . "/mlm/projects";
  	$req = $this->newMLMProject($projectid, $mlmid);

  	$response = $this->http_post($url, $req);
  	return $response;
  }

  /*
   * Generates XML for MLMProject entity
   */
  function newMLMProject($projectid, $mlmid){
  	$response =
  	"<?xml version=\"1.0\"?>
  	<mlmproject xmlns=\"http://www.fs.fed.us/nepa/schema\">
  	<mlmid>" . htmlentities($mlmid) . "</mlmid>
  	<projecttype>nepa</projecttype>
  	<projectid>" . htmlentities($projectid) . "</projectid>
  	</mlmproject>";
  	return $response;
	}
}
?>

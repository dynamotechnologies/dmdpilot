<?php

  /*
   *	A few notes on installation:
   *
   *  The logfile path (specified in the Constructor below) needs to exist and be
   *    writeable by apache. You may need to create this file and chown it.
   *
   */


  require_once realpath(dirname(__FILE__)) . "/../interfaces/drmIntf.php";
  require_once realpath(dirname(__FILE__)) . "/../includes/consts.php";
  require_once realpath(dirname(__FILE__)) . "/GovDelivery.php";
  require_once realpath(dirname(__FILE__)) . "/Datamart.php";


  /*
   * Define DRM proxy class
   * This object implements functions from the DRM interface
   */
  class DrmProxy implements drmIntf {

    private $USERNAME;
    private $PASSWORD;
    private $SERVER;
    private $PORT;
    private $DEBUG;
    private $LOGFILE;

	private $DOCTYPE;
	private $SG;
	private $ACCT;

    private $WSDL_GET_FILE;
    private $WSDL_GET_FILE_DATA;
    private $WSDL_GET_FILE_METADATA;
    private $WSDL_PUT_FILE;
    private $WSDL_UPDATE_FILE;
    private $WSDL_DELETE_FILE;
    private $WSDL_GENERATE_FILE_ID;
    private $WSDL_SEARCH_FILE;

    private $params;
    private $strLog;
    private $client;
    private $proxy;


	/*
	 *  Constructor
	 */
	function DrmProxy($protocol=DRM_PROTOCOL,
						$uid="soapuser",
						$pwd="soapmeup",
						$serv=DRM_SERVER,
						$prt=DRM_PORT,
						$wsdlpath=DRM_WSDL_PATH,
						$bug=DRM_DEBUG_MODE)
	{


	  $this->USERNAME	= $uid;
	  $this->PASSWORD	= $pwd;
	  $this->SERVER		= $serv;
	  $this->PORT		= $prt;
	  $this->DEBUG		= $bug;

	  $this->LOGFILE 	= realpath(dirname(__FILE__) . "/../logs/servlog.htm");

	  $this->DOCTYPE	= DMD_DOC_TYPE;
	  $this->SG			= DMD_SECURITY_GROUP;
	  $this->ACCT		= DMD_ACCOUNT;

	  $prefix = ( $protocol . "://" . $uid . ":" . $pwd . "@" . $serv . ":" . $prt . $wsdlpath );

	  $this->WSDL_GET_FILE 			= $prefix . "GetFile.wsdl";
	  $this->WSDL_GET_FILE_DATA 	= $prefix . "GetFile.wsdl";
	  $this->WSDL_GET_FILE_METADATA	= $prefix . "DocInfo.wsdl";
	  $this->WSDL_PUT_FILE 			= $prefix . "CheckIn.wsdl";
	  $this->WSDL_UPDATE_FILE		= $prefix . "CheckIn.wsdl";
	  $this->WSDL_GENERATE_FILE_ID	= $prefix . "CheckIn.wsdl";
	  $this->WSDL_DELETE_FILE		= $prefix . "DeleteDoc.wsdl";
	  $this->WSDL_SEARCH_FILE		= $prefix . "Search.wsdl";

	  $this->params		= array();
	  $this->strLog		= "<pre>" . date(DATE_RFC822) . "</pre>";
	}

	function zipPhaseDocs($caraid, $phaseid, $shortname) {

     $this->appendLog("Calling drmProxy.php::zipPhaseDocs");

		$connString = UCM_CONN_STRING;
		$username = UCM_USERNAME;
		$password = UCM_PASSWORD;

		$DM_connString = MYSQLDB_CONN_STRING;
		$DM_username = MYSQLDB_USERNAME;
		$DM_password = MYSQLDB_PASSWORD;

		$downloaderUrl = DOWNLOADER_JOB_SCRIPT;

		$email = $shortname."@fs.fed.us";
		$columns = "d,e,g,h";
		$archiveRoot = "documents";

		$cmd = "java -jar /var/www/common/caradownloadjobbuilder/CARADownloadJobBuilder.jar ";
		$cmd = $cmd . ' ' . $connString;
		$cmd = $cmd . ' ' . $username;
		$cmd = $cmd . ' ' . $password;
		$cmd = $cmd . ' ' . $DM_connString;
		$cmd = $cmd . ' ' . $DM_username;
		$cmd = $cmd . ' ' . $DM_password;
		$cmd = $cmd . ' ' . $caraid;
		$cmd = $cmd . ' ' . $phaseid;
		$cmd = $cmd . ' ' . $email;
		$cmd = $cmd . ' ' . $shortname;
		$cmd = $cmd . ' ' . $downloaderUrl;
		$cmd = $cmd . ' ' . $columns;
		$cmd = $cmd . ' ' . $archiveRoot;
		$cmd = $cmd . ' ' . '> /dev/null 2>&1 &';	//Ignore output and run in background

		exec($cmd);

		//For initial testing & debugging - log the command issued
		$fp = fopen("/var/tmp/cara-download-debug.txt","w");
		fwrite($fp, $cmd);
		fclose($fp);
        $this->appendLog("Calling drmProxy.php::zipPhaseDocs - END");

		return true;
	}


	private function http_post($url, $payload)
	{

		$result = "";
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		// Return a variable instead of posting it directly
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "$payload");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_exec($ch);

		curl_close($ch);
	}

	/**
	 * AND Search of the moreMetadata field for properties and values
	 * stored in the $propertyArray parameter
	 * e.g. array( property1 = value1, property2 = value2)
	 *
	 *
	 * Returns array of document ids and titles in the format
	 * 	 array[document_id] = Document Title
	 *
	 * @param $propertyArray
	 * @return $result_array
	 */
	private function searchMetaData($property_array){
		$pageNumber = 1;
		$startRow = 1;
		$endRow = 20;
		$totalProcessed = 0;
		$finishedProcessing = false;
		$queryStr = $this->constructMoreMetadataQuery($property_array);

		$result_array = array();

		do {

			$this->params = array(
					'queryText'=>$queryStr,
					'pageNumber'=>$pageNumber,
					'startRow'=>$startRow,
					'endRow'=>$endRow
			);
			// Create the SOAP connection objects
			$this->CreateConnection($this->WSDL_SEARCH_FILE);
			$response	= $this->getSoapResponse("NavigationSearch", "NavigationSearchResult");

			$numRows = count($response->NavigationSearchResult->SearchResults);
			$totalRows = $response->NavigationSearchResult->SearchInfo->totalRows;

			if ($numRows == 1){

				$doc_id = $response->NavigationSearchResult->SearchResults->dDocName;
				$title = $response->NavigationSearchResult->SearchResults->dDocTitle;
				$result_array[$doc_id] = $title;

			}else{
				foreach ($response->NavigationSearchResult->SearchResults as $aResult){

					$doc_id = $aResult->dDocName;
					$title = $aResult->dDocTitle;
					$result_array[$doc_id] = $title;
				}

			}
			$totalProcessed = $totalProcessed + $numRows;

			//If more results remain, move to next page
			if ($totalRows > $totalProcessed){

				$pageNumber = $pageNumber + 1;
				$startRow = $startRow +20;
				$endRow = $endRow + 20;
			}else{
				$finishedProcessing = true;
			}
		}while ($finishedProcessing == false);

		return $result_array;
	}

	/**
	 * Constructs a query string in the UCM/Oracle Query language separating each
	 * property clause with the AND preposition.
	 *
	 * @param $property_array
	 * @return string
	 */
 	private function constructMoreMetadataQuery($property_array){
		$propSep = "\":\"";
		$queryStr = "";
		$first = true;

		$keys = array_keys($property_array);

		foreach($keys as $key){

			$value = $property_array[$key];

			if ($first){
				$first = false;
			}else{
				$queryStr = $queryStr." <and> ";
			}
			$queryStr = $queryStr."xMoreMetadata <substring> `".$key;
			$queryStr = $queryStr.$propSep.$value."`";

		}

		return $queryStr;

	}





	/* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\  */

	public function associateDoc($projectId, $fileID) {

	  $options = array('fileType' => "");
	  $metadataOut = array();

	  // Set  parameters
	  $this->params = array('dDocName' => $fileID, 'dRevLabel' => 1);   // also accepts 'rendition', 'extraProps';

	  $this->CreateConnection($this->WSDL_GET_FILE_METADATA);
	  $response = $this->getSoapResponse("DocInfoByName", "DocInfoByNameResult");

	  //
	  // Extract Custom Metadata field locations from the response
	  //
	  $mmd = &$response->DocInfoByNameResult->ContentInfo->CustomDocMetaData->property;
	  $xOriginalAuthor	= $this->getCustomMetadataLocation($mmd,"xOriginalAuthor");
	  $xPubDate			= $this->getCustomMetadataLocation($mmd,"xPubDate");
	  $xAppSystemID		= $this->getCustomMetadataLocation($mmd,"xAppSystemID");
	  $xAppDocumentType	= $this->getCustomMetadataLocation($mmd,"xAppDocumentType");
	  $xMoreMetadata		= $this->getCustomMetadataLocation($mmd,"xMoreMetadata");

	  if ($xOriginalAuthor<0 OR $xPubDate<0 OR $xMoreMetadata<0 OR $xAppSystemID<0 OR $xAppDocumentType<0 ) {
	  	throw new Exception("Couldn't find location of custom metadata.", BAD_XML_EXCEPTION);
	  }

	  if (isset($sizeofPDF)) {
	    $metadataOut['filename'] = $fileID . "." . $response->DocInfoByNameResult->ContentInfo->dWebExtension;
	    $metadataOut['filesize'] = $sizeofPDF;
	  } else {
	  	$metadataOut['filename'] = $response->DocInfoByNameResult->ContentInfo->dOriginalName;
	    $metadataOut['filesize'] = $response->DocInfoByNameResult->ContentInfo->dFileSize;
	  }
	  $metadataOut['title'] 				= $response->DocInfoByNameResult->ContentInfo->dDocTitle;
	  $metadataOut['author'] 				= $mmd[$xOriginalAuthor]->value;
	  $metadataOut['pubdate'] 			= $mmd[$xPubDate]->value;
	  $metadataOut['appsysid'] 			= $mmd[$xAppSystemID]->value;
	  $metadataOut['appdoctype']	= $mmd[$xAppDocumentType]->value;
	  $metadataOut['moreMetadata'] 	= $this->getMoreMetadata($mmd[$xMoreMetadata]->value);
	  if (!isset($metadataOut['moreMetadata']["datelabel"])) {
		$metadataOut['moreMetadata']["datelabel"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["docauthor"])) {
		$metadataOut['moreMetadata']["docauthor"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["docsummary"])) {
		$metadataOut['moreMetadata']["docsummary"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["addressee"])) {
		$metadataOut['moreMetadata']["addressee"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["sensitiveflag"])) {
		$metadataOut['moreMetadata']["sensitiveflag"]="0";
	  }

	  $this->DestroyConnection();

	  try {
			// Add projectdocument record to Datamart
			$Datamart = new Datamart();
			$Datamart->addProjectDocument($projectId, $fileID, $metadataOut);

		} catch (Exception $e) {
		$msg = "Error in associate ProjectDocument " . $e->getMessage();
		$code = $e->getCode();
		throw new Exception($msg, $code);
		}
		fclose($fp1);
		return true;

	}

	function dissociateDoc($projectID,  $fileID) {

		$Datamart = new Datamart();
		try {
              $resp = $Datamart->deleteProjectDocument($projectID, $fileID);
		} catch (Exception $e) {
              // No error if project-document record is not in Datamart
			if ($e->getCode() != DATAMART_RECORD_NOT_FOUND) {
                throw $e;
			}
		}
		return true;
	}


/* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\  */


// TODO: optimize passing by value/reference

    /*
     * Retrieve file and base metadata
     * Parameters:
     *   IN:  fileID - string
     *   OUT: fileDataOut - string ref (data from retrieved file)
     *        metadataOut - array ref (metadata array)
     * Returns: nothing
     */
    public function getFile($fileID, $options, &$fileDataOut, &$metadataOut) {
	  // Set  parameters
	  if (isset($options['fileType'])) {
	    if ($options['fileType'] == "PDF") {
	  	  $this->params = array('dDocName' => $fileID, 'revisionSelectionMethod' => 'Latest', 'rendition' => 'Web');   // also accepts 'extraProps';
	  	  $sizeofPDF = strlen($this->getFileData($fileID, $options));
	    } else {
	      throw new Exception("Parameter fileType contains an invalid value.", FILETYPE_NOT_FOUND_EXCEPTION);
	    }
	  } else {
	  	$this->params = array('dDocName' => $fileID, 'revisionSelectionMethod' => 'Latest');   // also accepts 'extraProps';
	  }

	  $this->CreateConnection($this->WSDL_GET_FILE);

	  $response = $this->getSoapResponse("GetFileByName", "GetFileByNameResult");

	  //
	  // Extract Custom Metadata field locations from the response
	  //
	  $mmd = &$response->GetFileByNameResult->FileInfo->CustomDocMetaData->property;
	  $xOriginalAuthor	= $this->getCustomMetadataLocation($mmd,"xOriginalAuthor");
	  $xPubDate			= $this->getCustomMetadataLocation($mmd,"xPubDate");
	  $xAppSystemID		= $this->getCustomMetadataLocation($mmd,"xAppSystemID");
	  $xAppDocumentType	= $this->getCustomMetadataLocation($mmd,"xAppDocumentType");
	  $xMoreMetadata		= $this->getCustomMetadataLocation($mmd,"xMoreMetadata");

	  if ($xOriginalAuthor<0 OR $xPubDate<0 OR $xMoreMetadata<0 OR $xAppSystemID<0 OR $xAppDocumentType<0 ) {
		throw new Exception("Couldn't find location of custom metadata.", BAD_XML_EXCEPTION);
	  }

	  $fileDataOut						= $response->GetFileByNameResult->downloadFile->fileContent;
	  if (isset($sizeofPDF)) {
	  	$metadataOut['filename'] = $fileID . "." . $response->GetFileByNameResult->FileInfo->dWebExtension;
	    $metadataOut['filesize'] = $sizeofPDF;
	  } else {
	    $metadataOut['filename'] = $response->GetFileByNameResult->downloadFile->fileName;
	    $metadataOut['filesize'] = $response->GetFileByNameResult->FileInfo->dFileSize;
	  }
	  $metadataOut['title'] 			= $response->GetFileByNameResult->FileInfo->dDocTitle;
	  $metadataOut['author'] 			= $mmd[$xOriginalAuthor]->value;
	  $metadataOut['pubdate'] 			= $mmd[$xPubDate]->value;
	  $metadataOut['appsysid']			= $mmd[$xAppSystemID]->value;
	  $metadataOut['appdoctype']		= $mmd[$xAppDocumentType]->value;
	  $metadataOut['moreMetadata'] 		= $this->getMoreMetadata($mmd[$xMoreMetadata]->value);

	  if (!isset($metadataOut['moreMetadata']["datelabel"])) {
		$metadataOut['moreMetadata']["datelabel"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["docauthor"])) {
		$metadataOut['moreMetadata']["docauthor"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["docsummary"])) {
		$metadataOut['moreMetadata']["docsummary"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["addressee"])) {
		$metadataOut['moreMetadata']["addressee"]=" ";
	  }
	  if (!isset($metadataOut['moreMetadata']["sensitiveflag"])) {
		$metadataOut['moreMetadata']["sensitiveflag"]="0";
	  }

	  $this->DestroyConnection();
	  if ($this->DEBUG) {
	    $this->debugLog();
	  }
    }



    /*
     * Retrieve file data
     * Parameters: fileID - string
     * Returns: fileData - string (may be quite large)
     */
    public function getFileData($fileID, $options) {
	  // Set  parameters
	  if (isset($options['fileType'])) {
	    if ($options['fileType'] == "PDF") {
	  	  $this->params = array('dDocName' => $fileID, 'revisionSelectionMethod' => 'Latest', 'rendition' => 'Web');   // also accepts 'extraProps';
	  	} else {
		  throw new Exception("Parameter fileType contains an invalid value.", FILETYPE_NOT_FOUND_EXCEPTION);
	    }
	  } else {
	  	$this->params = array('dDocName' => $fileID, 'revisionSelectionMethod' => 'Latest');   // also accepts 'extraProps';
	  }

    try{
	  $this->CreateConnection($this->WSDL_GET_FILE_DATA);

	  $response = $this->getSoapResponse("GetFileByName", "GetFileByNameResult");
	  $fileData = $response->GetFileByNameResult->downloadFile->fileContent;

	  $this->DestroyConnection();
    } catch(Exception $e){
      $logmsg = $e->getMessage();
      $this->appendLog($logmsg);
      $fileData="0";
    }
	  if ($this->DEBUG) {
	    $this->debugLog();
	  }

      return $fileData;

    }



    /*
     * Retrieve file metadata
     * Parameters: fileID - string
     * Returns: metadata - array (may contain nested array)
     */
    public function getFileMetadata($fileID, $options) {
      if (isset($options['fileType'])) {
        if ($options['fileType'] == "PDF") {
          $sizeofPDF = strlen($this->getFileData($fileID, $options));
        } else {
          throw new Exception("Parameter fileType contains an invalid value.", FILETYPE_NOT_FOUND_EXCEPTION);
        }
      }

	  $metadataOut = array();

	  // Set  parameters
	  $this->params = array('dDocName' => $fileID, 'dRevLabel' => 1);   // also accepts 'rendition', 'extraProps';

	  $this->CreateConnection($this->WSDL_GET_FILE_METADATA);
	  $response = $this->getSoapResponse("DocInfoByName", "DocInfoByNameResult");

	  //
	  // Extract Custom Metadata field locations from the response
	  //
	  $mmd = &$response->DocInfoByNameResult->ContentInfo->CustomDocMetaData->property;
	  $xOriginalAuthor	= $this->getCustomMetadataLocation($mmd,"xOriginalAuthor");
	  $xPubDate			= $this->getCustomMetadataLocation($mmd,"xPubDate");
	  $xAppSystemID		= $this->getCustomMetadataLocation($mmd,"xAppSystemID");
	  $xAppDocumentType	= $this->getCustomMetadataLocation($mmd,"xAppDocumentType");
	  $xMoreMetadata		= $this->getCustomMetadataLocation($mmd,"xMoreMetadata");

	  if ($xOriginalAuthor<0 OR $xPubDate<0 OR $xMoreMetadata<0 OR $xAppSystemID<0 OR $xAppDocumentType<0 ) {
	  	throw new Exception("Couldn't find location of custom metadata.", BAD_XML_EXCEPTION);
	  }

	  if (isset($sizeofPDF)) {
	    $metadataOut['filename'] = $fileID . "." . $response->DocInfoByNameResult->ContentInfo->dWebExtension;
	    $metadataOut['filesize'] = $sizeofPDF;
	  } else {
	  	$metadataOut['filename'] = $response->DocInfoByNameResult->ContentInfo->dOriginalName;
	    $metadataOut['filesize'] = $response->DocInfoByNameResult->ContentInfo->dFileSize;
	  }
	  $metadataOut['title'] 				= $response->DocInfoByNameResult->ContentInfo->dDocTitle;
	  $metadataOut['author'] 				= $mmd[$xOriginalAuthor]->value;
	  $metadataOut['pubdate'] 			= $mmd[$xPubDate]->value;
	  $metadataOut['appsysid'] 			= $mmd[$xAppSystemID]->value;
	  $metadataOut['appdoctype']	= $mmd[$xAppDocumentType]->value;
	  $metadataOut['moreMetadata'] 	= $this->getMoreMetadata($mmd[$xMoreMetadata]->value);

		if (!isset($metadataOut['moreMetadata']["datelabel"])) {
		  $metadataOut['moreMetadata']["datelabel"]=" ";
		}
		if (!isset($metadataOut['moreMetadata']["docauthor"])) {
		  $metadataOut['moreMetadata']["docauthor"]=" ";
		}
		if (!isset($metadataOut['moreMetadata']["docsummary"])) {
		  $metadataOut['moreMetadata']["docsummary"]=" ";
		}
		if (!isset($metadataOut['moreMetadata']["addressee"])) {
		  $metadataOut['moreMetadata']["addressee"]=" ";
		}
		if (!isset($metadataOut['moreMetadata']["sensitiveflag"]) || $metadataOut['moreMetadata']["sensitiveflag"]==false) {
		  $metadataOut['moreMetadata']["sensitiveflag"]="0";
		}

	  $this->DestroyConnection();
	  if ($this->DEBUG) {
	    $this->debugLog();
	  }

      return $metadataOut;
    }



    /*
     * Store file data
     *
     * If moreMetadata is present with a project ID, insert a corresponding
     * record into the datamart.
     *
     * Parameters: file - string (may be quite large)
     *             metadata - array (may contain nested array)
     * Returns: fileID - string
     */
    public function putFile($file, $metadata, $options) {
      $insertIntoDatamart = True;

      $dDocName = isset($options['fileID']) ? $options['fileID'] : '';

      $Datamart = new Datamart();
      $moreMetadata = $metadata['moreMetadata'];

      //12/11/2013 - This value was being sent in a different case, causing the check to fail.
      if (!isset($moreMetadata['projectid']) && isset($moreMetadata['projectID'])){
	$moreMetadata['projectid'] = $moreMetadata['projectID'];
        $metadata['moreMetadata'] = $moreMetadata;
      }

      if (!isset($moreMetadata['projectid'])){
          $insertIntoDatamart = False;
      }

      // Set  parameters
	  $this->params = array('dDocName'	=> $dDocName,
	  				'dDocTitle'			=> ($metadata["title"]),
	  				'dDocType'			=> $this->DOCTYPE,
	  				'dDocAuthor'		=> $this->USERNAME,
	  				'dSecurityGroup'	=> $this->SG,
	  				'dDocAccount'		=> $this->ACCT,
	  				'CustomDocMetaData'	=>  array('property' => array( array('name' => 'xMoreMetadata', 'value' => json_encode($metadata["moreMetadata"]) ),
	  																	array('name' => 'xOriginalAuthor', 'value' => $metadata["author"] ),
	  																	array('name' => 'xPubDate', 'value' => $metadata["pubdate"] ),
	  																	array('name' => 'xAppSystemID', 'value' => $metadata["appsysid"] ),
	  																	array('name' => 'xAppDocumentType', 'value' => $metadata["appdoctype"] )
	  											  )				),
	  				'primaryFile'		=> array('fileName' => $metadata["filename"], 'fileContent' => $file),
	  		//		'alternateFile'		=> '',
	  		//		'extraProps'		=>
	  );

	  // Create the SOAP connection objects
	  $this->CreateConnection($this->WSDL_PUT_FILE);

	  $response	= $this->getSoapResponse("CheckInUniversal", "CheckInUniversalResult");
	  $fileID	= $response->CheckInUniversalResult->dDocName;

	  $this->DestroyConnection();
	  if ($this->DEBUG) {
	    $this->debugLog();
	  }

      if ($insertIntoDatamart) {
        try {
            // Add projectdocument record to Datamart
	     $projectID = $moreMetadata['projectid'];
	     // Use original file size as pdffilesize, this will be wrong if the
            // file is not a PDF but it's better than leaving the value blank.
            $metadata['filesize'] = strlen($file);
            $Datamart->addProjectDocument($projectID, $fileID, $metadata);
	    $Datamart->addToContainerDocs($projectID, $fileID, $metadata);
        } catch (Exception $e) {
            $msg = "Created fileID $fileID; " . $e->getMessage();
            $agent = "PUTFILE_QUEUE_1";
            $this->validateAgent($agent);
            $agent = $agent . "." . $fileID;
            $jobOptions = array();
            $wwwlink =""; // $Datamart->makeLinkURL($projectID, $fileID);
            $metadata['moreMetadata']['wwwlink'] = $wwwlink;
            $jobOptions = $this->getJobOptionMetadata($projectID, $fileID, $metadata);
			$jobOptions["DatamartError"] = $msg;
   	        $this->queueJob($agent, $jobOptions);
            //$code = $e->getCode();
            //throw new Exception($msg, $code);
        }
      }

      return $fileID;
    }

/*	* New Method created to get the metadata for putfile
	* when insertion into datamart is failing
	* Date : Dec 16, 2016
	* By : Chandana & Saravana
	*/
	function getJobOptionMetadata($projectID, $fileID, $metadata) {
	$jobOptions = array();
	try{
        	$jobOptions["projectID"] = $projectID;
		$jobOptions["fileID"] = $fileID;
		$jobOptions['docname'] = $metadata['title'];
    		$moreMetadata = $metadata['moreMetadata'];
		$jobOptions['datelabel'] = $moreMetadata['datelabel'];
		$jobOptions['docdate'] = $moreMetadata['docdate'];
		$jobOptions['addedby'] = $metadata['author'];
		$jobOptions['author'] = $moreMetadata['docauthor'];
		$jobOptions['addressee'] = $moreMetadata['addressee'];

		$phaseid = "";
    		if (isset($moreMetadata['caraphaseid'])){
        		$phaseid = $moreMetadata['caraphaseid'];
    		}
		$jobOptions['phaseid'] = $phaseid;

    		$description = "";
    		if (isset($moreMetadata['docsummary'])) {
      			$description = $moreMetadata['docsummary'];
    		}
		$jobOptions['description'] = $description;

    		$sensitiveflag = 0;
    		if (isset($moreMetadata['sensitiveflag'])) {
      		$flag = strtolower(trim($moreMetadata['sensitiveflag']));
      		switch ($flag) {
        		case "true":
          		  $sensitiveflag = 1;
	  		  $sensitiverat = $moreMetadata['sensitiverat'];
          		  break;
        		case "false":
          		  $sensitiveflag = 0;
          		  break;
        		case "1":
          		  $sensitiveflag = 1;
	  		  $sensitiverat = $moreMetadata['sensitiverat'];
          		  break;
        		case "0":
         		   $sensitiveflag = 0;
          		  break;
      			}
    		}

   		$jobOptions['sensitiveflag'] = $sensitiveflag;
   		$jobOptions['sensitivityrationale'] = $sensitiverat;

    		$pubflag = 0;
    		if (isset($moreMetadata['pubflag'])) {
      		// Requester should make sure input is restricted to 1/0
      		$pubflag = $moreMetadata['pubflag'] ? 1 : 0;
    		}

		$jobOptions['pubflag'] = $pubflag;

//	$wwwlink = $Datamart->makeLinkURL($projectID, $fileID);
//	$jobOptions['wwwlink'] = $wwwlink;

   		$wwwlink = "";
    		if (isset($moreMetadata['wwwlink'])) {
      		$wwwlink = $moreMetadata['wwwlink'];
   		}
		$jobOptions['wwwlink'] = $wwwlink;


    // If there is a filesize member in the metadata, assume this is a PDF,
    // go ahead and set pdffilesize.

	$pdffilesize = "";
    	if (isset($metadata['filesize'])) {
      	$filesize = round($metadata['filesize'] / 1024);
      	if ($filesize == 0) { $filesize = 1; }
      	$pdffilesize = "${filesize}kb";
    	}
	$jobOptions['pdffilesize'] = $pdffilesize;

	$contid = "";

	if (isset($moreMetadata['level3id'])) {
		$contid = $moreMetadata['level3id'];
    	}else if (isset($moreMetadata['level2id'])) {
    		$contid = $moreMetadata['level2id'];
    	}else if (isset($metadata['appdoctype'])){
		$apptype = $metadata['appdoctype'];
		if (strcmp( $apptype, 'Pre-Scoping') == 0){
			$contid = "-1";
		}else if (strcmp( $apptype, 'Scoping') == 0){
			$contid = "-2";
		}else if (strcmp($apptype, 'Analysis') == 0){
			$contid = "-3";
		}else if (strcmp($apptype, 'Supporting') == 0) {
			$contid = "-4";
		}else if (strcmp($apptype, 'Decision') == 0) {
			$contid = "-5";
		}else if (strcmp($apptype, 'Post-Decision') == 0){
			$contid = "-6";
		}else if (strcmp($apptype, 'Assessment') == 0){
			$contid = "-7";
		}else if (strcmp($apptype, 'Forest Plan') == 0){
			$contid = "-8";
		}else if (strcmp($apptype, 'Litigation') == 0){
			$contid = "89";
		}
	}
	$jobOptions['contid'] = $contid;
	} catch (Exception $e) {
		$jobOptions['error'] = $e->getMessage();

        }
	return $jobOptions;
  }
/*
 *	function: cacheJobFile(agent, fileData)
 *
 *	Given a (large) data file as part of a publish/transfer request,
 *	store the data file somewhere and return a "handle" to the file.
 *
 *	In this implementation, the function returns a filename in
 *	job_queues/jobfiles where the file is stored, but it could be
 *	rewritten to handle this function in any arbitrary manner.
 *
 *	The job processor script must understand how to retrieve files
 *	from the cache, given this handle.
 */
public function cacheJobFile($agent, &$fileData) {
  $CACHEDIR = realpath(dirname(__FILE__)) . "/../job_queues/jobfiles";
  $tmpFileName = tempnam($CACHEDIR, $agent . '-');
  $fp = fopen($tmpFileName, 'w');
  fwrite($fp, $fileData);
  fclose($fp);
  chmod($tmpFileName, 0666);	// remember to use octal
  return basename($tmpFileName);
}

/*
 *	function: queueJob(agent, [jobOptions])
 *	where	agent = name of agent
 *		jobOptions = list of options for the agent script,
 *			    these should be in the form of an
 *			    associative array (the cached file name
 *			    would be one of these)
 *	converts all of these options into XML and stores the
 *	file in the in_q directory as "agent.<unique_jobid>"
 */
public function queueJob($agent, $jobOptions) {
  global $TXID;

  $TMPDIR = realpath(dirname(__FILE__)) . "/../tmpdir";
  $QDIR = realpath(dirname(__FILE__)) . "/../job_queues/in_q";
  #
  #	Job is created in tempdir, then moved to qdir when done.
  #	This makes the queueing operation atomic.
  #
  $xmlstr = <<<XML
<?xml version='1.0' standalone='yes'?>
    <job>
    <agent>dummy</agent>
    <options></options>
    </job>
XML;

  $jobxml = new SimpleXMLElement($xmlstr);

  $jobxml->agent = $agent;

  foreach ($jobOptions as $optname => $optval) {
    $jobxml->options->addChild($optname, $optval);
  }

  $tmpFileName = tempnam($TMPDIR, $agent . '.');
  $fp = fopen($tmpFileName, 'w');
  fwrite($fp, $jobxml->asXML());
  fclose($fp);
  chmod($tmpFileName, 0666);	// remember to use octal

  // $tmpJobFileName = tempnam($QDIR, $agent . '.');
  $tmpJobFileName = "${QDIR}/${agent}.${TXID}";

  // Now move job into the incoming queue
  if (rename($tmpFileName, $tmpJobFileName) == False) {
    throw new Exception("Error queuing job", BROKER_SYSTEM_EXCEPTION);
  }
}

    /*
     *  Puts a Merged File
     * Parameters:
     * Returns: nothing
     *
     */
    public function putMergedFile($idList, $metadata) {
      $filesToMerge = array();
      $mergedFileName = '';

      try {

        # Retrieve the files in the idList
        #   make temp directory
        #   getFileData($fileID, $options)
        #   write each file as 0.pdf, 1.pdf, etc., named for array index
        $filesToMerge = array();
        $options = array('fileType' => 'PDF');
        foreach ($idList as $id) {
          $fileData = $this->getFileData($id, $options); // Should catch and rethrow?
          $tmpFileName = tempnam('/tmp', 'drmPDFMerge');
          $fp = fopen($tmpFileName, 'w');
          fwrite($fp, $fileData);
          fclose($fp);
          $filesToMerge[] = $tmpFileName;
        }

        # Execute the merge application
        #   pdfmerge <file1> <file2> <outputfile>

        $tmpFileName = tempnam('/tmp', 'drmPDFMergedFile');
        $cmd = '/var/www/html/dmdpilot/pdfmerge/pdfcat ' . join(' ', $filesToMerge) . ' ' . $tmpFileName;
        exec($cmd, $output, $rc);

        if ($rc != 0) {
          throw new Exception("Error merging files, rc = " . $rc,
            BROKER_SYSTEM_EXCEPTION);
        }

        $file = file_get_contents($tmpFileName);

        # Check in the new file using the supplied metadata
        #   read merged.pdf into $file
        #   putFile($file, $metadata);
        $mergedFileID = $this->putFile($file, $metadata);

        // Global clean-up
        foreach ($filesToMerge as $fileName) {
          unlink($fileName);
        }
        unlink($mergedFileName);

        return $mergedFileID;

      } catch (Exception $e) {

        // Global clean-up
        foreach ($filesToMerge as $fileName) {
          unlink($fileName);
        }
        if ($mergedFileName != '') {
          unlink($mergedFileName);
        }

        throw $e;
      }
    }



    /*
     * Update the metadata of a File in the system
     * Maintain the corresponding Datamart record, if it exists
     * Use all values from newMetadata
     * Opts are not actually used here
     * Parameters: id     - string
     *			   metadata - array (may contain nested array)
     * Returns: true
     *
     */
    public function replaceFileMetadata($id, $newMetadata, $opts = array()) {

	  $title		= $newMetadata["title"];
	  $author		= $newMetadata["author"];
	  $pubdate		= $newMetadata["pubdate"];
	  $appsysid		= $newMetadata["appsysid"];
	  $appdoctype		= $newMetadata["appdoctype"];
	  $moreMetadata		= $newMetadata["moreMetadata"];
	  $filename		= $newMetadata["filename"];
	  $filesize		= $newMetadata["filesize"];

      // Set  parameters
	  $this->params = array('dDocName'		=> $id,
	  				'dRevLabel' => 1,
	  				'dDocTitle'			=> ($title),
	  				'dDocType'			=> $this->DOCTYPE,
	  				'dDocAuthor'		=> $this->USERNAME,
	  				'dSecurityGroup'	=> $this->SG,
	  				'dDocAccount'		=> $this->ACCT,
	  				'CustomDocMetaData'	=>  array('property' => array( array('name' => 'xMoreMetadata', 'value' => json_encode($moreMetadata) ),
	  																	array('name' => 'xOriginalAuthor', 'value' => $author ),
	  																	array('name' => 'xPubDate', 'value' => $pubdate ),
	  																	array('name' => 'xAppSystemID', 'value' => $appsysid ),
	  																	array('name' => 'xAppDocumentType', 'value' => $appdoctype )
	  											  )				),
	  		//		'extraProps'		=>
	  );

	  // Create the SOAP connection objects
	  $this->CreateConnection($this->WSDL_UPDATE_FILE);

	  $response	= $this->getSoapResponse("UpdateDocInfoByName", "UpdateDocInfoByNameResult");

	  $this->DestroyConnection();
	  if ($this->DEBUG) {
	    $this->debugLog();
	  }

      // Get PDF file size
//      $pdffilesize = $filesize;	// Default to original file size
//     try {
//        $pdfopt = array('fileType' => 'PDF');
//        $pdfmetadata = $this->getFileMetadata($id, $pdfopt);
//       $pdffilesize = $pdfmetadata['filesize'];
//      } catch (Exception $e) {
//      }


//      $metadata = array(
//      		'appsysid' => $appsysid,
//      		'appdoctype' => $appdoctype,
//      		'filename' => $filename,
//      		'filesize' => $pdffilesize,
//      		'title' => $title,
//      		'author' => $author,
//      		'pubdate' => $pubdate,
//     		'moreMetadata' => $moreMetadata
//     );

// PALS 4.5 - Metadata is being updated to DataMart seperately
//      $Datamart = new Datamart();
//      $Datamart->updateProjectDocuments($id, $metadata);
      return true;
    }



    /*
     * Delete a File from the system
     * Parameters: fileID - string
     * Returns: nothing
     *
     * Note: Use with caution! This doesn't simply hide the doc. It really deletes it from the system!
     */
    public function deleteFile($fileID) {

	    $Datamart = new Datamart();
    	//Remove any ProjectDocuments associated with this fileId
    	try {
    		$Datamart->deleteProjectDocuments($fileID);
    	} catch (Exception $e) {
    		// No error if project-document record is not in Datamart
    		if ($e->getCode() != DATAMART_RECORD_NOT_FOUND) {
    			throw $e;
    		}
    	}
	  // Set  parameters
	  $this->params = array('dDocName' => $fileID);

	  // Create the SOAP connection objects
	  $this->CreateConnection($this->WSDL_DELETE_FILE);

	  // Make the call
	  $response = $this->getSoapResponse("DeleteDocByName", "DeleteDocByNameResult");

	  // Clean up garbage
	  $this->DestroyConnection();

	  // Log any errors
	  if ($this->DEBUG) {
	    $this->debugLog();
	  }

      return true;

    }


    /*
     * Generate a unique File ID
     * Parameters: None
     * Returns: A unique File ID
     *
     */
    public function generateFileID() {

      // Create the SOAP connection objects
	  $this->CreateConnection($this->WSDL_GENERATE_FILE_ID);

	  // Make the call
	  $response = $this->getSoapResponse("GetUniqueCheckinID", "GetUniqueCheckinIDResult");
	  $fileID = $response->GetUniqueCheckinIDResult->dDocName;

	  // Clean up garbage
	  $this->DestroyConnection();

	  // Log any errors
	  if ($this->DEBUG) {
	  	 $this->debugLog();
	  }

      return $fileID;
    }


    /*
     * Retrieve a list of all documents that have not been successfully converted and released
     * Parameters: none
     * Returns: Document Info of the problem docs
     */
    public function getStatusList() {
	  $metadataOut = array();

	  // Set  parameters
	  $this->params = array();

	  $this->CreateConnection($this->WSDL_GET_FILE_METADATA);
	  $response = $this->getSoapResponse("DocStatusList", "DocStatusListResult");

	  $this->DestroyConnection();
	  if ($this->DEBUG) {
	    $this->debugLog();
	  }

      return $response;
    }



/* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\  */



    	/*
		 *
		 * Receives a response string, presumable formatted as XML, and checks it
		 *   for validity. Checks for null response, malformed structure,
		 *   existence of appropriate nodes, and error codes present in the response.
		 *   Throws a PHP Exception if anything suspicious is found
		 *	 Always returns true
		 */
		private function CheckResponse($strXml, $eR) {
		  // Check for null or un-set parameter
		  if (isset($strXml)) {
		    if (empty($strXml)) {
		      throw new Exception("No response from repository (response empty).", REPOSITORY_TIMEOUT_EXCEPTION);
		    }
		  } else {
		    throw new Exception("No response from repository (response unset).", REPOSITORY_TIMEOUT_EXCEPTION);
		  }

		  // Check for the existence of expected response ($eR)
		  if (!isset($strXml->$eR)) {
		      throw new Exception("Expected value of '" . $eR . "' not found in response.", BAD_XML_EXCEPTION);
		  }

		  // ok, so if we've gotten this far, we know we've got good XML.
		  // Now check for existence of error codes within the XML response
		  if (isset($strXml->$eR->StatusInfo)) {
		  	$sm = $strXml->$eR->StatusInfo->statusMessage;
		  	$sc = $strXml->$eR->StatusInfo->statusCode;
		  }

		  if (isset($sc)) {
		    // Success returns "0", which is interpreted by PHP as FALSE. Therefore,
		    //		 anything other than FALSE means an error has occurred.
		    if ($sc) {
		      if (substr_count($sm, "The content ID is not unique")>0) {
		        throw new Exception("Status Code: " . $sc . " Status Message: " . $sm, FILEID_ALREADY_EXISTS_EXCEPTION);
		      } elseif (substr_count($sm, "Unable to find latest revision of ")>0) {
		        throw new Exception("Status Code: " . $sc . " Status Message: " . $sm, FILE_NOT_FOUND_EXCEPTION);
		      } elseif (substr_count($sm, "Unable to execute service method 'createFileName'. (System Error: Runtime error: java.lang.NullPointerException")>0) {
		        throw new Exception("Status Code: " . $sc . " Status Message: " . $sm, FILETYPE_NOT_FOUND_EXCEPTION);
		      } else {
		        throw new Exception("Status Code: " . $sc . " Status Message: " . $sm, REPOSITORY_SYSTEM_EXCEPTION);
		      }
		    }
		  }

		  return true;
		}


    	/*
		 *
		 * The XMLRPC vehicle does not seem to like our errors,
		 *   so we spit them out to a logfile
		 *
		 */
		private function debugLog() {

		  $logMsg = "<HTML><HEAD><TITLE>DRM Broker: Server-Side Output</TITLE></HEAD><BODY>" . $this->strLog . "</BODY></HTML>";

		  $log = fopen($this->LOGFILE,"w+");
		  fwrite($log,$logMsg,strlen($logMsg));
	      fclose($log);

		}


		/*
		 *  Append to the log string
		 */
		private function appendLog($s) {
		  $this->strLog .= "<pre>" . $s . "</pre>";
		}



		/*
		 *
		 * The custom Metadata fields are embedded in this hellish array of doom.
		 * 		Their position must be extracted by hand before we can reference them
	     *
	     */
		private function getCustomMetadataLocation($array, $field) {
		  $rval = -1;
		  for ($loc=0;$loc<count($array);$loc++) {
		    if ($array[$loc]->name == $field) {
		      $rval = $loc;
		  	}
		  }
	      return $rval;
	  	}




		/*
		 *
		 */
	  	private function CreateConnection($ws) {
		  $options = array(
			'login' => $this->USERNAME,
			'password' => $this->PASSWORD,
			'encoding' => mb_internal_encoding(),
			'exceptions' => True
                  );
		  if ($this->DEBUG) {
			$options['trace'] = 1;
		  }
		  try {
			$this->client = new SoapClient($ws, $options);
		  } catch (Exception $e) {
			throw new Exception($e->__toString(), REPOSITORY_SYSTEM_EXCEPTION);
		  }
		  $this->proxy = $this->client;
	  	}


	  	/*
		 *
		 */
	  	private function DestroyConnection() {
		  unset($this->proxy);
	  	  unset($this->client);
	  	}


	    /*
	     *
		 */
	  	private function getSoapResponse($func, $expectedResponse) {
		  $responseXml = $this->proxy->$func($this->params);

		  if(is_soap_fault($responseXml)) {
			throw new Exception("Fault Code: " . $responseXml->faultcode . " Fault String: " . $responseXml->faultstring, REPOSITORY_SYSTEM_EXCEPTION);
		  } else {
		    if ($this->DEBUG) {
			  $this->appendLog("SOAP communication successful:");
			  $this->appendLog("Request");
			  $this->appendLog(htmlspecialchars($this->proxy->__getLastRequest(), ENT_QUOTES));
			  $this->appendLog("Response");
			  $this->appendLog(htmlspecialchars($this->proxy->__getLastResponse(), ENT_QUOTES));
	  	    }
		  }

		  $this->CheckResponse($responseXml, $expectedResponse);

	      return $responseXml;
	    }

   public function endswith($string, $test){

	$strlen = strlen($string);
	$testlen = strlen($test);
	if ($testlen > $strlen) return false;
	return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;

   }


    public function publishFile($agent, $id, $opts) {
      $this->validateAgent($agent);

      # set default file type
      $getOptions = array('fileType' => 'PDF');

      # override default file type, if specified
      if (array_key_exists('fileType', $opts)) {
        $getOptions['fileType'] = $opts['fileType'];
      }
	# Get PDF file metadata
      $metadata = null;

      try {
        $pdfopt = array('fileType' => 'PDF');
        $metadata = $this->getFileMetadata($id, $pdfopt);

	//Test if this is actually a zip file.  If so, change targetfile to the the filename.
	$filename = $metadata['filename'];
	if ( $this->endswith($filename, "zip")){
		unset($getOptions['fileType']);
		$opts["targetname"] = $filename;

	}else{
	//This is not a zip file
	}

      } catch (Exception $e) {
        if ($e->getCode() == FILETYPE_NOT_FOUND_EXCEPTION) {
          throw new Exception("File conversion incomplete for " . $id . ", cannot be published", FILETYPE_NOT_FOUND_EXCEPTION);
        } else {
          throw $e;
        }
      }

      # get file
      if ($getOptions['fileType'] == 'original') {
        unset($getOptions['fileType']);
      }
      $fileData = $this->getFileData($id, $getOptions);

      # Store data file
      $tempfile = $this->cacheJobFile($agent, $fileData);

      # Queue new job
      $jobOptions = $opts;
      $jobOptions["cached_file"] = $tempfile;
      $this->queueJob($agent, $jobOptions);

      # Update Datamart with PDF file size
      $moreMetadata = $metadata['moreMetadata'];
      if (array_key_exists('projectid', $moreMetadata)) {
        $projectID = $moreMetadata['projectid'];
        $Datamart = new Datamart();
        $Datamart->updateProjectDocument($projectID, $id, $metadata);
      }
      return true;
    }

    public function transferFile($agent, $file, $options) {
      $this->validateAgent($agent);

      # Store data file
      $tempfile = $this->cacheJobFile($agent, $file);

      # Queue new job
      $jobOptions = $options;
      $jobOptions["cached_file"] = $tempfile;
      $this->queueJob($agent, $jobOptions);

      return true;
    }

    private function validateAgent($agent) {
      $agentList = array();
      $agentdir = realpath(dirname(__FILE__) . "/../jobctl/agents");

      if ($handle = opendir($agentdir)) {
        while (false !== ($file = readdir($handle))) {
          if ($file[0] != ".") {
            $agentList[] = $file;
          }
        }
        closedir($handle);
      } else {
	throw new Exception("Error while validating agent", BROKER_SYSTEM_EXCEPTION);
      }
      if (!in_array($agent, $agentList)) {
        throw new Exception("Invalid agent", INVALID_AGENT_EXCEPTION);
      }
    }

    /*
     * Error-handler function to temporarily recognize warnings as errors
     * (because errors from the Lucene search engine are only reported as
     * warnings, and would otherwise be missed).
     *
     * Returning False causes control to pass to the default error handler.
     *
     * Use set_error_handler() and restore_error_handler() to activate.
     *
     * Note that in order to be used as an error handler from within an object
     * this function has to be public and static
     */
    public static function tempErrorHandler($errno, $errstr, $errfile, $errline) {
      if ($errno == E_WARNING) {
        throw new Exception($errstr, $errno);
      } else {
        return False;
      }
    }

    /*
     * Issue search request to Solr, format results
     *
     * Our current search engine is Solr, listening on port 8983
     *
     * Options allowed from spec are:
     * indexname (string, must be PALSDOCS)
     * countitems (int optional, defaults to 10)
     * startitem (int optional, defaults to 0)
     * pagenum (int optional, defaults to 0)
     * collapse (boolean optional, defaults to 0)
     * projectids (array of int optional)
     */
    public function searchDocs($querystring, $opts) {
      // Set defaults
      $countitems = 10;
      $startitem = Null;
      $pagenum = Null;

      // Assume all options have been validated before we get here
      $indexname = $opts[OPT_INDEXNAME];
      if ($indexname == "PALSDOCS") {

        $collapse = False;
        $projectids = Null;

        foreach ($opts as $opt => $val) {
          $$opt = $val;
        }

        if (!is_null($startitem)) {
          $pagenum = Null;	// ignore pagenum if startitem given
        } else {
          if (!is_null($pagenum)) {
            $startitem = $pagenum * $countitems;
          } else {
            $startitem = 0;
          }
        }

# Execute search
# ==============
# using querystring, startitem, countitems, collapse, and projectids
# Search syntax:
# HTTP GET
# localhost:8983/solr/select/?q=search text
# &start=0&rows=10
# &fl=docid,metadata,score
# &hl=true&hl.fl=text (these could be put into the main config file too)
# &fq=(projid1 projid2 projid3)
# &collapse.field=projectid

        $projectid_filt = "";
        $projectid_count = 0;
        if (is_null($projectids) == False) {
          $projectid_count = count($projectids);
          if ($projectid_count > 0) {
            $projectid_filt = "&fq=projectid:(" .
              urlencode(implode(' ', $projectids)) . ")";
          }
        }
        $collapse_term = "";
        if ($collapse) {
          $collapse_term = "&collapse.field=projectid";
        }

        // Make empty querystring match everything
        if (strlen($querystring) === 0) {
          $querystring = "*:*";
        }

        $queryService = SOLR_SERVER . "select/";
	$query = "q=" . urlencode($querystring) .
	  "&indent=y" .	# for debugging
          "&start=${startitem}&rows=${countitems}" .
          "&fl=docid,metadata,score&hl=true&hl.fl=text" .
          "&hl.usePhaseHighlighter=true&hl.requireFieldMatch=true" .
          "&hl.maxAnalyzedChars=100000" .
          $projectid_filt . $collapse_term;

# Package results
# ===============
# Everything is wrapped in a <response/>
#
# <result>
#   <doc>
#     <str name="docid"/>
#     <str name="projectid"/>
#     <float name="score"/>
#   </doc>
# </result>
#
# If collapse is specified, response will contain a
# <lst name='collapse_counts'>
#   <str name="field">name of collapse field</str>
#   <lst name="results">
#     <lst name="Doc ID">
#       <int name="collapseCount">number of hidden items</int>
#       <str name="fieldValue">the collapsed projectid</str>
#     </lst>
#   </lst>
# </lst>
#
# If highlighting is specified, response will contain a
# <lst name='highlighting'>
#   <lst name="Doc ID">
#     <arr name="text">
#       <str>highlighted text</str>
#       (There could be multiple strings in here if so configured)
#     </arr>
#   </lst>
# </lst>

# Result format is:
# {
#   resultmetadata {
#     rowcount:
#     msize:
#   }
#   hitlist {
#     {
#       rownum:
#       relevance:
#       docid:
#       collapsed:
#       htmlcontext:
#       metadata:
#     }
#   }

        $ctx_params = array(
          'http' => array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
          )
        );
        $ctx_params['http']['content'] = $query;
        $context = stream_context_create($ctx_params);

        // Execute search
        $response_xml = False;
        try {
          set_error_handler(array('DrmProxy', 'tempErrorHandler'));
          $response_xml = file_get_contents($queryService, false, $context);
          restore_error_handler();
        } catch (Exception $e) {
          restore_error_handler();
          $msg = $e->getMessage();

$SOLR_ERRTXT_WILDCARD = "/not_allowed_as_first_character_in_Wildcard/";
$SOLR_ERRTXT_INACTIVE = "/failed to open stream: Connection refused/";
$SOLR_ERRTXT_BOOLEANS = "/too_many_boolean_clauses/";

          switch ($e->getCode()) {

            case E_WARNING:
              if (preg_match(
                "/not_allowed_as_first_character_in_Wildcard/", $msg) > 0) {
                throw new Exception("Cannot start a term with a wildcard",
                  BROKER_SYSTEM_EXCEPTION);
              }
              if (preg_match(
                "/failed to open stream: Connection refused/", $msg) > 0) {
                throw new Exception("Cannot connect to search engine",
                  BROKER_SYSTEM_EXCEPTION);
              }
              if (preg_match("/too_many_boolean_clauses/", $msg) > 0) {
                throw new Exception("Too many filter terms (" .
                  strval($projectid_count) . ") in query",
                  BROKER_SYSTEM_EXCEPTION);
              }
              $SOLR_ERRPARSE = "orgapachelucenequeryParserParseException_";
              if (preg_match("/$SOLR_ERRPARSE/", $msg) > 0) {
                $errmsg = preg_replace("/.*$SOLR_ERRPARSE/", '', $msg);
                throw new Exception("$errmsg",
                  BROKER_SYSTEM_EXCEPTION);
              }
              throw new Exception("Unhandled search condition: $msg",
                BROKER_SYSTEM_EXCEPTION);
              break;
          }
        }
        if ($response_xml === False) {
	  throw new Exception("No response from search application?",
            BROKER_SYSTEM_EXCEPTION);
        }
        $dom = new SimpleXMLElement($response_xml);

        $resultmetadata = Array(
          'rowcount' => count($dom->result->doc),
          'msize' => intval($dom->result['numFound']),
          'original_query' => strval($querystring)
        );
        $hitlist = Array();

        $highlights = Array();
        $collapseInfo = Array();
        foreach ($dom->lst as $section) {
          switch (strval($section['name'])) {

            case "highlighting":
              foreach ($section->lst as $doc) {
                $docid = strval($doc['name']);
                $snippet = strval($doc->arr->str);
                $snipWords = preg_split("/[\s]+/", $snippet);
                $snippet = implode(" ", $snipWords);
/*
                // Remove UTF8 BOM efbbbf (slightly overaggressive)
                $highlights[$docid] = trim($snippet,
                  "\x20\x09\x0A\x0D\x00\x0B\xef\xbb\xbf");
*/
                $highlights[$docid] = $snippet;
              }

            case "collapse_counts":
              foreach ($section->lst->lst as $doc) {
                $docid = strval($doc['name']);
                $numCollapsed = intval($doc->int);
                $collapseInfo[$docid] = $numCollapsed;
              }

            default:
              # Other sections are informational headers--do nothing
          }
        }

        $hitlist = Array();
        $numDocs = count($dom->result->doc);
        for ($idx = 0; $idx < $numDocs; $idx++) {
          $doc = $dom->result->doc[$idx];
          $hit = Array();
          $hit['rownum'] = $startitem + $idx;
          $hit['relevance'] = intval(floatval($doc->float) * 100);
          $docid = strval($doc->str[0]);
          $hit['docid'] = $docid;
          $hit['collapsed'] = 0;
          if (array_key_exists($docid, $collapseInfo)) {
            $hit['collapsed'] = $collapseInfo[$docid];
          }
          $hit['htmlcontext'] = '';
          if (array_key_exists($docid, $highlights)) {
            $hit['htmlcontext'] = $highlights[$docid];
          }
          $hit['metadata'] = json_decode(strval($doc->str[1]), true);
          $hitlist[] = $hit;
        }


        $response = Array(
          'resultmetadata' => $resultmetadata,
          'hitlist' => $hitlist);
        return $response;
      }
    }


    // Convert the serialized moreMetadata array back into an array
    //
    // We changed from PHP serialization to JSON serialization, so
    // we will inspect and convert the string as appropriate.
    //
    // Note that json_encode will encode an empty PHP array as [], but
    // if the array has contents, it will be encoded as an object, with {}
    // brackets.
    private function getMoreMetadata($mmdString) {
        $result = array();
        $mmdString = preg_replace('/&#34;/', '"', $mmdString); // HACK
        if ($mmdString === "[]") {
            // Handle empty moreMetadata
            $result = array();
        } else if ($mmdString[0] == '{') {
            // Handle JSON object (assoc array)
            $result = json_decode($mmdString, true);
        } else {
            // Handle serialized PHP
            $result = unserialize($mmdString);
        }
        if (is_array($result) !== True) {
	  throw new Exception("Unable to read moreMetadata format?",
            BROKER_SYSTEM_EXCEPTION);
        }
        return $result;
    }

    /*
     * Get list of new files for the given job ID
     *
     * Issue a GET to the notifier service at DRM_SERVER/dmd/getNewFileInfo,
     * get back a serialized PHP array containing either the full response
     * (including a resultmetadata header), or a pair of reason/message
     * members indicating an error
     */
    public function getNewFileList($jobid) {
      $notifierURL = "https://" . FILERECORDER_SERVER . "/filerecorder/getNewFileInfo.php?jobid=" . urlencode($jobid);
      $response = unserialize(file_get_contents($notifierURL));
      if (isset($response['reason'])) {
        if ($response['reason'] == 'NOJOB') {
          throw new Exception($response['message'], INVALID_JOB_ID_EXCEPTION);
        } else {
          throw new Exception("getNewFileList error: " . $response['message'],
            BROKER_SYSTEM_EXCEPTION);
        }
      }
      return $response;
    }

    /*
     *	Forward mailing list request to GovDelivery
     *  Returns true on success
     *  Throws exception on failure
     */
    public function createNEPAMailingList($palsid, $shortname) {
      $gd = new GovDelivery();
      $gd->addTopic(trim($palsid));
      // GovDelivery response consists of XML about the new topic,
      // not necessary to return this to the caller

      // Send an alert email to fs-support
      $datamart = new Datamart();
      $dom = $datamart->getProjectDOM($palsid);
      $projectname = strval($dom->name);
      $adminunit = strval($dom->unitcode);

      //error_log(var_dump($dom), 3, "/var/www/html/mlm/errors.log");
      $to = "jeremy.lam@dynamotechnologies.com,derek.schaefer@dynamotechnologies.com,emnepa@fs.fed.us";


      $headers = "From: DMD Service Broker<noreply@dynamotechnologies.com>\n";
      $hostname = gethostname();
      $subject = "$hostname: NEPA mailing list created";
      $msg = "PALS User ";
      $msg .= $shortname;
      $msg .= " has requested the activation of a mailing list for:\n";
      $msg .= "\n";
      $msg .= "Project #: $palsid\n";
      $msg .= "Project name: $projectname\n";
      $msg .= "Administrative unit code: $adminunit\n";
      $msg .= "\n";
      $msg .= "=================\n";
      mail($to, $subject, $msg, $headers);

      return true;
    }

    /*
     *  Merge contents of $changes into $original
     *
     *  This is because I don't really trust PHP array_merge()
     */
    public function mergeArrays($original, $changes) {
      foreach ($changes as $key => $val) {
        $original[$key] = $val;
      }
      return $original;
    }

    public function markNEPAAide($docID, $moreMetadata) {
      $currentMD = $this->getFileMetadata($docID);
      $mmd = $currentMD['moreMetadata'];

      // If this is already a NEPA Aide, do nothing
      if (array_key_exists("unit", $mmd)) {
        return True;
      }

      // Merge with existing document metadata
      $currentMD['moreMetadata'] = $this->mergeArrays($mmd, $moreMetadata);
      $this->replaceFileMetadata($docID, $currentMD);

      return True;
    }

    public function unmarkNEPAAide($docID) {
      // Delete Aide fields from moreMetadata
      $aideFields = array(
        'unit',                 // unit code
        'analtype',             // one of [CE|EA|EIS|All]
        'aidetype',             // one of [Template|Checklist|Guidance|Example]
        'aidesummary',          // text, max 250 characters
        'avgrank',              // float
        'totalreviews');        // int

      $currentMD = $this->getFileMetadata($docID);
      $mmd = $currentMD['moreMetadata'];

      // If this is not a NEPA Aide, do nothing
      if (array_key_exists("unit", $mmd) === False) {
        return True;
      }

      foreach ($aideFields as $fieldname) {
        unset($mmd[$fieldname]);
      }
      $currentMD['moreMetadata'] = $mmd;
      $this->replaceFileMetadata($docID, $currentMD);

      return True;
    }

    public function updateNEPAAide($docID, $moreMetadata) {
      // Merge with existing document metadata
      $currentMD = $this->getFileMetadata($docID);
      $mmd = $currentMD['moreMetadata'];

      // If this is not a NEPA Aide, throw an exception
      if (array_key_exists("unit", $mmd) === False) {
        throw new Exception("Document $docID is not an Aide, cannot update", INVALID_METADATA_FIELD_EXCEPTION);
      }

      $currentMD['moreMetadata'] = $this->mergeArrays($mmd, $moreMetadata);
      $this->replaceFileMetadata($docID, $currentMD);

      return True;
    }

  }

?>

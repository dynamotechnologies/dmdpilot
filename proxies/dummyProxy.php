<?php
  require_once realpath(dirname(__FILE__)) . "/../interfaces/dummyIntf.php";
  require_once realpath(dirname(__FILE__)) . "/../interfaces/drmIntf.php";
  require_once realpath(dirname(__FILE__)) . "/../includes/consts.php";

  $TMPDIR = realpath(dirname(__FILE__)) . "/../tmpdir";

/*
 * Define dummy proxy class
 * This object should implement *ALL* of the available
 * interfaces for testing purposes
 */
  class DummyProxy implements dummyIntf, drmIntf {

    /******************
     * Dummy functions
     ******************/

    function helloWorld() {
      return "Hello, world!";
    }

    function echoMsg($message) {
      return $message;
    }

    function zipPhaseDocs($parama, $paramb, $paramc) {
      return "Hello, zip1!";
    }

    function associateDoc($projectId,  $fileID) {

    	return "associateDoc ".$fileID." to ".$projectId;
    }

    function dissociateDoc($projectId,  $fileID) {
      return "dissociateDoc ".$fileID." from ".$projectId;
    }

    /****************
     * DRM functions
     ****************/

    function getFile($fileID, $options, &$fileDataOut, &$metadataOut) {
      $fileDataOut = $this->getFileData($fileID, $options);
      $metadataOut = $this->getFileMetadata($fileID, $options);
    }

    /*
     * Retrieve file data
     * Parameters: fileID - string
     * Returns: fileData - string (may be quite large)
     */
    function getFileData($fileID, $options) {
      global $TMPDIR;

      // Look up the filename
      $filename = "$TMPDIR/${fileID}.txt";
      $orig_filename = $filename;

      $fileType = "";
      if (isset($options['fileType'])) {
        $fileType = $options['fileType'];
      }

      // If fileType is anything other than PDF, act as if the alternate
      // version does not exist
      if ($fileType != "") {
        $filename = "/filedoesnotexist";
      }
      if ($fileType == "PDF") {
        $filename = realpath(dirname(__FILE__)) . "/../proxies/dummySample.pdf";
      }

      if (!is_readable($filename)) {
        if (file_exists($filename)) {
          throw new Exception("Can't retrieve file--check permissions",
            BROKER_SYSTEM_EXCEPTION);
        } else {
          if (file_exists($orig_filename)) {
            throw new Exception("File not found in [$fileType] format",
              FILETYPE_NOT_FOUND_EXCEPTION);
          } else {
            throw new Exception("File not found",
              FILE_NOT_FOUND_EXCEPTION);
          }
        }
      }

      // Return file data in a big string
      $mquote_cfg = get_magic_quotes_runtime();
      set_magic_quotes_runtime(false);
      $fp = fopen($filename, 'rb');
      $fileData = fread($fp, filesize($filename));
      set_magic_quotes_runtime($mquote_cfg);
      fclose($fp);

      return $fileData;
    }

    /*
     * Retrieve file metadata
     * Parameters: fileID - string
     * Returns: metadata - struct
     */
    function getFileMetadata($fileID, $options) {
      global $TMPDIR;

      // Look up the filename
      $filename = "$TMPDIR/${fileID}.txt";
      $orig_filename = $filename;

      $fileType = "";
      if (isset($options['fileType'])) {
        $fileType = $options['fileType'];
      }

      // If fileType is anything other than PDF, act as if the alternate
      // version does not exist
      if ($fileType != "") {
        $filename = "/filedoesnotexist";
      }
      if ($fileType == "PDF") {
        $filename = realpath(dirname(__FILE__)) . "/../proxies/dummySample.pdf";
      }

      if (!is_readable($filename)) {
        if (file_exists($filename)) {
          throw new Exception("Can't retrieve file--check permissions",
            BROKER_SYSTEM_EXCEPTION);
        } else {
          if (file_exists($orig_filename)) {
            throw new Exception("File not found in [$filetype] format",
              FILETYPE_NOT_FOUND_EXCEPTION);
          } else {
            throw new Exception("File not found",
              FILE_NOT_FOUND_EXCEPTION);
          }
        }
      }

      // Return file metadata
      $metadatafilename = "$TMPDIR/${fileID}_metadata.txt";
      $metadata = unserialize(file_get_contents($metadatafilename));

      if (!$metadata) {
        throw new Exception("Unable to reconstruct metadata!",
          BROKER_SYSTEM_EXCEPTION);
      }

      $metadata['filesize'] = filesize($filename);

      return $metadata;
    }

    /*
     * Store file data
     * Parameters: file - string (may be quite large)
     *             metadata - struct (may contain nested struct)
     * Returns: fileID - string
     */
    function putFile($file, $metadata, $options) {
      global $TMPDIR;

      // Make up a fileID
      $basepart = strtoupper(base_convert(time(), 10, 16));
      $randompart = strtoupper(base_convert(rand(0, 255), 10, 16));
      $fileID = $basepart . $randompart;

      // Store file
      if (!is_dir($TMPDIR)) {

        $oldmask = umask(0);
        // Suppress error messages--the default error reporting level
        // messes up phpxmlrpc server
        $oldErrReportLvl = error_reporting(0);
        $dirMade = mkdir($TMPDIR);
        error_reporting($oldErrReportLvl);
        umask($oldmask);

        if (!$dirMade) {
          throw new Exception("Can't create storage directory",
            BROKER_SYSTEM_EXCEPTION);
        }
      }

      $filename = "$TMPDIR/${fileID}.txt";
      $fp = fopen($filename, "wb");
      fwrite($fp, $file, strlen($file));
      fclose($fp);

      // Store metadata
      $metadatafilename = "$TMPDIR/${fileID}_metadata.txt";
      $fp = fopen($metadatafilename, "wb");
      $serialform = serialize($metadata);
      fwrite($fp, $serialform, strlen($serialform));
      fclose($fp);

      // Return the fileID
      return $fileID;
    }

    function deleteFile($fileID) {
      global $TMPDIR;

      $filename = "$TMPDIR/${fileID}.txt";

      // If file doesn't exist then just return
      if (!file_exists($filename)) {
        throw new Exception("Can't delete file--file " . $filename .
          " not found",
          FILE_NOT_FOUND_EXCEPTION);
      }

      // Otherwise delete the file and metadata
      if (!unlink($filename)) {
          throw new Exception("Can't delete file--check permissions",
            BROKER_SYSTEM_EXCEPTION);
      }
      $filename = "$TMPDIR/${fileID}_metadata.txt";
      if (!unlink($filename)) {
          throw new Exception("Can't delete metadata--check permissions",
            BROKER_SYSTEM_EXCEPTION);
      }
    }

    public function putMergedFile($idList, $metadata) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

    public function replaceFileMetadata($id, $newMetadata, $options) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

    public function publishFile($agent, $id, $options) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

    public function transferFile($agent, $file, $options) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

function makeContextSnippet($docid) {
  $snippet = "United States USDA Department of ~- Agriculture Forest Service Sawtooth National Forest Supervisor's Office 2647 Kimberly Rd. E.  Twin Falls, ID. 83301 208-737-3200 Fax: 208-737-3236 File Code: 1950/2720 Date: September 2, 2009 Dear Interested Citizen: I am writing to update you on the Galena <b>cell tower</b> proposal on the Sawtooth National Recreation Area. The Environmental Assessment (EA) for the '2009 Galena Summit Communications Project' has been completed.  I have decided to implement Alternative C - Forest Plan VQO Map Amendment and to deny the special use permit for the proposed <b>cell tower</b>. My Decision Notice and Finding of No Significant";
  return $snippet;
}

function updateMetadata(&$hit, $docid) {
  $newMeta = array(
    'filename' => '2009_galena_comm_project_coverltr.pdf',
    'filesize' => '491062',
    'title' => 'Sawtooth NRA Communication Site Designation (Galena Cell Tower)',
    'author' => 'merussell',
    'pubdate' => '20091111T10:00:37',
    'appsysid' => 'PALS',
    'appdoctype' => 'pdf'
  );
  foreach (array_keys($newMeta) as $key) {
    $hit['metadata'][$key] = $newMeta[$key];
  }
}

function makeDummyHitList() {

$hitlist = Array(

Array(
	'rownum'	=> 0,
	'relevance'	=> 99,
	'docid'		=> 'FSPLT1_012401',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '3003'
		)
	)
),

Array(
	'rownum'	=> 1,
	'relevance'	=> 99,
	'docid'		=> 'FSPLT1_012403',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '3003'
		)
	)
),

Array(
	'rownum'	=> 2,
	'relevance'	=> 98,
	'docid'		=> 'FSPLT1_012388',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '17746'
		)
	)
),

Array(
	'rownum'	=> 3,
	'relevance'	=> 95,
	'docid'		=> 'FSPLT1_012389',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '17746'
		)
	)
),

Array(
	'rownum'	=> 4,
	'relevance'	=> 92,
	'docid'		=> 'FSPLT1_008307',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '987'
		)
	)
),

Array(
	'rownum'	=> 5,
	'relevance'	=> 88,
	'docid'		=> 'FSPLT1_008817',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '22730'
		)
	)
),

Array(
	'rownum'	=> 6,
	'relevance'	=> 85,
	'docid'		=> 'FSPLT1_008816',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '22730'
		)
	)
),

Array(
	'rownum'	=> 7,
	'relevance'	=> 75,
	'docid'		=> 'FSPLT1_008815',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '22730'
		)
	)
),

Array(
	'rownum'	=> 8,
	'relevance'	=> 72,
	'docid'		=> 'FSPLT1_008389',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '23073'
		)
	)
),

Array(
	'rownum'	=> 9,
	'relevance'	=> 64,
	'docid'		=> 'FSPLT1_008993',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '1635'
		)
	)
),

Array(
	'rownum'	=> 10,
	'relevance'	=> 62,
	'docid'		=> 'FSPLT1_008191',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '30360'
		)
	)
),

Array(
	'rownum'	=> 11,
	'relevance'	=> 52,
	'docid'		=> 'FSPLT1_009447',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '10370'
		)
	)
),

Array(
	'rownum'	=> 12,
	'relevance'	=> 52,
	'docid'		=> 'FSPLT1_008204',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '16899'
		)
	)
),

Array(
	'rownum'	=> 13,
	'relevance'	=> 51,
	'docid'		=> 'FSPLT1_009493',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '22688'
		)
	)
),

Array(
	'rownum'	=> 14,
	'relevance'	=> 50,
	'docid'		=> 'FSPLT1_009768',
	'collapsed'	=> 0,
	'htmlcontext'	=> 'html snippet',
	'metadata'	=> Array(
		'moreMetadata' => Array(
			'projectid' => '26829'
		)
	)
)

);

// Update metadata with cloned verbose values
foreach (array_keys($hitlist) as $key) {
  $docid = $hitlist[$key]['docid'];
  $hitlist[$key]['htmlcontext'] = $this->makeContextSnippet($docid);
  $this->updateMetadata($hitlist[$key], $docid);
}

return $hitlist;

}

function makeDummyCollapsedHitList() {

$hitlist = $this->makeDummyHitList();
$newhitlist = array();
foreach ($hitlist as $hit) {
  $pid = $hit['metadata']['moreMetadata']['projectid'];
  if (array_key_exists($pid, $newhitlist)) {
    $newhitlist[$pid]['collapsed']++;
  } else {
    $newhitlist[$pid] = $hit;
  }
}
return array_values($newhitlist);

}

    public function searchDocs($querystring, $options) {
      $meta = Array();
      $meta['msize']	= 22;

      $hits = '';

      if ($options['collapse']) {
        $hits = $this->makeDummyCollapsedHitList();
      } else {
        $hits = $this->makeDummyHitList();
      }
      $meta['rowcount'] = count($hits);

      $resp = Array('resultmetadata' => $meta, 'hitlist' => $hits);
      return $resp;
    }

    public function getNewFileList($jobid) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

    public function createNEPAMailingList($palsid, $shortname) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

    public function markNEPAAide($palsid, $moreMetadata) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

    public function unmarkNEPAAide($palsid) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }

    public function updateNEPAAide($palsid, $moreMetadata) {
      throw new Exception("Not implemented",
        BROKER_SYSTEM_EXCEPTION);
    }
  }

?>

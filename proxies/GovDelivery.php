<?php

require_once realpath(dirname(__FILE__)) . "/../includes/consts.php";
require_once realpath(dirname(__FILE__)) . "/Http.php";

// May need to uncomment below if being used apart from the broker
// date_default_timezone_set("America/New_York");  // already set in broker.php

/*
 *	PHP Object encapsulating access to GovDelivery
 *
 *	API methods:		addTopic(TOPIC_CODE)
 *				getTopics()
 *
 *	No GovDelivery API access to update/delete topics.
 *
 *	We may need to support snailmail subscribers with no
 *	email address, although this should not affect the
 *	task to support CARA email subscriptions.
 *
 *				addSubscriber(EMAIL_ADDR)
 *				deleteSubscriber(EMAIL_ADDR)
 *
 *	Nominally GovDelivery allows you to update the subscriber record
 *	which lets you change the email address or change the key from an
 *	email address to a phone number.  There is no current need for an
 *	API to expose this yet, and no current plan to expose an API to
 *	update subscriber questions.
 *
 *				addSubscription(EMAIL_ADDR, TOPIC_CODE)
 *
 *	private methods:	http_get(url)
 *				http_post(url, payload)
 *				http_put(url, payload)
 *				http_delete(url)
 *				newTopic(TOPIC_CODE)
 *				newSubscriber(EMAIL_ADDR)
 *
 *	payload is an XML string
 *
 *	Errors may come from 3 sources:
 *	- GovDelivery (invalid request, inconsistent data)
 *	- Web Server (bad URL, timeouts)
 *	- cURL lib (internal errors)
 *
 *	GovDelivery and Web Server errors can be used for program flow
 *	(e.g. unique key already in use).  Curl errors are generally fatal.
 *
 *	Internal to this class, GovDelivery and Web Server errors are thrown
 *	as HttpExceptions, while Curl errors are thrown as CurlExceptions.
 *	Classes using the public API methods probably aren't too interested
 *	in these distinctions, so convert everything to standard Exception
 *	in the public methods.
 */

class GovDelivery {

  /* From consts.php */
  private $HOST = GOVDELIVERY_HOST;
  private $ACCOUNT = GOVDELIVERY_ACCOUNT;
  private $USERPWD = GOVDELIVERY_USERPWD;

  public $ACCOUNT_PREFIX;
  private $AUTH_USERPWD;

  function __construct() {
    if (($this->HOST === "GOVDELIVERY_HOST") ||
        ($this->ACCOUNT == "GOVDELIVERY_ACCOUNT") ||
        ($this->USERPWD == "GOVDELIVERY_USERPWD")) {
      throw new Exception(
        "Mailing List Manager not configured, please notify admin",
        BROKER_SYSTEM_EXCEPTION);
    }

    $HOST = $this->HOST;
    $ACCOUNT = $this->ACCOUNT;

    $this->ACCOUNT_PREFIX = "https://${HOST}/api/account/${ACCOUNT}";
    $this->AUTH_USERPWD = utf8_encode($this->USERPWD);
  }

  function http_get($url)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Return a variable instead of posting it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(200), $result);
    curl_close($ch);
    return $result;
  }

  function http_post($url, $payload)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Return a variable instead of posting it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml"));
    curl_setopt($ch, CURLOPT_POSTFIELDS, "$payload");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(200), $result);
    curl_close($ch);
    return $result;
  }

  function http_put($url, $payload)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Return a variable instead of posting it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PUT, true);
    curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml"));
    $putData = tmpfile();
    fwrite($putData, $payload);
    fseek($putData, 0);
    curl_setopt($ch, CURLOPT_INFILE, $putData);
    curl_setopt($ch, CURLOPT_INFILESIZE, strlen($payload)); 
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(200), $result);
    curl_close($ch);
    return $result;
  }

  function http_delete($url)
  {
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $this->checkErrors($ch, array(200), $result);
    curl_close($ch);
    return $result;
  }
  /*
   *  Generate a new topic as XML
  *
  *  Code is the unique ID, must be all caps and alphanumeric and/or dash
  *  GovDelivery is expecting a code with a starting "NEPA_" prefix,
  *  like NEPA_123456 (yes it sounds like their documentation is wrong)
  *
  *  Usage:
  *  $GD = new GovDelivery();
  *  $newTopicXML = $GD->newTopic("NEPA-TEST");
  */
  function newActiveTopic($activeid, $projectname, $shortname, $projectdescription) {
  	$time = strftime("%Y-%m-%dT%H:%M:%S");

  	
  	
  	$response =
  	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
  	<topic>
  	<code>$activeid</code>
  	<default-pagewatch-results></default-pagewatch-results>
  	<description>" . htmlspecialchars($projectdescription, ENT_QUOTES, 'UTF-8') . "</description>
  	<lock-version>0</lock-version>
  	<name>" . htmlspecialchars($projectname, ENT_QUOTES, 'UTF-8') . "</name>
  	<pagewatch-autosend>false</pagewatch-autosend>
  	<pagewatch-enabled>false</pagewatch-enabled>
  	<pagewatch-suspended>false</pagewatch-suspended>
  	<rss-feed-description></rss-feed-description>
  	<rss-feed-title></rss-feed-title>
  	<rss-feed-url></rss-feed-url>
  	<send-by-email-enabled>false</send-by-email-enabled>
  	<short-name>" . htmlspecialchars($shortname, ENT_QUOTES, 'UTF-8') . "</short-name>
  	<subscribers-count>0</subscribers-count>
  	<wireless-enabled>false</wireless-enabled>
  	<pages></pages>
  	</topic>
  	";
  	return $response;
  }
  

  
  /*
   *  Generate a new topic as XML
   *
   *  Code is the unique ID, must be all caps and alphanumeric and/or dash
   *  GovDelivery is expecting a code with a starting "NEPA_" prefix,
   *  like NEPA_123456 (yes it sounds like their documentation is wrong)
   *
   *  Usage:
   *  $GD = new GovDelivery();
   *  $newTopicXML = $GD->newTopic("NEPA-TEST");
   */
  function newTopic($code) {
    $time = strftime("%Y-%m-%dT%H:%M:%S");
    $response =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<topic>
<code>NEPA_$code</code>
    <default-pagewatch-results></default-pagewatch-results>
    <description>Created on $time ***_DO_NOT_EDIT_THIS_RECORD!_***</description>
    <lock-version>0</lock-version>
    <name>ZZZ-$code</name>
    <pagewatch-autosend>false</pagewatch-autosend>
    <pagewatch-enabled>false</pagewatch-enabled>
    <pagewatch-suspended>false</pagewatch-suspended>
    <rss-feed-description></rss-feed-description>
    <rss-feed-title></rss-feed-title>
    <rss-feed-url></rss-feed-url>
    <send-by-email-enabled>false</send-by-email-enabled>
    <short-name>ZZZ-$code</short-name>
    <subscribers-count>0</subscribers-count>
    <wireless-enabled>false</wireless-enabled>
    <pages></pages>
</topic>
";
    return $response;
  }

  /*
   *  Generate a new Subscriber record as XML
   *
   *  This only supports the subscribe-by-email variant, not by
   *  phone number.
   */
  function newSubscriber($email) {
    $response =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<subscriber>
    <email>" . htmlentities($email) . "</email>
    <send-notifications>false</send-notifications>
    <digest-for>0</digest-for>
</subscriber>
";
    return $response;
  }

  /*
   *  Generate an XML request for a new list of topics for a subscriber
   *
   *  This will replace the subscriber's current list, so the input list
   *  of topic codes must be complete.
   *
   *  Note that the <topics> tag must include the type="array" attribute
   *  or the operation will fail.
   */
  function newSubscriptionList($topicCodeAr) {
    $topiclist = "";
    $codeAr = array_unique($topicCodeAr);
    foreach ($codeAr as $value) {
      $topiclist = "${topiclist}<topic><code>$value</code></topic>";
    }
    $response =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<subscriber>
    <send-notifications>false</send-notifications>
    <topics type=\"array\">
${topiclist}
    </topics>
</subscriber>
";
    return $response;
  }

 /**
  *   category_xml = '<topic><categories type="array">'
  for id in catid_list:
    category_xml += '<category><code>' + escape(id) + '</code></category>'
  category_xml += '</categories></topic>'
  * 
  * @param unknown $categoryAr
  * @return string
  */
  
  function newTopicCategoryList($categoryAr) {
  	
  	$categorylist = "";
  	$codeAr = array_unique($categoryAr);
  	foreach ($codeAr as $value) {
  		$categorylist = $categorylist . "<category><code>$value</code></category>";
  	}
  $response =
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <categories type=\"array\">
  " . $categorylist .
  "</categories>
  ";
  return $response;
  } 
  
  
  /*
   *  Check the status of the last cURL request, given a list of HTTP
   *  response codes that indicate success.
   *
   *  cURL errors are thrown as Exceptions.
   *  HTTP errors are thrown as HttpExceptions.
   */
  function checkErrors($curl, $goodStatusList, $svrResponse) {
    if (curl_errno($curl) !== 0) {
      // Curl error
      throw new CurlException(
        curl_error($curl),
        curl_errno($curl)
      );
    }
    $info = curl_getinfo($curl);
    $httpcode = $info["http_code"];
    if (! in_array($httpcode, $goodStatusList)) {
      throw new HttpException(
        $this->makeErrorMsg($httpcode, $svrResponse),
        $httpcode);
    }
  }

  /*
   *  If svrResponse is a list of nested <errors>, extract the text
   *  for use as an error message
   *
   *  <errors>
   *    <error>line 1</error>
   *    <error>line 2</error>
   *    <error>line 3</error>
   *  </errors>
   *
   *  Otherwise just report back the HTTP code
   */
  function makeErrorMsg($code, $svrResponse) {
    $msg = "";
    try {
      $xml = new SimpleXMLElement($svrResponse);
      foreach ($xml->error as $errtxt) {
        $msg = $msg . "\n" . trim($errtxt);
      }
    } catch (Exception $e) { }
    if (strlen($msg) > 0) {
      return $msg;
    } else {
      return "HTTP error $code";
    }
  }

/***********************************
 *   Public functions start here
 ***********************************/

  /*
   *	Add a new topic
   *
   *	Throw exception with MAILING_LIST_ALREADY_EXISTS
   *	if topic already exists
   */
  function addTopic($code) {
    $req = $this->newTopic($code);
    $url = $this->ACCOUNT_PREFIX . "/topics.xml";
    try {
      $response = $this->http_post($url, $req);
      return $response;
    } catch (HTTPException $e) {
      if ($e->getCode() == 422) {
        if (strpos($e->getMessage(), "has already been taken")) {
          throw new Exception($e->getMessage(),
            MAILING_LIST_ALREADY_EXISTS);
        }
      }
      throw new Exception("HTTP error code " . $e->getCode() . " " .
                          $e->getMessage(), MAILING_LIST_SERVER_EXCEPTION);
    } catch (CurlException $e) {
      throw new Exception("CURL error code " . $e->getCode() . " " .
                          $e->getMessage(), MAILING_LIST_SERVER_EXCEPTION);
    }
  }
  /*
   *	Add a new topic
  *
  *	Throw exception with MAILING_LIST_ALREADY_EXISTS
  *	if topic already exists
  */
  function addActiveTopic($activeid, $projectname, $shortname, $projectdescription){
  	$req = $this->newActiveTopic($activeid, $projectname, $shortname, $projectdescription);
  	$url = $this->ACCOUNT_PREFIX . "/topics.xml";
  	try {
  		$response = $this->http_post($url, $req);
  		return $response;
  	} catch (HTTPException $e) {
  		if ($e->getCode() == 422) {
  			if (strpos($e->getMessage(), "has already been taken")) {
  				throw new Exception($e->getMessage(),
  						MAILING_LIST_ALREADY_EXISTS);
  			}
  		}
  		throw new Exception("HTTP error code " . $e->getCode() . " " .
  				$e->getMessage(), MAILING_LIST_SERVER_EXCEPTION);
  	} catch (CurlException $e) {
  		throw new Exception("CURL error code " . $e->getCode() . " " .
  				$e->getMessage(), MAILING_LIST_SERVER_EXCEPTION);
  	}
  }
  
  /*
   *	Get list of all topics for this account
   */
  function getTopics() {
    $url = $this->ACCOUNT_PREFIX . "/topics.xml";
    return $this->http_get($url);
  }
  
  function getTopic($code){
  	$url = $this->ACCOUNT_PREFIX . "/topics/$code.xml";
  	return $this->http_get($url);
  }
  
  /*
   *	Get list of all topics for this account
  */
  function getCategories() {
  	$url = $this->ACCOUNT_PREFIX . "/categories.xml";
  	return $this->http_get($url);
  }  
  /*
   *	Get list of all topics for this account
  */
  function getCategory($code) {
  	$url = $this->ACCOUNT_PREFIX . "/categories/$code.xml";
  	return $this->http_get($url);
  }
  
  function setTopicCategories($topicid, $categoryids){
  	
  	$req = newTopicCategoryList($categoryids);
  	$url = $this->ACCOUNT_PREFIX . "/topics/" .  $topic . "/categories.xml";
  	$response = $this->http_post($url, $req);
  	return $response;
  }
  
  /*
   *	Add a single subscriber
   *
   *	TBD: Add subscriber details as "user questions"
   */
  function addSubscriber($email, $details = null) {
    $newUser = $this->newSubscriber($email);
    $url = $this->ACCOUNT_PREFIX . "/subscribers.xml";
    try {
      $response = $this->http_post($url, $newUser);
      return $response;
    } catch (HTTPException $e) {
      if ($e->getCode() !== 422) {	// No error if subscriber exists
        throw $e;
      }
    }
    /*
     * At this point we have to decide whether or not to add/update
     * the subscriber-questions info.  Ask Matt when this should be done,
     * and find out what the API is.
     */
  }

  /*
   *	Remove a subscriber
   */
  function deleteSubscriber($email) {
    $user64 = base64_encode($email);
    $url = $this->ACCOUNT_PREFIX . "/subscribers/${user64}.xml";
    $response = $this->http_delete($url);
    return $response;
  }

  /*
   *	Add a topic to a subscriber's list of subscriptions
   *
   *	Note: the PUT response is empty when the update succeeds
   */
  function addSubscription($email, $topicCode) {
    $user64 = base64_encode($email);
    $url = $this->ACCOUNT_PREFIX . "/subscribers/${user64}/topics.xml";

    // Get subscriber topics
    $subscriberTopicsXML = $this->http_get($url);

    $subscriberTopics = new SimpleXMLElement($subscriberTopicsXML);
    $topicArray = array();
    foreach ($subscriberTopics->topic as $topic) {
      $topicArray[] = $topic->{'to-param'};
    }

    // Add new topic
    $topicArray[] = $topicCode;
    $newSubs = $this->newSubscriptionList($topicArray);

    // Put subscriber topics
    $response = $this->http_put($url, $newSubs);
    return $response;
  }
  
  function addCategory($id, $name, $shortname, $description, $parent, $qs_page){

  	
  	$req = $this->newCategory($id, $name, $shortname, $description, $parent, $qs_page);
 	$url = $this->ACCOUNT_PREFIX . "/categories.xml";


  	try {
  		$response = $this->http_post($url, $req);
  		return $response;
  	} catch (HTTPException $e) {
		$code = $e->getCode();
  		fwrite($fp2, "HTTPException: $code\n");
  		if ($e->getCode() == 422) {
  			if (strpos($e->getMessage(), "has already been taken")) {
  				throw new Exception($e->getMessage(),
  						MAILING_LIST_ALREADY_EXISTS);
  			}
  		}
  		throw new Exception("HTTP error code " . $e->getCode() . " " .
  				$e->getMessage(), MAILING_LIST_SERVER_EXCEPTION);

  		throw new Exception($req, MAILING_LIST_SERVER_EXCEPTION);
  		  		
  	} catch (CurlException $e) {
		$code = $e->getCode();
  		fwrite($fp2, "CurlException: $code\n");
  		throw new Exception("CURL error code " . $e->getCode() . " " .
  				$e->getMessage(), MAILING_LIST_SERVER_EXCEPTION);
  	}
  }
  
  /*
   *  Generate a new Subscriber record as XML
  *
  *  This only supports the subscribe-by-email variant, not by
  *  phone number.
  */
  function newCategory($id, $name, $shortname, $description, $parent, $qs_page) {


 // 			  if (len(qs_page) > 0):
 //   qs_page = "<code>" + escape(qs_page) + "</code>"
  	$response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>					
    <category>								
    <allow-subscriptions type=\"boolean\">true</allow-subscriptions>	
    <code>$id</code>					
    <default-open type=\"boolean\">true</default-open>			
    <description>$description</description>		
    <lock-version type=\"integer\">1</lock-version>			
    <name>$name</name>					
    <short-name>$shortname</short-name>			
    <parent><code>$parent</code></parent>		
    <qs_page><code>$qs_page</code></qs_page>				
</category>";
  	
  	
  	return $response;
  }
  
  
  
  
}

?>

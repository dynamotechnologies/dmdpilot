#!/bin/ksh
#       This is the default standard profile provided to a user.
#       They are expected to edit it to meet their own needs.

sqlplus /nolog << eof
connect /  as sysdba
shutdown immediate
eof

lsnrctl stop

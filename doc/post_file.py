#!/usr/local/bin/python

"""
Upload a file to a URL

Command-line args:
1    URL
2    path to file to upload
3,4+ pairs of name/value form values

Prints the output of the CGI located at URL
"""

import MultipartPostHandler, urllib2, cookielib
from sys import argv

argv.pop(0)		# argv[0] - application name
URL = argv.pop(0)	# argv[1]
filename = argv.pop(0)	# argv[2]
params = {}		# parse remaining argv elements in pairs
while (len(argv) > 0):
    name = argv.pop(0)
    val = argv.pop(0)
    params[name] = val
cookies = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies),
                                MultipartPostHandler.MultipartPostHandler)
params['file'] = open(filename, "rb")
print opener.open(URL, params).read()


<?php

/*
 *  Script to generate sample XMLRPC transcripts for each of the
 *  supported request types--needs to be run from the PHP CLI i.e.
 *
 *    $ php generate_samples.php > output.txt
 *
 */

// Edit this to point to the server/project directory you're in right now:
# $BASEURL="http://10.37.1.230/drmdev_mth2";
exit("Edit the script and configure the broker URL first!!!\n");

// test putfile (special case)
$lastLine=exec("python post_file.py $BASEURL/tests/handle_testput.php sample_file.txt title 'Sample file' author 'Test script' pubdate '2008-02-15' appsysID TEST appdoctype DOC projectID 0002 decisionID 1010 debug 2", $output_ar);

$output = join("\n", $output_ar);

$DOCID = parseDocID($output);
print "Got doc ID $DOCID\n";
printResults("putFile", $output);

/**********
 *  TESTS
 **********/
$TEST_GETFILE="$BASEURL/tests/handle_testget.php?id=$DOCID&fmt=PDF&debug=2&optionsFlag=on";
$output = file_get_contents($TEST_GETFILE);
printResults("getFile", $output);

$TEST_GETFILEDATA="$BASEURL/tests/handle_testgetd.php?id=$DOCID&fmt=PDF&debug=2&optionsFlag=on";
$output = file_get_contents($TEST_GETFILEDATA);
printResults("getFileData", $output);

$TEST_GETFILEMETADATA="$BASEURL/tests/handle_testgetm.php?id=$DOCID&fmt=PDF&debug=2&optionsFlag=on";
$output = file_get_contents($TEST_GETFILEMETADATA);
printResults("getFileMetadata", $output);

$TEST_DELETE="$BASEURL/tests/handle_testdelete.php?id=$DOCID&debug=2";
$output = file_get_contents($TEST_DELETE);
printResults("deleteFile", $output);

/*
 * Parse from ---SENDING--- to ---END---
 * Then parse from ---INFLATED RESPONSE--- to ---END---
 */
function printResults($testname, $output) {
  // *? is the "minimal" match operator (as opposed to the "greedy" * operator)
  $sent = preg_replace('/.*SENDING---(.*?)---END.*/s', '${1}', $output);
  $got = preg_replace('/.*INFLATED RESPONSE---(.*?)---END.*/s','${1}',$output);

  print "==============================\n";
  print "Request type: $testname\n";
  print "--- SENT ---\n";
  print html_entity_decode($sent);
  print "\n";

  print "--- RECEIVED \n";
  print html_entity_decode($got);
  print "\n";
  print "==============================\n";
}

/*
 * Parse the output of the "post_file.py" request (test for putFile)
 * and return the new document ID
 * Parse from ---INFLATED RESPONSE--- to ---END---
 * Then parse from <string> to </string>  This should be the only payload
 */
function parseDocID($output) {
  $out_1 = html_entity_decode($output);
  // *? is the "minimal" match operator (as opposed to the "greedy" * operator)
  $got = preg_replace('/.*INFLATED RESPONSE---(.*?)---END.*/s','${1}',$out_1);
  $id = preg_replace('/.*\<string\>(.*?)\<\/string\>.*/s','${1}',$got);

  return $id;
}

?>

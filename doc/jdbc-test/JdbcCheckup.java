/*
 * This file was originally based on:
 * http://docs.oracle.com/cd/B19306_01/java.102/b14355/getsta.htm#i1003811
 * It has been modified to reduce some stupid logic present in the old file, to use ojdbc14.jar, and 
 * to use an Oracle SID connection instead of a service name.
 */



/*
 * This sample can be used to check the JDBC installation.
 * Just run it and provide the connect information. It will select
 * "Hello World" from the database.
 */

// You need to import the java.sql and JDBC packages to use JDBC
import java.sql.*;
import oracle.jdbc.*;

// We import java.io to be able to read from the command line
import java.io.*;

class JdbcCheckup
{
  public static void main(String args[]) throws SQLException, IOException
  {

    // Prompt the user for connect information
    System.out.println("Please enter information to test connection to the database");
    String user;
    String password;
    String host;
    String port;
    String sid;

    user = readEntry("User: ");
    int slash_index = user.indexOf('/');
    if (slash_index != -1)
    {
      password = user.substring(slash_index + 1);
      user = user.substring(0, slash_index);
    } else {
      password = readEntry("Password: ");
    }
    host = readEntry("Host: ");
    port = readEntry("Port: ");
    sid = readEntry("SID: ");
    
    System.out.println("Connecting to the database...");
    System.out.println("Connecting...");
    System.out.flush();

    String url = "jdbc:oracle:thin:@" + host + ":" + port + ":" + sid;
    System.out.println("Using connection string " + url);
    System.out.flush();

    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
    Connection conn = DriverManager.getConnection(url, user, password);


    // Create a statement
    Statement stmt = conn.createStatement();

    // Do the SQL "Hello World" thing
    ResultSet rset = stmt.executeQuery("select 'Hello World' from dual");

    while (rset.next())
      System.out.println(rset.getString(1));
    // close the result set, the statement and connect
    rset.close();
    stmt.close();
    conn.close();
    System.out.println("Your JDBC installation is correct.");
  }

  // Utility function to read a line from standard input
  static String readEntry(String prompt)
  {
    try
    {
      StringBuffer buffer = new StringBuffer();
      System.out.print(prompt);
      System.out.flush();
      int c = System.in.read();
      while (c != '\n' && c != -1)
      {
        buffer.append((char)c);
        c = System.in.read();
      }
      return buffer.toString().trim();
    }
    catch(IOException e)
    {
      return "";
    }
  }
}

11/15 Removed broker/apis/ directory, moved code up one level
      Renamed configs/ to includes/
      Changed constants to DEFINE tokens, moved to includes/consts.inc

11/16 Moved test functions from drmProxy to dummyProxy, leave empty
      functions in drmProxy

11/19 Made dummy methods in drmProxy return something
      Made getFile return a struct containing file and metadata members
      Added getFileData which contains the old getFile code
      Renamed all test files so tests are .php files (and can use include())

11/20 Added optional "options" parameter to getFile/getFileData functions,
      "options" is an XMLRPC struct containing any option flags

11/26 Updated testget/testgetd to test "options" parameter.

12/03 Checked in to SVN repository on phase1

12/04 Integrated with Stellent code
      New broker configuration files in include directory, rename appropriate
      file to brokerConfig.inc
      Renamed config file in tests directory, edit and rename config file to
      config.php

12/13 Added deleteFile API

12/18 Added appsysid and appdoctype metadata fields, putFile now raises
      MISSING_METADATA_EXCEPTION when required fields are not present

12/18 putFile raises EMPTY_FILE_EXCEPTION when zero-length file is submitted

2008
2/28 FILETYPE_NOT_FOUND returned when a fileType option is specified but no
     rendition of the file can be found in that format.  This indicates
     that the file ID does reference an existing file.
     This behavior differs from prior versions where the broker would simply
     return the original file.

     getFileMetadata now adds a "filesize" member to the returned metadata
     struct indicating the size of the referenced file in bytes.

     The "options" struct can now be specified optionally for the
     getFileMetadata procedure, specifically so that it can be used to refer
     to the PDF version of a document.  If the file cannot be found in the
     requested rendition, FILETYPE_NOT_FOUND is returned.

     testgetm.php and handle_testgetm.php modified to use the filetype option.

     test/handle_* functions have been modified to take "debug" as a CGI
     parameter (it is a GET option in all files except for handle_testput.php,
     where it is a POST parameter).  This allows us to generate examples of
     XMLRPC transactions by submitting GET/POST requests through these PHP
     scripts--this is useful for generating documentation.

     Added doc directory which contains some PHP and Python scripts to
     automatically generate sample XMLRPC transactions for documentation.

4/11 Stellent support for filesize metadata attribute added, front end code
     now returns a standard message when exception FILETYPE_NOT_FOUND is
     thrown.

5/13 Changed logging so that logfile.txt contains summaries of transactions
     (including elapsed time) and transactions.txt contains the complete
     (redacted) XMLRPC messages.

7/11 Added front-end interfaces for putMergedFile and replaceFileMetadata,
     which currently resolve to stub methods which throw "not implemented"
     exception messages.

7/12 Committed back-end processing for putMergedFile and replaceFileMetadata

12/15 Modified drmProxy.php to use PHP SOAP library instead of NuSOAP.

12/30 sanity_check: test if the summary log is writable

2/18/09 Latest version of the code uses the PHP 5.2.8 library, which has a
        few significant improvements over the old library, in particular, it
        uses the limits set in php.ini.  So as part of migration to 5.2.8,
        we're using the following changes to the php.ini file in test:

        max_execution_time from 30 to 60 (seconds)
        memory_limit from 34M to 1000M (hopefully overkill)
        post_max_size from 33M to 100M (memory_limit > post_max_size)
        upload_max_filesize from 32M to 100M (upload_max_filesize > post_max)

        * Note that in dev, a 68 Mb file was seen to cause memory use to
          expand to about 1 Gb, so upload_max_filesize could be set to
          68 and post_max_size to 69, to be consistent.  We currently have
          no special error handling when files that are "too large" are
          submitted--this is mostly because the spec does not take a hard
          line on the maximum size files that we can process, which it really
          should.

        * Also make sure that the directories under job_queues all have
          world-writable privileges.

5/22/09 Fixed bug where validateMetadata was not properly handling datetime
        values being sent (as strings) from PHPXMLRPC.
        Added filetype blacklist to validateMetadata.
        Added the _srcDocIDs moreMetadata parameter to all files created as
        a result of putMergedFile.  This will help track files in the event
        that they need to be recreated (although it probably won't help if
        the source files have been deleted).

6/2/09 Fixed reporter.sh and agentutils.py to place job logs in the logs/
       directory.

7/7/09 Added "broker/shim.txt" file which is a workaround that prevents PHP
       from holding ridiculous amounts of memory during putFile requests on
       64-bit platforms.  Not quite sure why this works, but reading a large
       file with get_file_contents() appears to help.

7/7/09 Got rid of shim file.  Wasn't helping.

8/4/09 Added hostname to PALS_WWW_1 report subject line

8/5/09 Updated test scripts to use HTTP_BASIC_USER and HTTP_BASIC_PASS if it
       is defined in tests/config.php, to enable HTTP BASIC authorization
       where present.

9/18/09 Added sanity_check test for presence of 24 Mb test file.
        Added code to ut_start.php to stop processing if sanity_check fails.

9/18/09 Changed queueJob (in drmProxy.php) to create the jobfile using the
        transaction ID (TXID) as part of the filename, instead of randomly
        generating a file suffix.  This should be sufficiently unique, and
        allow easy forward/backward tracing of job status.

11/7/09	Changed agents and reports to read config variables out of the new
	jobs.conf file in the includes directory.  This must be created as
	part of installation.

11/9/09 First version with (dummy) search function

12/8/09 Added validation for filename length (80)

12/13/09 Changed search function to use Solr repository (still no ingest)

12/22/09 Change title width to 200

1/26 Added author, title, appdoctype metadata fields to search index,
     made author, title, appdoctype, moreMetadata the only valid fields
     to change in replaceFileMetadata

5/20 Added code to handle multifile uploads, getNewFileList()
     Expanded MAX_FILENAME_LENGTH to 255

4/20/11 Added GovDelivery API

5/17 Added extended replaceFileMetadata behavior to merge moreMetadata fields

5/23 Changed test code for replaceFileMetadata and server code in broker.php
     and drmAPI.php to handle case where replaceFileMetadata request has an
     empty metadata structure, or an empty options structure.

     Apparently when an empty XMLRPC struct is submitted as a request parameter
     PHP will convert it to an empty list, but when the type of this variable
     is queried it will return as an 'array' instead of a 'struct', because
     these look identical in PHP.  This causes validation errors because
     (empty) struct parameters will not match the expected method signature.

     The workaround is to convert the parameter signature to a struct when
     the method is replaceFileMetadata, the parameter type is array, and the
     parameter is empty.  It's not great but it works.  I think for the most
     part, clients aren't submitting empty options structs if they don't want
     any options.

6/1 Renamed consts.inc to consts.php

6/6 Added Datamart access so that putFile and deleteFile also attempt to
    add/remove records from the Datamart (configured in consts.php)

7/19 Modified replaceFileMetadata so that certain metadata/moreMetadata
     fields will automatically update the Datamart ProjectDocument records.
     In particular:
       a.  title -> docname
       b.  moreMetadata[pubflag] -> pubflag
           pubflag = 1 / 0
       c.  moreMetadata[wwwlink] -> wwwlink
       d.  moreMetadata[description] -> description
       e.  the filesize of the PDF rendition -> pdffilesize
     We may need to modify this further, to generate the value for wwwlink
     when pubflag is set to 1.

7/21 Modified drmProxy.php and Datamart.php so that checking in a document
     will automatically assign a wwwlink value.  This is a temporary fix for
     the problem of indicating the public location of a published document.
     Modified publishFile so that publishing a document automatically
     updates the pdffilesize on the Datamart.
     Modified Datamart.php so that updateProjectDocument does not update
     pubflag/wwwlink values, since these are assigned directly through the
     Datamart API and will be removed from DMD metadata.

* Broker has been updated to support PALS 3.0

10/13 Updated to copy moreMetadata/sensitiveflag to Datamart.

12/21 Changed deleteFile response to boolean 0 (false) when file is not
      found--previous response was a string "803".

1/12/2012 Removed processstatus from the list of required NEPA Aide metadata.
          No longer validated in drmAPI.php or drmProxy.php

2/4/2013 Get GovDelivery credentials from common/parseini.php

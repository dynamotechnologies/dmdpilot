<?php
  /*
   *  File: brokerConfig.inc.drm
   *  Uses DRM proxy for services
   *  To use this file, rename to brokerConfig.inc
   */
  registerService('helloWorld', $dummyProxy);
  registerService('echoMsg', $dummyProxy);
  registerService('zipPhaseDocs', $drmProxy);
  registerService('associateDoc', $drmProxy);
  registerService('dissociateDoc', $drmProxy);
  registerService('getFile', $drmProxy);
  registerService('getFileData', $drmProxy);
  registerService('getFileMetadata', $drmProxy);
  registerService('putFile', $drmProxy);
  registerService('deleteFile', $drmProxy);
  registerService('putMergedFile', $drmProxy);
  registerService('replaceFileMetadata', $drmProxy);
  registerService('publishFile', $drmProxy);
  registerService('transferFile', $drmProxy);
  registerService('searchDocs', $drmProxy);
  registerService('getNewFileList', $drmProxy);
  registerService('createNEPAMailingList', $drmProxy);
  registerService('markNEPAAide', $drmProxy);
  registerService('unmarkNEPAAide', $drmProxy);
  registerService('updateNEPAAide', $drmProxy);
?>

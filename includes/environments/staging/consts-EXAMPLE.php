<?php

require_once("/var/www/common/parseini.php");

/*
 * Exception constants
 */

// XMLRPC errors (codes taken from PHPXMLRPC)
define("XMLRPC_UNKNOWN_METHOD", 1);
define("XMLRPC_BAD_PARAMETERS", 3);

// General/broker exceptions
define("BROKER_SYSTEM_EXCEPTION",   800); // Trouble with system, config err?
define("NO_SERVER_AVAIL_EXCEPTION", 801);
define("BAD_DATE_FORMAT_EXCEPTION", 802);

define("TITLE_LENGTH_EXCEPTION", 811);
define("AUTHOR_LENGTH_EXCEPTION", 812);
define("FILENAME_LENGTH_EXCEPTION", 817);
define("INVALID_METADATA_FIELD_EXCEPTION", 813);
define("DISALLOWED_FILE_TYPE_EXCEPTION",   816);

define("INVALID_OPTION_EXCEPTION",  804);
define("MISSING_METADATA_EXCEPTION", 805);
define("EMPTY_FILE_EXCEPTION",      806);

define("INVALID_AGENT_EXCEPTION",   815);

define("INVALID_JOB_ID_EXCEPTION",	818);

// Repository exceptions
define("FILETYPE_NOT_FOUND_EXCEPTION", 810);
define("FILE_NOT_FOUND_EXCEPTION",  803);
define("REPOSITORY_SYSTEM_EXCEPTION",	807);
define("BAD_XML_EXCEPTION",		808);
define("REPOSITORY_TIMEOUT_EXCEPTION",	809);
define("FILEID_ALREADY_EXISTS_EXCEPTION", 814);

// Mailing list exceptions
define("MAILING_LIST_SERVER_EXCEPTION",	900);
define("MAILING_LIST_ALREADY_EXISTS", 910);

// Datamart exceptions
define("DATAMART_SERVER_EXCEPTION",		1000);
define("DATAMART_REFERENCE_VIOLATION",		1010);
define("DATAMART_RECORD_ALREADY_EXISTS",	1020);
define("DATAMART_RECORD_NOT_FOUND",		1040);

define("FILETYPE_NOT_FOUND_MSG",
  "File could not be found in the requested format, please try again later.");

/*
 * Option names
 */

define("OPT_FILETYPE", "fileType");
define("OPT_INDEXNAME", "indexname");
define("OPT_COUNTITEMS", "countitems");
define("OPT_STARTITEM", "startitem");
define("OPT_PAGENUM", "pagenum");
define("OPT_COLLAPSE",  "collapse");
define("OPT_PROJECTIDS", "projectids");
define("OPT_DELETEMM", "deleteMM");
define("OPT_MERGEMM", "mergeMM");



/*
 * Limits
 */
define("MAX_TITLE_LENGTH", 200);
define("MAX_AUTHOR_LENGTH", 80);
define("MAX_FILENAME_LENGTH", 255);

/*
 * Server Config
 */

define("DRM_PROTOCOL", "http");
/*
 * Uncomment one of the following server definitions for access to a
 * DRM server.
 * Also, make sure any host names (e.g. oracletest2, oracleprod2) are
 * defined correctly in /etc/hosts!  These names are privately used by the
 * Stellent WSDLs and need to resolve to their correct addresses.
 */

/* AWS pilot
define("DRM_SERVER", "oracleprod2");
define("DRM_WSDL_PATH", "/web/groups/secure/wsdl/custom/");
define("DRM_PORT", "80");
*/

/* AWS test
define("DRM_SERVER", "oracletest2");
define("DRM_WSDL_PATH", "/web/groups/secure/wsdl/custom/");
define("DRM_PORT", "80");
*/

/* Lentech pilot
define("DRM_SERVER", "10.0.15.12");
define("DRM_WSDL_PATH", "/web/groups/secure/wsdl/custom/");
define("DRM_PORT", "80");
*/

/* Lentech test
define("DRM_SERVER", "10.0.14.11");
define("DRM_WSDL_PATH", "/web/groups/secure/wsdl/custom/");
define("DRM_PORT", "80");
*/

/* Old POCG fstst server
define("DRM_SERVER", "svcbroker.fstst.phaseonecg.com");
define("DRM_WSDL_PATH", "/drm3/groups/secure/wsdl/custom/");
define("DRM_PORT", "80");
*/

define("DRM_DEBUG_MODE", FALSE);

/*
 * Uncomment one of the following to identify the cache server used to
 * publish documents
 */
// define("DMD_CACHE", "a123.g.akamai.net/7/123/11558/abc123/forestservic.download.akamai.com/11558/www/nepa");	# Pilot
// define("DMD_CACHE", "cache.ecosystem-management.org");	# Old pilot config, temporarily deprecated
// define("DMD_CACHE", "testcache.ecosystem-management.org");	# Test

/*
 * Security Group and Account determine UCM security. They are passed to
 *   the content server during checkin/update, along with the Doctype.
 */
define("DMD_SECURITY_GROUP","dmd");
define("DMD_ACCOUNT","dmd");
define("DMD_DOC_TYPE","dmd");

/*
//  For use with old POCG fstst server
define("DMD_SECURITY_GROUP","test");
define("DMD_ACCOUNT","test");
define("DMD_DOC_TYPE","test");
*/

/*
 * Uncomment one of the following server definitions for access to a
 * GovDelivery server:
 */

/*
  // GovDelivery test
  define("GOVDELIVERY_HOST", "stage-api.govdelivery.com");
  define("GOVDELIVERY_ACCOUNT", "FSPALS");
*/

/*
  // GovDelivery production
  define("GOVDELIVERY_HOST", "api.govdelivery.com");
  define("GOVDELIVERY_ACCOUNT", "USDAFS");
*/

// Get GovDelivery credentials from config/dmUtil-conf
define("GOVDELIVERY_USERPWD", get_govdelivery_userpwd());

/*
 * Uncomment one of the following server definitions for access to the
 * Datamart server:
 */

/*
// Dev
  define("DATAMART_PREFIX", "http://localhost:7001/api/1_0");
  define("DATAMART_USERPWD", "admin:p1cgorcl");
*/

/*
  // Test
  define("DATAMART_PREFIX", "http://localhost:7001/api/1_0");
  define("DATAMART_USERPWD", "svcbroker:brokertest00");
*/

/*
  // Pilot
  define("DATAMART_PREFIX", "http://localhost:7001/api/1_0");
  define("DATAMART_USERPWD", "svcbroker:Br0k3r3squ3");
*/

/*
 * Set location of the filerecorder service host
 */
define("FILERECORDER_SERVER", "localhost");

?>
